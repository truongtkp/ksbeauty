<#import "template.ftl" as layout>
    
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('username','password') displayInfo=realm.password && realm.registrationAllowed && !registrationDisabled??; section>
    <#if section = "header">
    <#elseif section = "form">
    <div id="kc-form">
        <div id="wrapper-form">
            <img class="logo-website" src="" alt="" />
            <div id="kc-form-wrapper">
                    <div class="form-header-anfico">
                            <img class="logo-websites" src="" alt="" />
                            <h2 class="NVNValky">LOGIN</h2>
                            <p>Please enter your account information to login.</p>
                    </div>
                <#if realm.password>
                    <form id="kc-form-login" onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post">
                        <div class="${properties.kcFormGroupClass!}">
                            <label for="username" class="${properties.kcLabelClass!}">Account</label>

                            <#if usernameEditDisabled??>
                                <input tabindex="1" id="username" class="${properties.kcInputClass!}" name="username" value="${(login.username!'')}" type="text" disabled />
                            <#else>
                                <input tabindex="1" id="username" class="${properties.kcInputClass!}" name="username" value="${(login.username!'')}"  type="text" autofocus autocomplete="off"
                                    aria-invalid="<#if messagesPerField.existsError('username','password')>true</#if>"
                                />

                               
                            </#if>
                        </div>

                        <div class="${properties.kcFormGroupClass!}">
                            <label for="password" class="${properties.kcLabelClass!}">Password</label>

                            <input tabindex="2" id="password" class="${properties.kcInputClass!}" name="password" type="password" autocomplete="off"
                                aria-invalid="<#if messagesPerField.existsError('username','password')>true</#if>"
                            />

                             <#if messagesPerField.existsError('username','password')>
                                    <span id="input-error"  class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                           Account or password is incorrect!
                                    </span>
                                </#if>
                        </div>

                        <div id="kc-form-buttons" class="${properties.kcFormGroupClass!}">
                            <input type="hidden" id="id-hidden-input" name="credentialId" <#if auth.selectedCredential?has_content>value="${auth.selectedCredential}"</#if>/>
                            <input tabindex="4" class="btn-submit" name="login" id="kc-login" type="submit" value="Login"/>
                        </div>                     
                            <#if realm.password && social.providers??>
                                <div id="kc-social-providers" class="${properties.kcFormGroupClass!}">
                            
                                    <ul class="${properties.kcFormSocialAccountListClass!} <#if social.providers?size gt 3>${properties.kcFormSocialAccountListGridClass!}</#if>">
                                        <#list social.providers as p>
                                            <a id="social-${p.alias}" class="btn-submit" style="color:#fff" <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                                    type="button" href="${p.loginUrl}">
                                                <#if p.iconClasses?has_content>
                                                    <div class="icon-google" style="margin-left:-50px"></div>
                                                    <span class="${properties.kcFormSocialAccountNameClass!} kc-social-icon-text">${p.displayName!}</span>
                                                <#else>
                                                    <span class="${properties.kcFormSocialAccountNameClass!}">${p.displayName!}</span>
                                                </#if>
                                            </a>
                                        </#list>
                                    </ul>
                                </div>
                            </#if>
                    </form>
                </#if>
            </div>
            <div class="bg-login_sub">
            </div>
        </div>
    </div>
    <#elseif section = "info" >
        <#if realm.password && realm.registrationAllowed && !registrationDisabled??>
            <div id="kc-registration-container">
                <div id="kc-registration">
                    <span>${msg("noAccount")} <a tabindex="6" href="${url.registrationUrl}">${msg("doRegister")}</a></span>
                </div>
            </div>
        </#if>
    </#if>

</@layout.registrationLayout>
