<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('firstName','lastName','email','username','password','password-confirm'); section>
    <#if section = "header">
        ${msg("registerTitle")}
    <#elseif section = "form">
          <div class="wap-register">
        <div class="center">
                <img class="logo-register" src="" alt="" />
        </div>
    
        <form id="kc-register-form" action="${url.registrationAction}" method="post">
           <div class="kc-form-title col-2" style="flex-direction: column">
                    <h3>Register</h3>
                    <p class="text-welcome" style="font-size: 14px;padding: 0 4px;text-align: center;">After successful registration, we redirect you to <a href="${client.baseUrl}">alumin.com.vn</a>  Please check your email.</p>
            </div>
                   <#if !realm.registrationEmailAsUsername>
                <div class="${properties.kcFormGroupClass!}">
                    <div>
                        <label for="username" class="${properties.kcLabelClass!}">User name<span style="color:#F10000;">*</label>
                    </div>
                    <div>
                        <input type="text" id="username" class="${properties.kcInputClass!}" name="username"
                               value="${(register.formData.username!'')}" autocomplete="username"
                               aria-invalid="<#if messagesPerField.existsError('username')>true</#if>"
                        />
                        <span class="form-message"></span>
                        <#if messagesPerField.existsError('username')>
                            <span id="input-error-username" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('username'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </div>
            </#if>

             <div class="${properties.kcFormGroupClass!}">
                    <div>
                        <label for="fullname" class="${properties.kcLabelClass!}">Full name<span style="color:#F10000;">*</label>
                    </div>
                    <div>
                        <input type="text" id="fullname" class="${properties.kcInputClass!}" name="fullname"
                               value="${(register.formData.fullname!'')}" autocomplete="fullname"
                               aria-invalid="<#if messagesPerField.existsError('fullname')>true</#if>"
                        />
                        <span class="form-message"></span>
                        <#if messagesPerField.existsError('fullname')>
                            <span id="input-error-fullname" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('fullname'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </div>

        <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('birthday',properties.kcFormGroupErrorClass!)}">
            <div>
                <label for="user.attributes.birthday" class="${properties.kcLabelClass!}">Date of birth<span style="color:#F10000;">*</span></label>
            </div>
            <div>
                <input
                type="date"
                id="user.attributes.birthday"
                class="${properties.kcInputClass!}"
                name="user.attributes.birthday"
                value="${(register.formData['user.attributes.birthday']!'')}"
                />
                <span class="form-message"></span>
            </div>
            </div>


            <div class="${properties.kcFormGroupClass!}">
                <div>
                    <label for="email" class="${properties.kcLabelClass!}">Email<span style="color:#F10000;">*</span></label>
                </div>
                <div>
                    <input type="text" id="email" class="${properties.kcInputClass!}" name="email"
                           value="${(register.formData.email!'')}" autocomplete="email"
                           aria-invalid="<#if messagesPerField.existsError('email')>true</#if>"
                    />
                    <span class="form-message"></span>
                    <#if messagesPerField.existsError('email')>
                        <span id="input-error-email" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('email'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

    
            <#if passwordRequired??>
                <div class="${properties.kcFormGroupClass!}">
                    <div>
                        <label for="password" class="${properties.kcLabelClass!}">Password<span style="color:#F10000;">*</span></label>
                    </div>
                    <div>
                        <input type="password" id="password" class="${properties.kcInputClass!}" name="password"
                               autocomplete="new-password"
                               aria-invalid="<#if messagesPerField.existsError('password','password-confirm')>true</#if>"
                        />
                           <span class="form-message"></span> 
                        <#if messagesPerField.existsError('password')>
                            <span id="input-error-password" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('password'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </div>

                <div class="${properties.kcFormGroupClass!}">
                    <div>
                        <label for="password-confirm"
                               class="${properties.kcLabelClass!}">Confirm password<span style="color:#F10000;">*</span></label>
                    </div>
                    <div>
                    <div class="${properties.kcInputClass!}">
                        <input type="password" id="password-confirm"
                        class="form-item-eye" 
                               name="password-confirm"
                               aria-invalid="<#if messagesPerField.existsError('password-confirm')>true</#if>"
                        />
                        <img id="eye-icon" class="eye-icon activePassword" />

                    </div>
                            <span class="form-message"></span>
                        <#if messagesPerField.existsError('password-confirm')>
                            <span id="input-error-password-confirm" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('password-confirm'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </div>
            </#if>
            
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('phone',properties.kcFormGroupErrorClass!)}">
            <div>
                <label for="user.attributes.phone" class="${properties.kcLabelClass!}">Phone number<span style="color:#F10000;">*</span></label>
            </div>
            <div>
                <input
                type="text"
                id="user.attributes.phone"
                class="${properties.kcInputClass!}"
                name="user.attributes.phone"
                value="${(register.formData['user.attributes.phone']!'')}"
                />
                <span class="form-message"></span>
            </div>
            </div>

              <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('company',properties.kcFormGroupErrorClass!)}">
            <div>
                <label for="user.attributes.company" class="${properties.kcLabelClass!}">Company</span></label>
            </div>
            <div>
                <input
                type="text"
                id="user.attributes.company"
                class="${properties.kcInputClass!}"
                name="user.attributes.company"
                value="${(register.formData['user.attributes.company']!'')}"
                />
                <span class="form-message"></span>
            </div>
            </div>


            <#if recaptchaRequired??>
                <div class="form-group">
                    <div>
                        <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                    </div>
                </div>
            </#if>

            <div class="col-2 btn-register_wap">
                <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                    <input class="transparent-class" type="submit" value="${msg("doRegister")}"/>
                </div>

                 <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                     <span>Do you have an account?</span>
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                        <span><a href="${url.loginUrl}">Login</a></span>
                    </div>
                </div>
            </div>
        </form>
    </div>
    </#if>
</@layout.registrationLayout>
