import clsx from "clsx";
import { useNavigate } from "react-router-dom";
import useInView from "../../hooks/useInView";
import Button from "../Button";
import { useEffect, useMemo, useRef } from "react";
import { useTranslation } from "react-i18next";
import { useAppSelector } from "../../hooks/hook";
import { hostBE } from "../../types/host";

export default function TopicItem() {
  const { ref, isInView } = useInView();
  const [t] = useTranslation();
  const navigate = useNavigate();
  const translate = useAppSelector((state) => state.translateSlice);
  const refFrame = useRef<HTMLIFrameElement>(null);

  useEffect(() => {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      create_iframe();
    }
  }, [refFrame.current]);

  function create_iframe() {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      refFrame.current?.classList.add("ios");
    }
  }

  return (
    <div
      className="mb-36 mt-48 lssm:px-[24px] md:px-[80px]  xl:px-[50px]  w-1920:px-[162px] relative"
      ref={ref}
    >
      <div>
      <div className="relative">
        <div
          className={clsx(
            " sc<992:flex justify-between sc991:flex-col"
          )}
        >
                    <div
            ref={refFrame}
            className={clsx(
              "sc<992:w-[50%] h-full sc991:w-full sc991:order-1 z-0 btn-footer after:z-0 after:top-1 after:left-1 lg:after:top-[20px] lg:after:left-[20px]",
              {
                "animate__animated animate__fadeInRight": isInView,
              }
            )}
          >
            <iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" scrolling="no" allowFullScreen={true} src="https://www.youtube.com/embed/tg9hNriQYTE" className=' sc991:w-full lssm:h-[280px] md:h-[370px] lg:h-[540px] xl:h-[422px] md:w-full object-cover rounded-[32px] iframeVideo z-10' width="100%" height="422" title="SPA DO&JA"></iframe>
          </div>
           <div className={clsx("w-[55%] sc<992:ml-[60px] sc991:w-full sc991:mt-[24px] flex flex-col items-center")}>
          <div
            className={clsx(
                "flex flex-col text-text-title font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[48px]",
                { "animate__animated animate__fadeInRight": isInView }
              )}
            >
          <span className="Valky text-[#2F2D38] text-[48px]">{t("home.topic.service")}</span>
          <div>
          <img src={`${hostBE}/image/1691376056347のimage39.png`} className="xl:w-[270px] lg:w-[270px] w-[140px] py-8 mx-auto" alt="icon-topic1.png"/>
          </div>
        </div>
        <div
          className={clsx("flex justify-between items-end text", {
            "animate__animated animate__fadeInRight": isInView,
          })}
        >
        </div>
            <div
              className={clsx(
                "lssm:text-px14 w-1920:text-px20 2xl:text-px18 xl:text-px16 md:text-px16 sc<992:mb-[32px] sc991:mb-3 text-[#2F2D38] text-justify",
                { "animate__animated animate__flash": isInView }
              )}
              dangerouslySetInnerHTML={{ __html: t("home.description_sub") }}
            ></div>
            <div className="sc>768:flex sc>768:justify-center">
              <Button
                onClick={() => navigate("/gioi-thieu")}
                color="grey"
                className="sc>768:text-px14 text-px16 sc<992:mb-[72px] sc991:mx-auto sc991:mb-6 w-1920:h-[60px] 2xl:h-[55px] xl:h-[50px] lg:h-[45px] m992:h-[45px] md:h-[45px] sm:h-[40px] sm-480:h-[40px] rounded-[5px] w-1920:w-[200px] 2xl:w-[190px] xl:w-[180px] lg:w-[180px] m992:w-[180px] md:w-[180px] sm:w-[170px] sm-480:w-[170px] shadow-md"
              >
                <span className="flex items-center text-inherit font-medium w-1920:text-px20 2xl:text-px18 m992:text-px16 md:text-px16">
                  {t("button.see_more")}
                </span>
              </Button>{" "}
            </div>
          </div>         
        </div>
      </div>
      </div>
    </div>
  );
}
