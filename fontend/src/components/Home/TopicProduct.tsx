import clsx from "clsx";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { CgArrowLongRight } from "react-icons/cg";
import { Link, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import useInView from "../../hooks/useInView";
import { changeCategoryChecked } from "../../reducers/categorySlice";
import { getProductActiveHome } from "../../reducers/productPublic";
import { hostBE } from "../../types/host";

import Button from "../Button";

const datas = [
  {
    title: "home.topic.brows",
    des: "home.description_sub",
    listImage: [
      {
        image: "http://localhost:3000/fe/young-woman.png",
        grid: 6
      },
      {
        image: "http://localhost:3000/fe/young-woman.png",
        grid: 3
      },
      {
        image: "http://localhost:3000/fe/young-woman.png",
        grid: 3
      },
      {
        image: "http://localhost:3000/fe/young-woman.png",
        grid: 3
      },
      {
        image: "http://localhost:3000/fe/young-woman.png",
        grid: 3
      },
      {
        image: "http://localhost:3000/fe/young-woman.png",
        grid: 6
      },
      {
        image: "http://localhost:3000/fe/young-woman.png",
        grid: 4
      },
      {
        image: "http://localhost:3000/fe/young-woman.png",
        grid: 4
      },
      {
        image: "http://localhost:3000/fe/young-woman.png",
        grid: 4
      },
    ]
  },
  {
    title: "home.topic.lips",
    des: "home.description_sub",
    listImage: []
  },
  {
    title: "home.topic.nails",
    des: "home.description_sub",
    listImage: []
  },
  {
    title: "home.topic.lashes",
    des: "home.description_sub",
    listImage: []
  }
]

export default function TopicProduct() {
  const { ref, isInView } = useInView();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const translate = useAppSelector((state) => state.translateSlice);
  const [indexActive, setIndexActive] = useState<number>(0)
  const categories = useAppSelector(
    (state) => state.productPublic.productActiveHome
  );
  const [t] = useTranslation();

  const handleNavigate = (id: number) => {
    navigate("/sanpham");
    dispatch(changeCategoryChecked(id));
  };
  useEffect(() => {
    // if (productActiveHome.length === 0) {
    dispatch(getProductActiveHome());
    // }
  }, []);

  return (
    <div>
      <div className="text-center">
        <h2 className="Valky mb-8 text-[#F5E3D5] text-[48px] font-medium 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[56px]">{t("home.topic.ks")}</h2>
        <span className="Valky text-[#2F2D38] text-[48px] font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[48px]">{t("home.topic.beautyful")}</span>
      </div>
      <div className="flex justify-between items-center my-16">
        {datas.map((data, index) => {
          return <div onClick={() => setIndexActive(index)} key={data.title} className="relative aspect-[362/160] w-[362px] flex justify-center items-center rounded-[24px]"
          style={{
            backgroundImage: `url('${hostBE}/fe/young-woman.png')`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}>
            <div className="absolute bottom-0 right-0 w-full">
                <div className="w-full opacity-30 bg-[#000000] aspect-[362/160] rounded-[24px]"></div>
            </div>
            <h3 className="text-white text-[38px] iframeVideo z-50">{t(data.title)}</h3>
          </div>
        })}
      </div>
      <div className="mt-24">
        <div
          className={clsx(
            "flex flex-col text-center text-text-title font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[48px]",
            { "animate__animated animate__fadeInRight": isInView }
            )}
            >
          <span className="Valky text-[#2F2D38] text-[48px]">{t(datas?.[indexActive]?.title)}</span>
          <div>
          <img src={`${hostBE}/image/1691376056347のimage39.png`} className="xl:w-[270px] lg:w-[270px] w-[140px] py-8 mx-auto" alt="icon-topic1.png"/>
          </div>
          <div>
            <p className={clsx(
                "my-5 lssm:text-px14 w-1920:text-px20 2xl:text-px18 xl:text-px16 md:text-px16 sc<992:mb-16 sc991:mb-3 text-[#2F2D38] px-[10%] text-center",
                { "animate__animated animate__flash": isInView }
              )}>{t(datas?.[indexActive]?.des)}</p>
          </div>
          <div className="grid grid-cols-12 gap-5">
          {datas?.[indexActive].listImage.map((image, index) => {
            return <div key={index} style={{ gridColumn: `span ${image.grid} / span ${image.grid}` }} className="h-[363px] overflow-hidden rounded-[24px]">
                  <img src={`${image.image}`} alt="" className="w-full h-full object-cover rounded-[24px] duration-700 hover:scale-125" />
            </div>
          })}
          </div>
        </div>
      </div>
    </div>
  );
}
