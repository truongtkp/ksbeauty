type Props = {
  width: number | string;
  height: number | string;
};

export default function Map({ width, height }: Props) {
  return (
    <iframe
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3192.162706847825!2d174.83082507662053!3d-36.86252408027627!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d0d49c0618d4475%3A0x1d76f97db204948d!2s20%20Thatcher%20Street%2C%20Mission%20Bay%2C%20Auckland%201071%2C%20New%20Zealand!5e0!3m2!1sen!2s!4v1681783983131!5m2!1sen!2s"
      width={width}
      height={height}
    ></iframe>
  );
}
