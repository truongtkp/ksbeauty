import { useContext, useEffect, useState } from "react";
import { StepperContext } from "../../contexts/StepperContext";
import { useTranslation } from "react-i18next";

export default function Details({data,setter}:any) {
    const [t] = useTranslation();
    const { userData, setUserData }:any = useContext(StepperContext);

    const [ service, setService] = useState([]);
    const [onHighlighted,setOnHighlighted] = useState(data.service.id);

    const handleChange = (e:any) => {
        const {name, value} = e.target;
        setUserData({ ...userData, [name]: value});
    };

    useEffect(() => {
        let temp = data;
        temp.callback = () => {
            return data.service.id != 0;
        }
        temp.onClickNext = () => {
            data.service.id != 0 && data.next();
        }
        temp.onClickBack = () => {
            data.back()
        };
        setter(temp);

        fetch ("/api/package").then(
            (res) => {
                return res.json();
            }
        ).then((res) => {
            setService(res.list);
        })
    },[]);

    const handler = (event:any) => {
        let temp1 = data;
        let temp = event.currentTarget.id;
        data.service.id = temp;
        setter(temp1);
        setOnHighlighted(temp);
    }

    const items = () => {
        const rows = [];
        for (let i = 0; i < service.length; i++){
            rows.push(
                <div key={(service[i] as any).id} id={(service[i] as any).id} onClick={handler}
                className={ (((service[i] as any).id == onHighlighted)?"bg-[#E5A380] ":"") +
                    "flex justify-between items-center text-[#64483B] duration-200 px-[21px] py-[11px] mb-3 shadow-[0_4px_4px_0_rgba(0,0,0,0.25)] rounded-md border-1 border-[#C4BEBB] hover:text-white hover:bg-[#E5A380]"
            }>
                    <div>
                        <h3 className="lsm-360:text-px12 lsm-380:text-px12 font-bold text-px20 leading-[25px] sm-390:text-px14 sm-480:text-px14 sc>768:text-px16 mb-1">{(service[i] as any).title}</h3>
                        {/* <p className="uppercase text-px16 leading-[18px]">1H</p> */}
                    </div>
                    <p className="lsm-360:text-px12 lsm-380:text-px12 text-px20 sc>768:text-px16 leading-[25px] sm-390:text-px14 sm-480:text-px14 font-bold">{(service[i] as any).price} NZ$</p>
                </div>
            ) 
        }
        return rows 
    };

    return (
        <div className="w-full rounded-[10px] shadow-[0_4px_4px_0px_rgba(0,0,0,0.25)] border-1 border-[#C4BEBB]">
        <h2 className="absolute top-0 lssm:text-px20 md:text-[32px] text-text-primary font-bold">{t("contact.title")}</h2>
        <p className="absolute top-14 text-text-gray lssm:text-px14 md:text-px16">{t("contact.description")}</p>

        {/* Title start */}
        <div className="w-full flex justify-between items-center px-[33px] py-[23px] bg-[#F5ECE5] shadow-[0_4px_4px_0_rgba(0,0,0,0.25)] rounded-t-[10px]">
            <div>
                <h2 className="text-px32 sc>768:text-px16 font-medium text-[#64483B] uppercase leading-10">Service Package</h2>
            </div>
            <div>
                <p className="lsm-360:text-px12 lsm-380:text-px12 text-[#64483B] text-px16 sm-480:text-px14 sm-390:text-px14 font-medium leading-5"><span className="text-[#E6A380]">(Selected: {data.service.id == 0?0:1})</span> {service.length} Packages</p>
            </div>
        </div>
        {/* Title end */}

        {/* Menu Address start */}
        <div className="rounded-b-[10px] bg-[#FAF9F6]">
            <div className="overflow-y-scroll px-[34px] py-[22px] h-[435px]">
                {items()}
                {/* <div className="flex justify-between items-center text-[#64483B] duration-200 px-[21px] py-[11px] mb-3 shadow-[0_4px_4px_0_rgba(0,0,0,0.25)] rounded-md border-1 border-[#C4BEBB] hover:text-white hover:bg-[#E5A380]">
                    <div>
                        <h3 className="font-bold text-px20 leading-[25px] mb-1">CSD Cơ bản</h3>
                        <p className="uppercase text-px16 leading-[18px]">1H</p>
                    </div>
                    <p className="text-px20 leading-[25px] font-bold">200.000USD$</p>
                </div> */}
            </div>
        </div>
        {/* Menu Address end */}
    </div>);
}