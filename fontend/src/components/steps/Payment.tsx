import { useTranslation } from "react-i18next"
import { FormikValues,useFormik } from "formik";
import * as Yup from "yup";
import {loadScript} from "@paypal/paypal-js"
import { useEffect, useState} from "react";
import paypalService from "../../services/paypal";
import bookingService from "../../services/booking";


export default function Payment({data,setter,handleClick}:any) {
    const [checked, setChecked] = useState(false);
    // const [triggerChecker, setTriggererChecker] = useState(false);

    const [isLoading, setIsLoading] = useState(true);
    const [paypalGenerated, setPaypalGenerated] = useState(false);
    // const [paypalCallback, setPaypalData] = useState({
    //     callback: (values:FormikValues) => {
    //         console.log("no good");
    //     }
    // }); 

    // const [cacheValue , setCacheValue] = useState({
    //     fullname: "",
    //     phone: "",
    //     email: "",
    //     content: ""
    // });

    const [t] = useTranslation();
    const dateToLocalTimeISOString = (param:Date) => {
        return new Date(param.getTime() - (param.getTimezoneOffset() * 60000)).toISOString();
    }

    // const clientId = "AUIedOs8C2i7GhroqNCuxF1Rkay0KlcfdyJ876DSoEpPQ84w-wE7U9M-oEOa3xWELhPp5E_TbMQUDfmK";
    const formik = useFormik({
        initialValues: {
            fullname: '',
            phone: "",
            email: "",
            content: "",
            isValidated: false
        },
        validationSchema: Yup.object({
            fullname: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.name"), min: 5 })),
            email: Yup.string().required(t("validate.error.required")).matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, t("validate.error.pattern", { name: t("career.form.email") })),
            phone: Yup.string().required(t("validate.error.required")).matches(/^[0-9]{10}$/, t("validate.error.pattern", { name: t("contact.form.phone") })),
            // notcontente: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.help"), min: 5 })),
        }),
        onSubmit: (values) => {
            if (!checked){
                console.log("not checked");
                return;
            }
            console.log("Validation Successful!");
            values.isValidated = true;

            // let temp = cacheValue;
            // temp.fullname = values.fullname;
            // temp.phone = values.phone;
            // temp.email = values.email;
            // temp.content = values.content;
            // setCacheValue(temp);
            // paypalCallback.callback(values);
        }
    })

    const generateSdkScript = (token:any) => {
        loadScript({
        //   "sdkBaseURL":"https://www.paypal.com/sdk/js",
        //   components: "buttons,hosted-fields", // with hosted field
        //   "client-id":clientId,
          "client-id": token.client_id,
          "data-client-token": token.client_token,
        //   "currency": "NZD"
          "currency": "USD"
        
        }).then((paypal) => {


            // Using hosted fields
        //   if (!paypal?.HostedFields?.isEligible()){
        //     console.log("NOT ELIGABLE");
        //     return;
        //   }
          
        //   console.log("HOSTED FIELDS IS ELIGABLE");
        //   paypal.HostedFields.render(
        //     {
        //       createOrder: () => {
        //         setIsLoading(true);
        //         const temp:string[] = data.time.split(':');
        //         const hour:number = parseInt(temp[0]);
        //         const minute:number = parseInt(temp[1]);

        //         return fetch("/api/booking", {
        //             method: "POST",
        //             headers: {
        //                 'Accept': 'application/json',
        //                 'Content-Type': 'application/json'
        //             },
        //             body: JSON.stringify({
        //                 addressId: parseInt(data.location.id),
        //                 email: cacheValue.email,
        //                 fullName: cacheValue.fullname,
        //                 phoneNo: cacheValue.phone,
        //                 packageId: parseInt(data.service.id),
        //                 staffId: parseInt(data.staff.id),
        //                 hour: hour,
        //                 minute: minute,
        //                 bookingDate: dateToLocalTimeISOString(data.bookingDate).split('T')[0]
        //             })
        //         }).then((res) => {
        //           return res.json()
        //         }).then((orderData) => {
        //           console.log(orderData);
        //           data.orderId = orderData.orderId;
        //           // orderId = orderData.id; // needed later to complete capture
        //           return orderData.orderId;
        //         }).catch((err) => {
        //             alert(JSON.stringify(err));
        //             setIsLoading(false);
        //         })
        //       },

        //       fields: {
        //         number: {
        //           selector: "#card-number",
        //           placeholder: "1111 1111 1111 1111"
        //           // value: "test"
        //         },
        //         cvv: {
        //           selector: "#cvv",
        //           placeholder: "123"
        //         },
        //         expirationDate: {
        //           selector: "#expiration-date",
        //           placeholder: "00/0000"
        //         }
        //       }
        //     }
        //   ).then((cardField) => {
        //     document?.querySelector("#card-form")?.addEventListener("submit", (event) => {
        //       event.preventDefault();
        //       formik.submitForm(); // update value
        //     })

        //     let paypalTemp = paypalCallback;

        //     paypalTemp.callback = (values:FormikValues) => {
        //         cardField.submit().then((res) => {
        //             console.log(data.orderId);
        //             const temp:string[] = data.time.split(':');
        //             const hour:number = parseInt(temp[0]);
        //             const minute:number = parseInt(temp[1]);

        //             fetch ('/api/booking/confirm/' + data.orderId, {
        //                 method: "POST",
        //                 headers: {
        //                     'Accept': 'application/json',
        //                     'Content-Type': 'application/json'
        //                 },
        //                 body: JSON.stringify({
        //                     addressId: parseInt(data.location.id),
        //                     email: cacheValue.email,
        //                     fullName: cacheValue.fullname,
        //                     phoneNo: cacheValue.phone,
        //                     packageId: parseInt(data.service.id),
        //                     staffId: parseInt(data.staff.id),
        //                     hour: hour,
        //                     minute: minute,
        //                     bookingDate: dateToLocalTimeISOString(data.bookingDate).split('T')[0]
        //                 })
        //             }

        //             ).then((res) => {
        //                 if (res.status == 200) data.next();
        //             })
        //         }).catch((err) => {
        //             // console.log(err);
        //             alert(JSON.stringify(err));
        //             setIsLoading(false);
        //         })
        //     }
        //     setPaypalData(paypalTemp);
        //   })



        paypal?.Buttons?.({
          // Sets up the transaction when a payment button is clicked
          createOrder: async function () {

                formik.values.isValidated = false;
                await formik.submitForm();

                if (!formik.values.isValidated){
                    throw Error("Validation Failed");
                }

                const temp:string[] = data.time.split(':');
                const hour:number = parseInt(temp[0]);
                const minute:number = parseInt(temp[1]);

                return bookingService.postBookingSimple(
                    {
                        addressId: parseInt(data.location.id),
                        email: formik.values.email,
                        fullName: formik.values.fullname,
                        phoneNo: formik.values.phone,
                        packageId: parseInt(data.service.id),
                        staffId: parseInt(data.staff.id),
                        hour: hour,
                        minute: minute,
                        bookingDate: dateToLocalTimeISOString(data.bookingDate).split('T')[0]
                    }
                ).then((orderData) => {
                  data.orderId = orderData.orderId;
                  // orderId = orderData.id; // needed later to complete capture
                  return orderData.orderId;
                }).catch((err) => {
                    alert(JSON.stringify(err));
                    setIsLoading(false);
                })
              },
              onError: function (err) {
                setIsLoading(false);
              },

          // Finalize the transaction after payer approval
          onApprove: function () {
            const temp:string[] = data.time.split(':');
            const hour:number = parseInt(temp[0]);
            const minute:number = parseInt(temp[1]);

            return fetch ('/api/booking/confirm/' + data.orderId, {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    addressId: parseInt(data.location.id),
                    email: formik.values.email,
                    fullName: formik.values.fullname,
                    phoneNo: formik.values.phone,
                    packageId: parseInt(data.service.id),
                    staffId: parseInt(data.staff.id),
                    hour: hour,
                    minute: minute,
                    bookingDate: dateToLocalTimeISOString(data.bookingDate).split('T')[0]
                })
            }).then((res) => {
                if (res.status == 200) {
                    data.next();
                    return;
                };
                alert(JSON.stringify(res));
            })
          },
        })
        .render("#paypal-button-container");


          // console.log("The order id is: " + orderId);
        }).then((res) => {
            data.backBool = true;
            setter(data);
        })
        .catch((err) => {
          console.log("Failed to generate paypal SDK",err);
        }).finally(() => {
            setIsLoading(false);
        })
    };


    useEffect(() => {
        data.callback = () => {
            // ASYN FUNCTION WRAPPER
            formik.submitForm();
            return true;
        }
        data.onClickNext = () => {
            // console.log("not good");
            formik.submitForm();
        }
        data.onClickBack = () => {
            data.back();
        }
        data.backBool = false;
        setter(data);
    },[]);

    useEffect(() => {

        // Comment here for fake paypal 
        if (!paypalGenerated){
            paypalService.getTokenIdentity().then((res) => {
                generateSdkScript(res);
            })
            setPaypalGenerated(true);
        }
    })

    const handleFieldChange = function (event:any) {
        formik.handleChange(event);
        (formik.values as any)[event.target.name] = event.target.value;
        // console.log(formik.values);

    }

    return (

        <div className="w-full rounded-[10px]">
        { isLoading && <div className="fixed top-0 left-0 h-screen w-full bg-[#0003]/20 z-50 flex justify-center">
            <button type="button" className=" transition ease-in-out duration-150 " disabled>

            <svg className="animate-spin -ml-1 mr-3 h-40 w-40 text-gray-400" fill="none" viewBox="0 0 24 24">
                <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
            </svg>

                {/* <svg className="animate-spin h-5 w-5 mr-3 ..." viewBox="0 0 24 24"></svg> */}
                {/* Processing... */}
            </button>
        </div> }

        <h2 className="absolute top-0 lssm:text-px20 md:text-[32px] text-text-primary font-bold">{t("contact.title")}</h2>
        <p className="absolute top-14 text-text-gray lssm:text-px14 md:text-px16">{t("contact.description1")}</p>




       {/* Content start */}
            <div className="text-white bg-[#BC8A79] mb-4 pl-[26px] pr-[34px] pt-4 pb-8 shadow-[0_4px_4px_0_rgba(0,0,0,0.25)] rounded-[14px]">
                <div className="flex justify-between items-center pb-[30px]">
                    <div>
                        {/* <h3 className="font-bold text-px20 leading-[25px] mb-1">CSD Cơ bản</h3> */}
                        <h3 className="font-bold text-px20 sc>768:text-px18 leading-[25px] mb-2">{data.service.title}</h3>
                        <p className="uppercase text-px16 sc>768:text-px14 leading-[18px] mb-2">1H</p>
                        {/* <p className="uppercase text-px16 leading-[18px]">Phục vụ: Trang Trần</p> */}
                        <p className="uppercase text-px16 sc>768:text-px14 leading-[18px]">Staff: {data.staff.name}</p>
                    </div>
                    {/* <p className="text-px20 leading-[25px] font-bold">200.000USD$</p> */}
                    <p className="text-px20 leading-[25px] sc>768:text-px18 font-bold">{data.service.price} NZ$</p>
                </div>
                <div className="flex justify-between items-center pb-[25px]">
                    <div>
                        <h3 className="font-bold text-px20 sc>768:text-px18 leading-[25px] mb-2">Promotion price :</h3>
                    </div>
                    <p className="text-px20 leading-[25px] sc>768:text-px18 font-bold">{data.service.price} NZ$</p>
                </div>
                <div className="flex justify-between items-center text-white py-[15px] border-t-[1px] border-[#F4C5AB]">
                    <p className="font-bold text-px20 sc>768:text-px18 leading-5">Total :</p>
                    {/* <p className="text-px20 leading-[25px] font-bold">200.000USD$</p> */}
                    <p className="text-px20 leading-[25px] sc>768:text-px18 font-bold">{data.service.price} NZ$</p>
                </div>
            </div>
       {/* Content end */}

       {/* Form start */}
            <div className="border-dashed border-[1px] border-[#F1B290] rounded-[25px]">
            <form id="card-form"
            
            // onSubmit={formik.handleSubmit} 
            
            className="lssm:p-[28px] lg:p-18 xl:px-[45px] flex flex-col justify-center items-center">
                    <div className="mt-8 w-full">
                    <div className="mt-[20px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[23%] 2xl:w-[25%] xl:w-[23%] lg:w-[31%] m992:w-[29%] sc991:w-[31%] w-1920:text-px20 2xl:text-px20 xl:text-px16 sc991:text-px14 font-medium text-[#68483C]">Name</label>
                            <div className="flex-1">
                                <input
                                    type="text"
                                    name="fullname"
                                    placeholder=""
                                    value={formik.values.fullname}
                                    onChange={handleFieldChange}
                                    className="pl-2 w-full h-[40px] border-[1px] rounded-[10px] border-[#F4C5AB] outline-none"
                                />
                                <span className="text-[14px] text-red-600 mt-1">
                                    {formik.errors.fullname}
                                </span>
                            </div>
                        </div>

                        <div className="mt-[20px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[23%] 2xl:w-[25%] xl:w-[23%] lg:w-[31%] m992:w-[29%] sc991:w-[31%] w-1920:text-px20 2xl:text-px20 xl:text-px16 sc991:text-px14 font-medium text-[#68483C]">Email</label>
                            <div className="flex-1">
                                <input
                                    type="text"
                                    name="email"
                                    placeholder=""
                                    value={formik.values.email}
                                    onChange={handleFieldChange}
                                    className="pl-2 w-full h-[40px] border-[1px] rounded-[10px] border-[#F4C5AB] outline-none"
                                />
                                <span className="text-[14px] text-red-600 mt-1">
                                    {formik.errors.email}
                                </span>
                            </div>
                        </div>

                        <div className="mt-[16px] mb-5 flex justify-between items-center">
                            <label className="form-label w-1920:w-[23%] 2xl:w-[25%] xl:w-[23%] lg:w-[31%] m992:w-[29%] sc991:w-[31%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-medium text-[#68483C]">Phone</label>
                            <div className="flex-1">
                                <input
                                    type="text"
                                    name="phone"
                                    placeholder=""
                                    value={formik.values.phone}
                                    onChange={handleFieldChange}
                                    className="pl-2 w-full h-[40px] border-[1px] rounded-[10px] border-[#F4C5AB] outline-none"
                                />
                                <span className="text-[14px] text-red-600 mt-1">
                                    {formik.errors.phone}
                                </span>
                            </div>
                        </div>

                        <div className="mt-[16px] mb-5 flex justify-between items-center">
                            <label className="form-label w-1920:w-[23%] 2xl:w-[25%] xl:w-[23%] lg:w-[31%] m992:w-[29%] sc991:w-[31%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-bold text-[#68483C]">Voucher Code</label>
                            <div className="flex-1">
                                <input
                                    type="text"
                                    name="codevoucher"
                                    placeholder=""
                                    // value={formik.values.phone}
                                    onChange={formik.handleChange}
                                    className="pl-2 w-full h-[40px] border-[1px] rounded-[10px] border-[#F4C5AB] outline-none"
                                />
                            </div>
                        </div>

                        <div className="mt-[24px] mb-[24px] flex justify-start items-center">
                            <h2 className="text-[#68483C] w-1920:text-px20 2xl:text-px20 m992:text-px16 xl:text-px16 font-semibold"> {t("button.title")} </h2>
                        </div>



                        {/* {CARD FIELD FORM} */}

                        {/* <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-medium text-[#68483C]">Card number</label>
                            <div
                                id = "card-number"
                                placeholder=""
                                className="pl-2 w-full h-[40px] border-[1px] rounded-[10px] border-[#F4C5AB] outline-none"
                            />
                            <span className="text-[14px] text-red-600 mt-1">
                            </span>
                        </div>

                        <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-medium text-[#68483C]">Expiration date</label>
                            <div
                                id = "expiration-date"
                                placeholder=""
                                className="pl-2 w-full h-[40px] border-[1px] rounded-[10px] border-[#F4C5AB] outline-none"
                            />
                            <span className="text-[14px] text-red-600 mt-1">
                            </span>
                        </div>

                        <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-medium text-[#68483C]">Security code</label>
                            <div
                                id= "cvv"
                                placeholder=""
                                className="pl-2 w-full h-[40px] border-[1px] rounded-[10px] border-[#F4C5AB] outline-none"
                            />
                            <span className="text-[14px] text-red-600 mt-1">
                            </span>
                        </div> */}



                        <div id="paypal-button-container"></div>

                        
                        <div className="mt-[24px] mb-3 flex items-center">
                            <input className="relative float-left mt-[0.15rem] border-[#D9A283] mr-[6px] h-[1.125rem] w-[1.125rem] appearance-none rounded-[0.25rem] border-[0.125rem] border-solid border-neutral-300 outline-none before:pointer-events-none before:absolute before:h-[0.875rem] before:w-[0.875rem] before:scale-0 before:rounded-full before:bg-transparent before:opacity-0 before:shadow-[0px_0px_0px_13px_transparent] before:content-[''] checked:border-primary checked:bg-primary checked:before:opacity-[0.16] checked:after:absolute checked:after:ml-[0.25rem] checked:after:-mt-px checked:after:block checked:after:h-[0.8125rem] checked:after:w-[0.375rem] checked:after:rotate-45 checked:after:border-[0.125rem] checked:after:border-t-0 checked:after:border-l-0 checked:after:border-solid checked:after:border-white checked:after:bg-transparent checked:after:content-[''] hover:cursor-pointer hover:before:opacity-[0.04] hover:before:shadow-[0px_0px_0px_13px_rgba(0,0,0,0.6)] focus:shadow-none focus:transition-[border-color_0.2s] focus:before:scale-100 focus:before:opacity-[0.12] focus:before:shadow-[0px_0px_0px_13px_rgba(0,0,0,0.6)] focus:before:transition-[box-shadow_0.2s,transform_0.2s] focus:after:absolute focus:after:z-[1] focus:after:block focus:after:h-[0.875rem] focus:after:w-[0.875rem] focus:after:rounded-[0.125rem] focus:after:content-[''] checked:focus:before:scale-100 checked:focus:before:shadow-[0px_0px_0px_13px_#3b71ca] checked:focus:before:transition-[box-shadow_0.2s,transform_0.2s] checked:focus:after:ml-[0.25rem] checked:focus:after:-mt-px checked:focus:after:h-[0.8125rem] checked:focus:after:w-[0.375rem] checked:focus:after:rotate-45 checked:focus:after:rounded-none checked:focus:after:border-[0.125rem] checked:focus:after:border-t-0 checked:focus:after:border-l-0 checked:focus:after:border-solid checked:focus:after:border-white checked:focus:after:bg-transparent dark:border-neutral-600 dark:checked:border-[#D9A283] dark:checked:bg-[#D9A283]" 
                            type="checkbox" value="" onClick={(event:any) => {
                                setChecked(event.target.checked);
                            }
                                }/>
                            <label className="ml-2 text-px16 sc991:text-px14 text-[#68483C] font-semibold">I agree to the terms of this Privacy& Cookies Policy*</label>
                        </div>
                    </div>
                </form>
            </div>
       {/* Form end */}
   </div>
    );
}