import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import { useTranslation } from "react-i18next";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { useAppSelector } from "../../hooks/hook";
import { Autoplay, FreeMode, Navigation, Thumbs } from "swiper";
import { useState } from "react";

export default function SliderBannerHome() {
    const banner = useAppSelector(state => state.bannerSlice);
    const [t] = useTranslation();
    const [activeThumb, setThumbActive] = useState<any>(null);
    return (
        <div className="relative w-full min-h-full h-full max-w-full max-h-full">
                <Swiper
                slidesPerView={1}
                thumbs={{swiper: activeThumb && !activeThumb.destroyed ? activeThumb : null}}
    
                spaceBetween={0}
                initialSlide={1}

                // slidesPerGroup={1}
                loop={false}
                loopFillGroupWithBlank={true}
                autoplay={{
                delay: 5000,
                disableOnInteraction: false,
                }}

                modules={[ Navigation, Autoplay, Thumbs, FreeMode]}
                className="min-w-full min-h-full !overflow-visible	"
            >

                {
                    banner.banners.map((item, index)=> {            
                        return (

                            <SwiperSlide  key={item.id} className="min-w-full min-h-full !w-[621px] rounded-t-[300px] bg-[#FCF5EF] btn-banner after:top-0 after:left-0 lg:after:top-0 lg:after:left-0">
                                <img className="banner_home_primary w-[621px] h-[732px] rounded-t-[300px] object-cover p-6 z-50" src={item?.imageUrl ?? ""} alt={""}  />
                                <img src="/images/homepages/iconbanner.png" className="absolute -top-[4.5%] left-[45%] z-50" alt="" />
                                <div className="absolute top-[10%] -left-[10%] bg-white px-6 pt-14 rounded-t-[300px] z-50">
                                    <span className="Valky text-[#2F2D38] text-[38px] font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[38px]">{t("home.banner.sale")}</span>
                                    <div className="text-[#2F2D38] Valky lssm:w-fit xl:w-max sm-480:text-[30px] lssm:text-[24px] lg:text-[50px] font-bold uppercase xl:min-text-[96px]  2xl:text-[96px] w-1920:text-[96px] relative z-[1] animate__animated animate__fadeInDown">
                                        {t("home.banner.50")}
                                    </div>
                                </div>
                            </SwiperSlide>  
                    
                        )                 
                    })
                }
                    
                
            </Swiper>
                <div className="absolute max-w-fit  bottom-1 left-[50%] translate-x-[-50%] z-[3]">
                    
                <Swiper
                    slidesPerView={banner.banners.length}
                    // spaceBetween={30}
                    // pagination={{
                    //   clickable: true,
                    // }}
                    // navigation={true}
                    initialSlide={1}
                    freeMode={true}
                    onSwiper={setThumbActive}
                    watchSlidesProgress={true}
                    modules={[ Navigation, Thumbs, FreeMode]}
                    className="h-[20px]   swiper-banner-home"
                >
                    {
                        banner.banners.map((item:any, index) => {
                            return   <SwiperSlide key={index} className="w-full h-[3px]">
                                            <div className="w-[12px] rounded-[50%] h-[12px] mr-[12px] bg-[#ccc] cursor-pointer">

                                            </div>
                                    </SwiperSlide>
                        })
                    }
                </Swiper>
                </div>
        
        </div>
    )
}