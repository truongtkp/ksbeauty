
import clsx from "clsx";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { getSupplier } from "../../reducers/supplier";
import { FormContact } from "../../types";
import { hostBE } from "../../types/host";
import ContactItem from "../ContactItem";

export default function MultiContact() {
  const dispatch = useAppDispatch();
  const supplierData = useAppSelector((state) => state.supplierSlice);

  useEffect(() => {
    dispatch(getSupplier({ page: 0, limit: 9 }));
  }, []);

  const handleLoop = (item: FormContact, index: any) => {
    for (let i = 0; i < index; i++) {
      if (item.social === supplierData.suppliers[i].social) return false;
    }

    return true;
  };

  // {"bg-[#BC8A79]":type==="GMAIL", "bg-[#BC8A79]":type==="SKYPE", "bg-[#BC8A79]":type==="PHONE", "bg-[#BC8A79]":type==="ZALO", "bg-[#BC8A79]":type==="FACEBOOK"}

  return (
    <div className="fixed top-[50%] translate-y-[-50%] sc>768:left-[24px] md:right-[24px] z-20">
      <div>
        {supplierData.suppliers.map((item, index) => {
          return (
            handleLoop(item, index) && (
              <div
                key={item.id}
                className="my-[50px] cursor-pointer relative lssm:p-[10px] md:p-[15px] bg-white-color contact_parent w-auto h-auto box-shadow rounded-[100rem]"
                style={{
                  ["--color" as string]:
                    item.social === "GMAIL"
                      ? "#BC8A79"
                      : item.social === "SKYPE"
                      ? "#BC8A79"
                      : item.social === "PHONE"
                      ? "#BC8A79"
                      : item.social === "ZALO"
                      ? "#BC8A79"
                      : "#BC8A79",
                }}
              >
                <img
                  className={clsx("lssm:w-[20px] md:w-[32px] object-cover")}
                  src={`${hostBE}/fe/${
                    item.social === "FACEBOOK"
                      ? "facebook_contact"
                      : item.social === "PHONE"
                      ? "phone_contact"
                      : item.social === "GMAIL"
                      ? "mail_contact"
                      : item.social === "SKYPE"
                      ? "sky_contact"
                      : "zalo_contact"
                  }.svg`}
                  alt=""
                />
                <div className="absolute top-[-75%] lssm:left-[50px] md:left-[-350px] contact_children sc>768:left-[-300px]">
                  {supplierData.suppliers.map((supplier) => {
                    return (
                      supplier.social === item.social && (
                        <ContactItem
                          key={supplier.id}
                          type={supplier.social}
                          url={supplier.avatarUrl}
                          name={supplier.fullname}
                          contact={supplier.link}
                        />
                      )
                    );
                  })}
                </div>
              </div>
            )
          );
        })}
      </div>
    </div>
  );
}
