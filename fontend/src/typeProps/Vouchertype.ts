import { Option } from "../types";

export type searchVoucherParam = {
    type: string;
    keySearch: string;
    option: Option;
  }
  
  export type voucherType = {
    id: number;
    voucher: string
    orderStatus: boolean
    to: string
    from: string
    email: string
    phoneNo: string
    expirationDate: string
  }

  export type  typeVoucherEdit = {
    voucher: string
    orderStatus: boolean
    to: string
    from: string
    email: string
    phoneNo: string
    expirationDate: string
  }
  
  export type voucherTypePost = {
    voucher: string
    orderStatus: boolean
    to: string
    from: string
    email: string
    phoneNo: string
    expirationDate: string
  }
  
  export type voucherResult = {total:number, list: voucherType[]}