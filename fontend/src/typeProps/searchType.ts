import { Option } from "../types";

export type listSearch = {
  total: number;
  list: [];
};

export type SearchParam = {
  type: string;
  param: string;
  option: Option;
};
