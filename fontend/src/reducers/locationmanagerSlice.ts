import { ConsoleSqlOutlined } from "@ant-design/icons";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { locationResult, locationType, searchLocationParam } from "../typeProps/Locationtype";
import { Option, Location } from "../types";

interface locationInterface {
    loadding: boolean;
    error:null | string;
    total: number;
    currentPage: number;
    locationList: locationType[]
}

const initialState: locationInterface= {
    loadding:false,
    error:null,
    total: 0,
    currentPage: 1,
    locationList: []
}

export const  locationManagerSlice = createSlice({
    name: "locationManagerSlice",
    initialState,
    reducers: {
        setCurrenPage(state,action: PayloadAction<number>){
            state.currentPage = action.payload;
        },
        getLocation(state,action: PayloadAction<Option>){
            state.loadding= true;
        },
        getLocationSuccess (state,action:PayloadAction<locationResult>){
            state.total = action.payload.total;
            state.locationList = action.payload.list;
            state.loadding = false;
            
        },
        getLocationFail(state,action:PayloadAction<string>){
            state.error = action.payload;
        },
        searchLocation(state,action:PayloadAction<searchLocationParam>){
            state.loadding = true;
        },
        searchLocationSuccess(state,action:PayloadAction<locationResult>){
            state.total = action.payload.total;
            state.locationList = action.payload.list;
            state.loadding = false;
        },
        editLocation (state,action:PayloadAction<Location>){
           
        },
        editLocationSuccess(state,action:PayloadAction<locationType>){
            const newLocation = state.locationList.map((item,index)=> {
                if(item.id === action.payload.id){
                    item = action.payload
                }
                return item;
            })
            state.locationList = newLocation
        },
        putPriorityLocation (state,action:PayloadAction<number>){
        },
        putPrioritySuccess(state,action:PayloadAction<locationType>){
            const newState = state.locationList.map((item,index)=> {
                if(item.id === action.payload.id){
                    item = action.payload
                }
                return item
            })
            state.locationList = newState;
        }
    }
});


export const {setCurrenPage, getLocation,getLocationSuccess,getLocationFail,searchLocation,searchLocationSuccess,editLocation,editLocationSuccess,putPriorityLocation,putPrioritySuccess} = locationManagerSlice.actions;

export default locationManagerSlice.reducer;