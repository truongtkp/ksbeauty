import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import Button from "../../components/Button";
import SliderBannerHome from "../../components/SliderBannerHome";
import { useAppDispatch } from "../../hooks/hook";
import { getBanner } from "../../reducers/banner";

export default function Banner() {
  const [t] = useTranslation();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getBanner());
  }, []);

  return (
    <div className="flex justify-between items-center lssm:px-[24px] md:px-[80px] xl:px-[50px] w-1920:px-[162px] relative pt-[3%] z-50">
      <div className="w-full pr-[10%]">
        <div className="text-[#2F2D38] Valky lssm:w-fit xl:w-max sm-480:text-[30px] lssm:text-[24px] lg:text-[50px] font-bold uppercase xl:min-text-[96px]  2xl:text-[96px] w-1920:text-[96px] relative z-[1] animate__animated animate__fadeInDown">
           {t("home.banner.care")}
           <p className="">{t("home.banner.your")} </p>
        </div>
        <p className="lssm:text-px14 text-[#2F2D38] md:text-px20 mb-[5%] lssm:leading-4 md:leading-7 sc991:text-white-color animate__animated animate__fadeInUp">
          {t("home.banner.description")}
        </p>
        <Button
          onClick={() => navigate("/lien-he")}
          color="primary"
          className="sc>768:text-px14 text-px16 sc<992:mb-[72px] sc991:mx-auto sc991:mb-6 w-1920:h-[60px] 2xl:h-[55px] xl:h-[50px] lg:h-[45px] m992:h-[45px] md:h-[45px] sm:h-[40px] sm-480:h-[40px] rounded-[5px] w-1920:w-[200px] 2xl:w-[190px] xl:w-[180px] lg:w-[180px] m992:w-[180px] md:w-[180px] sm:w-[170px] sm-480:w-[170px] shadow-md"
        >
          <span className="flex items-center text-inherit font-medium w-1920:text-px20 2xl:text-px18 m992:text-px16 md:text-px16">
          {t("button.see_more")}
          </span>
        </Button>
      </div>
      <div className="">
        <SliderBannerHome />
      </div>
        <img src="/images/homepages/beautymark.png" className="absolute -bottom-[100%] left-0 z-0" alt="" />
    </div>
    // <div className="flex items-center justify-center text-center banner_home_primary relative text-white-color">
    //   <div className="flex flex-col items-center justify-end pb-[37px] h-full my-auto  sc991:absolute z-[2] lssm:px-[24px] sm:px-[40px] md:px-[80px] lg:px-[120px] 2xl:px-[242px]">
    //     <div className="text-[#85512B] Valky lssm:w-fit xl:w-max sm-480:text-[30px] lssm:text-[24px] lg:text-[50px] font-bold uppercase xl:min-text-[96px]  2xl:text-[96px] w-1920:text-[96px] relative z-[1] animate__animated animate__fadeInDown">
    //       {t("home.banner.title")}
    //     </div>
    //     <p className="lssm:text-px14 md:text-px20 sc>768:px[24px] lssm:mb-[14px] md:mb-[28px] lssm:leading-4 md:leading-7 sc991:text-white-color animate__animated animate__fadeInUp">
    //       {t("home.banner.description")}
    //     </p>
    //     <Button
    //       onClick={() => navigate("/lien-he")}
    //       color="primary"
    //       className="sc>768:text-px14 2xl:text-[32px] xl:text-[24px] max-lg:text-[20px] uppercase 2xl:px-[34px] 2xl:py-[14px] lg:px-[28px] lg:py-[10px] max-w-fit mb-4 shadow-xl"
    //     >
    //       {t("home.banner.button")}
    //     </Button>
    //   </div>
    //   <div className="w-full max-w-full min-h-[100%] inset-0 absolute overflow-hidden banner_home-after">
    //     <SliderBannerHome />
    //   </div>
    // </div>
  );
}
