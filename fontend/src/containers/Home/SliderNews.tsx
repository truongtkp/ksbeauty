import { Swiper, SwiperSlide } from "swiper/react";
import clsx from "clsx";
import "swiper/css";
import "swiper/css/pagination";
import { useTranslation } from "react-i18next";
import Button from "../../components/Button/index";
import "swiper/css/navigation";
import { Autoplay, Navigation, Grid } from "swiper";
import React, { useMemo, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { useNavigate } from "react-router-dom";
import useInView from "../../hooks/useInView";
import {
    MdOutlineArrowBackIosNew,
    MdOutlineArrowForwardIos,
  } from "react-icons/md";
import { getProductPublic } from "../../reducers/productPublic";

export default function SliderNews() {

    const { ref, isInView } = useInView();
    const dispatch = useAppDispatch();
    const productList = useAppSelector((state) => state.productPublic);
    const navigate = useNavigate();
    const [t] = useTranslation();
    const { categoryList, categoryFilter } = useAppSelector(
      (state) => state.categorySlice
    );
    const numberCategory = categoryFilter.length > 0 ? categoryFilter[0] : 0;
    const width = useMemo(() => {
      return window.innerWidth;
    }, []);

    useEffect(() => {
      dispatch(
        getProductPublic({
          type: "en",
          search: "",
          categoryId: numberCategory,
          page:0,
          limit: 9,
          sort: 0,
        })
      );
    }, [])
    

    const navigationPrevRef = React.useRef(null);
    const navigationNextRef = React.useRef(null);

    return (
        <div className="flex-1 flex items-center lssm:px-[24px] md:px-[80px] xl:px-[50px] w-1920:px-[162px] relative mt-10">
                  <Swiper
        slidesPerView={
          width >= 1850
            ? 2
            : width >= 992
            ? 2
            : width >= 380
            ? 1
            : width >= 330
            ? 1
            : 1
        }
        spaceBetween={30}
        // slidesPerGroup={1}
        loop={false}
        loopFillGroupWithBlank={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        navigation={{
          prevEl: navigationPrevRef.current,
          nextEl: navigationNextRef.current,
        }}
        onSwiper={(swiper: any) => {
          setTimeout(() => {
            swiper.params.navigation.prevEl = navigationPrevRef.current;
            swiper.params.navigation.nextEl = navigationNextRef.current;

            swiper.navigation.destroy();
            swiper.navigation.init();
            swiper.navigation.update();
          });
        }}
        modules={[Navigation, Autoplay]}
        className="slider-company-home"
      >
        {productList.productPublicList.map((item, index) => {
          if ((index + 1) % 2 === 0 && width < 768) return "";
          return width < 768 ? (
            <SwiperSlide className="slider-company-home-item " key={item.id}>
              <div className="sc>768:max-h-[74px]">
                <img
                  src={item?.avatarUrl ?? ""}
                  className="w-full"
                  alt={item?.avatarUrl ?? ""}
                />
              </div>
              {
                index + 1 < productList.productPublicList.length && (
              <div className="mt-[24px] sc>768:max-h-[74px]">
                <img
                  src={productList.productPublicList[index + 1]?.avatarUrl ?? ""}
                  className="w-full"
                  alt={productList.productPublicList[index + 1]?.avatarUrl ?? ""}
                />
              </div>

                )
              }
            </SwiperSlide>
          ) : (
            <SwiperSlide className="slider-company-home-item" key={item.id}>
              <div className="">
                <img className="w-1920:w-[300px] 2xl:w-[140px] xl:w-[120px] lg:w-[100px] m992:w-[90px] sc991:w-[60px] w-1920:h-[300px] 2xl:h-[140px] xl:h-[120px] lg:h-[100px] m992:h-[90px] sc991:h-[60px] rounded-[10%]" src={item?.avatarUrl ?? ""} alt={item?.avatarUrl ?? ""} />
                <div className="flex flex-col !justify-between !items-start ml-5">
                    <p
                    className={clsx(
                        "h-auto font-bold 2xl:mb-5 text-base sm-480:text-xl mb-3 text-bg_blue_bold line-clamp-1",
                        { "animate__animated animate__fadeInDown": isInView }
                    )}
                    >
                    {item?.titleEn ?? ""}
                    </p>
                    <p
                    className={clsx(
                        "sm-480:text-base text-sm h-auto font-normal text-text-gray line-clamp-2"
                    )}
                    >
                    {item?.descriptionEn ?? ""}
                    </p>
                    <Button
                      onClick={() => navigate(`/chitietsanpham/${item?.id}`)}
                      color="primary"
                      className="2xl:mt-3 mt-2 sc>768:text-px14 text-px16 bg-white sc991:mb-6 h-[40px] rounded-[5px] w-1920:w-[150px] 2xl:w-[140px] xl:w-[130px] lg:w-[130px] m992:w-[130px] md:w-[130px] sm:w-[120px] sm-480:w-[120px] shadow-md"
                    >
                      <span className="flex items-center text-inherit font-medium w-1920:text-px20 2xl:text-px18 m992:text-px16 lsm-320:text-[12px]">
                        {t("button.see_more")}
                      </span>
                    </Button>
                </div>
              </div>
            </SwiperSlide>
          );
        })}
      </Swiper>

      {/* <div
        ref={navigationPrevRef}
        className="absolute top-[50%] lssm:left-[-24px] md:left-[-50px] lg:left-[-60px]cursor-pointer translate-y-[-50%] text-text-primary sc>768:text-white-color sc>768:text-[24px] md:text-[48px]"
      >
        <MdOutlineArrowBackIosNew />
      </div>
      <div
        ref={navigationNextRef}
        className="absolute top-[50%] lssm:right-[-24px] md:right-[-50px] lg:right-[-60px] cursor-pointer translate-y-[-50%] text-text-primary sc>768:text-white-color sc>768:text-[24px] md:text-[48px]"
      >
        <MdOutlineArrowForwardIos />
      </div> */}

    
        </div>
    )
}