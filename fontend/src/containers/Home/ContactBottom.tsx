import clsx from "clsx";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import Button from "../../components/Button";
import useInView from "../../hooks/useInView";
import PopUpForm  from "../Contact/PopupForm";
import { hostBE } from "../../types/host";
import { showModal } from "../../reducers/modal";
import { useAppDispatch } from "../../hooks/hook";
import SilderNews from "../Home/SliderNews";

export default function ContactBottom() {

    const navigate = useNavigate();
    const [t] = useTranslation();
    const { ref, isInView } = useInView();
    const dispatch = useAppDispatch();

    const showModalContact = () => {
        dispatch(showModal(<PopUpForm />))
      };

    return (
        <div className="lssm:h-auto sc<992:h-[579px] max-w-full my-24" ref={ref}>
            <div className="flex flex-col justify-center items-center">
                <span className="Valky pb-5 text-[#2F2D38] font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[48px]">{t("home.topic.Body")}</span>
                <div
                className={clsx(
                    "lssm:text-px14 w-1920:text-px20 2xl:text-px18 xl:text-px16 md:text-px16 sc<992:mb-[32px] sc991:mb-3 text-[#64483B] text-justify",
                    { "animate__animated animate__flash": isInView }
                )}
                dangerouslySetInnerHTML={{ __html: t("home.body_sub") }}
                ></div>
            </div>
            <SilderNews/>
        </div>
    )

}