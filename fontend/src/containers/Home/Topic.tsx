import clsx from "clsx";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import SliderProject from "../../components/Home/SliderProject";
import TopicItem from "../../components/Home/Topic";
import { useNavigate } from "react-router-dom";
import Button from "../../components/Button";
import useInView from "../../hooks/useInView";
import TopicProduct from "../../components/Home/TopicProduct";
import TopicProductMobile from "../../components/Home/TopicProductMobile";
import { hostBE } from "../../types/host";

export default function Topic() {
  const [t] = useTranslation();
  const navigate = useNavigate();
  const { ref, isInView } = useInView()

  return (
    <div>
      <div className="w-full">
        <TopicItem />
        <div className="relative my-24 lssm:px-[24px] md:px-[80px] xl:px-[50px] w-1920:px-[162px]">
          <div className="lssm:h-auto relative sc<992:h-[690px] z-10 lg:block max-lgs:hidden">
            <TopicProduct />
          </div>
          <div className="lssm:h-auto relative sc<992:h-[690px] lssm:pt-[30px] md:pt-[50px] z-10 lg:hidden max-lgs:block">
            <TopicProductMobile />
          </div>
        </div>

        <div
          className="relative aspect-[1920/547] z-50"
          // className="relative w-1920:h-[100vh] 2xl:h-[100vh] xl:h-[100vh] m992:h-[95vh] md:h-[91vh] sm:h-[58vh] sm-480:h-[45vh] sm-390:h-[43vh] lsm-380:h-[37vh] lsm-320:h-[35vh]"
          style={{
            backgroundImage: `url('${hostBE}/fe/young-woman.png')`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}>

          <div className="absolute bottom-0 right-0 w-full">
              <div className="w-full opacity-30 bg-[#000000] aspect-[1920/547]"></div>
          </div>
          <div className="absolute top-[20%] right-0 w-full">
                <div className="-mt-4 pr-0">
                  <div
                    className={clsx(
                      "text-center w-1920:pt-14 2xl:pt-12 text-text-title font-bold uppercase 2xl:leading-[35px] md:text-[32px] xl:text-[34px] 2xl:text-[48px] mb-[2%]",
                      { "animate__animated animate__fadeInRight": isInView }
                    )}
                  >
                    <span className="Valky 2xl:pb-[28px] sc991:pb-[8px] 2xl:pt-10 lg:pb-0 pb-6 lsm-380:pb-0 text-[#FFFFFF] w-1920:text-[128px] 2xl:text-[60px] xl:text-[40px] m992:text-[45px] sc991:text-[60px] md:text-[48px] lg:text-[25px] sm-480:text-[20px] sm:text-[50px] sm-390:text-[25px] lsm-380:text-[20px] lsm-320:text-[16px]">{t("home.topic.topic4")}</span>
                  </div>
                </div>
                <div className={clsx("xl:w-[60%] text-center mx-auto")}>
                  <div
                    className={clsx(
                      "sm-480:text-[10px] md:text-[14px] m992:text-px14 sm:text-px14 xl:text-px16 w-1920:text-px20 2xl:text-px18 sc<992:mb-[3%] sc991:mb-4 2xl:pt-[5px] lg:pt-0 sm-480:pt-[14px] sm-390:text-[8px] lsm-380:text-[8px] lsm-320:text-[8px] text-[#FFFFFF] text-center",
                      { "animate__animated animate__flash": isInView }
                    )}
                    dangerouslySetInnerHTML={{ __html: t("home.description_sub") }}
                  ></div>
                  <div className="flex justify-center">
                    <Button
                      onClick={() => navigate("/lien-he")}
                      color="primary"
                      className="sc>768:text-px14 text-px16 sc<992:mb-[72px] !bg-white sc991:mb-6 w-1920:h-[60px] 2xl:h-[55px] xl:h-[50px] lg:h-[45px] m992:h-[45px] md:h-[45px] sm:h-[40px] sm-480:h-[40px] rounded-[5px] w-1920:w-[150px] 2xl:w-[140px] xl:w-[130px] lg:w-[130px] m992:w-[130px] md:w-[130px] sm:w-[120px] sm-480:w-[120px] shadow-md"
                    >
                      <span className="flex items-center text-inherit text-primary font-medium w-1920:text-px20 2xl:text-px18 m992:text-px16 lsm-320:text-[12px]">
                        {t("button.booking")}
                      </span>
                    </Button>{" "}
                  </div>
                  <div></div>
                </div>
              </div>
        </div>

        <div
          className="relative aspect-[1920/1800] lg:hidden max-lgs:block"
          // className="relative w-1920:h-[100vh] 2xl:h-[100vh] xl:h-[100vh] m992:h-[95vh] md:h-[91vh] sm:h-[58vh] sm-480:h-[45vh] sm-390:h-[43vh] lsm-380:h-[37vh] lsm-320:h-[35vh]"
          style={{
            backgroundImage: `url('${hostBE}/fe/young-woman.png')`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}>

          <div className="absolute w-full bg-[#0d0c0c] opacity-40 inset-y-0 inset-x-0	"></div>
          <div className="w-[80%] mx-auto translate-y-12">
            <div className="-mt-4 pr-0">
              <div
                className={clsx(
                  "flex flex-col w-1920:pl-52 2xl:pl-40 xl:pl-32 w-1920:pt-2 2xl:pt-12 items-end flex-wrap-reverse text-text-title font-bold uppercase 2xl:leading-[35px] md:text-[32px] xl:text-[34px] 2xl:text-[48px]",
                  { "animate__animated animate__fadeInRight": isInView }
                )}
              >
                <span className="Valky 2xl:pb-[28px] sc991:pb-[8px] 2xl:pt-6 lg:pb-0 pb-6 lsm-380:pb-0 text-[#FFFFFF] w-1920:text-[128px] 2xl:text-[60px] xl:text-[40px] m992:text-[45px] sc991:text-[60px] md:text-[48px] lg:text-[25px] sm-480:text-[20px] sm:text-[50px] sm-390:text-[25px] lsm-380:text-[20px] lsm-320:text-[16px]">{t("home.topic.topic4")}</span>
              </div>
              <div
                className={clsx("flex justify-between items-end text", {
                  "animate__animated animate__fadeInRight": isInView,
                })}
              >
              </div>
            </div>
            <div className={clsx("w-full sc991:mt-[14px] sm-390:mt-0 lsm-380:mt-0 lsm-320:mt-0 xl:mx-auto")}>
              <div
                className={clsx(
                  "sm-480:text-[10px] md:text-[14px] m992:text-px14 sm:text-px14 xl:text-px16 w-1920:text-px20 2xl:text-px18 sc<992:mb-[15px] sc991:mb-4 2xl:pt-[5px] lg:pt-0 sm-480:pt-[14px] sm-390:text-[14px] lsm-380:text-[14px] lsm-320:text-[14px] text-[#FFFFFF] text-justify",
                  { "animate__animated animate__flash": isInView }
                )}
                dangerouslySetInnerHTML={{ __html: t("home.description_submobile") }}
              ></div>
              <div className="sc>768:flex sc>768:justify-center">
                <Button
                  onClick={() => navigate("/lien-he")}
                  color="primary"
                  className="sc>768:text-px14 text-px16 sc<992:mb-[72px] bg-white sc991:mb-6 w-1920:h-[60px] 2xl:h-[55px] xl:h-[50px] lg:h-[45px] m992:h-[45px] md:h-[45px] sm:h-[40px] sm-480:h-[40px] rounded-[5px] w-1920:w-[150px] 2xl:w-[140px] xl:w-[130px] lg:w-[130px] m992:w-[130px] md:w-[130px] sm:w-[120px] sm-480:w-[120px] shadow-md"
                >
                  <span className="flex items-center text-inherit font-medium w-1920:text-px20 2xl:text-px18 m992:text-px16 lsm-320:text-[14px]">
                    {t("button.booking")}
                  </span>
                </Button>{" "}
              </div>
              <div></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
