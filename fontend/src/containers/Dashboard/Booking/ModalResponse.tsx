import { ChangeEvent, FormEvent, useState } from "react";
import { useTranslation } from "react-i18next";
import Button from "../../../components/Button";
import Input from "../../../components/InputImage";
import Radio from "../../../components/Radio";
import { useAppDispatch, useAppSelector } from "../../../hooks/hook";
import { bookingStatusSuccess } from "../../../reducers/bookingmanagerSlice";
import { hideModal } from "../../../reducers/modal";
import { pushPopup } from "../../../reducers/popupSlice";
import bookingService from "../../../services/booking";
import { Booking } from "../../../types";

type Props = {
    booking: Booking
}

export default function ModalResponse ({booking}:Props) {
    const dispatch = useAppDispatch();
    // const [bookingStatus, bookingStatusSuccess] = useState<boolean | undefined>(booking.);
    const [message, setMessage] = useState("");
    const [t] = useTranslation();
    const {isEnglish} = useAppSelector(state => state.translateSlice)

    // const handleChange = (event:ChangeEvent<HTMLTextAreaElement>) => {
    //     setFeedBack(event.currentTarget.value);
    //     setMessage("")
    // }

    // const handleSubmit = (event:FormEvent) => {
    //     event.preventDefault();
    //     if(feedBack.trim() === "") {
    //         setMessage( isEnglish ? "Vui lòng điền phản hồi của bạn" : "Please fill in your feedback")
    //     }else {
    //         contactService.feedBackContact({id: contact.id, feedback: feedBack}).then((data:Contact)=> {
    //             dispatch(feedBackSuccess(data));
    //             dispatch(hideModal())
    //             dispatch(pushPopup({
    //                 type: "SUCCESS",
    //                 message: t("notify.success", {name: t("contact.response")})
    //             }))
    //         }).catch(error => {
    //             dispatch(pushPopup({
    //                 type: "ERROR",
    //                 message: t("notify.error", {name: t("contact.response")})
    //             }))
    //         })
    //     }
    // }

    return (
        <div className="lssm:w-[380px] sm:w-[500px] md:w-[600px] lg:w-[840px]  rounded-[20px] bg-white-color py-[30px] lssm:px-[24px] md:px-[40px] xl:px-[80px]">
                <h2 className="lssm:text-px20 md:text-[32px] text-text-primary font-bold text-center mb-[50px]">{t("dashboard.request.form.titlebooking")}</h2>

                <form className="grid lssm:grid-cols-1 md:grid-cols-2 gap-x-[24px] gap-y-[30px]">
                    <div>
                        <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.addressbooking")}</label>
                        <Input value={booking.address.name} readOnly={true}  />
                    </div>

                    <div>
                        <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.service")}</label>
                        <Input value={booking.servicePackage.title} readOnly={true}  />
                    </div>

                    <div>
                        <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.datetime")}</label>
                        <Input   value={booking.bookingDate.split('T')[0] + " " + booking.timetable} readOnly={true} />
                    </div>

                    <div>
                        <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.staff")}</label>
                        <Input   value={booking.staff.name} readOnly={true} />
                    </div>

                    <div>
                        <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.customername")}</label>
                        <Input   value={booking.fullName} readOnly={true} />
                    </div>

                    <div>
                        <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.phone")}</label>
                        <Input    value={booking.phoneNo} readOnly={true} />
                    </div>

                    <div className="md:col-span-2">
                        <label className="block mb-[12px] text-px16 font-bold text-text-primary">{t("contact.form.email")}</label>
                        <Input    value={booking.email} readOnly={true} />
                    </div>

                    {/* <div className="md:col-span-2">
                        <label className="block mb-[12px] text-px16 font-medium text-text-primary">{t("contact.form.content_request")}</label>
                        <textarea readOnly={true} value={booking} className="border-[1px] px-1 py-1 border-solid border-text-primary outline-none  rounded-[10px] w-full" rows={8}>

                        </textarea>
                    </div> */}
                    <div className="md:col-span-2 flex justify-end">
                    <Button onClick={()=>dispatch(hideModal())} color="empty" className="h-[60px] w-[120px]">{t("button.cancel")}</Button>
                    {/* <Button type="submit" color="primary" className="h-[60px] ml-3 w-[120px]">{t("dashboard.request.form.btn_response")}</Button> */}
                    </div>
                </form>

        </div>
    )
}