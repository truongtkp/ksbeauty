import { useState } from "react";
import { StepperContext } from "../../contexts/StepperContext";
import { useTranslation } from "react-i18next";
import Button from "../../components/Button";
import { useFormik } from "formik";
import * as Yup from "yup"
import contactService from "../../services/contact";
import { useAppDispatch } from "../../hooks/hook";
import { pushPopup } from "../../reducers/popupSlice";
import Stepper from "../../components/Stepper/Stepper";
import StepperControl from "../../components/Stepper/StepperControl";
import Location from "../../components/steps/Location";
import Service from "../../components/steps/Service";
import { useNavigate } from "react-router-dom";
import Confirm from "../../components/steps/Confirm";
import TimeStaff from "../../components/steps/TimeStaff";
import Payment from "../../components/steps/Payment";
import Confirmed from "../../components/steps/Confirmed";
import { hostBE } from "../../types/host";

export default function FormContact() {
  const [t] = useTranslation();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      fullname: '',
      phone: "",
      email: "",
      companyName: "",
      companyAddress: "",
      content: "",
      fromTo: ""
    },
    validationSchema: Yup.object({
      fullname: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.name"), min: 5 })),
      phone: Yup.string().required(t("validate.error.required")).matches(/^[0-9]{10}$/, t("validate.error.pattern", { name: t("contact.form.phone") })),
      email: Yup.string().required(t("validate.error.required")).matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, t("validate.error.pattern", { name: t("career.form.email") })),
      companyAddress: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.address"), min: 5 })),
      companyName: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.name_company"), min: 5 })),
      content: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.help"), min: 5 })),
    })
    ,
    onSubmit: (values) => {
      contactService.postContact(values).then(() => {
        dispatch(pushPopup({
          type: "SUCCESS",
          message: t("contact.form.postSuccess")
        }))

        resetForm()
      }).catch(() => {
        dispatch(pushPopup({
          type: "ERROR",
          message: t("contact.form.postFail")
        }))
      })
    }
  })

  const resetForm = () => {
    formik.resetForm();
  }

  const [currentStep, setCurrentStep] = useState(1);
  const [userData, setUserData] = useState('');
  const [finalData, setFinalData] = useState({
    currentStep: 1,
    next: function (this:any) {
      let newStep = this.currentStep;
      this.currentStep = (newStep <= steps.length)? newStep + 1: newStep;
      setCurrentStep(this.currentStep); // for triggering update
    },
    back: function (this:any) {
      let newStep = this.currentStep;
      this.currentStep = (newStep > 0)? newStep - 1: newStep;
      setCurrentStep(this.currentStep); // for triggering update
    },
    callback: () => {return false;},
    onClickNext: () => {},
    onClickBack: () => {},
    backBool: true,
    location: {
      id: 0,
      name: "",
      address: ""
    },
    service: {
      id: 0,
      title: "",
      price: 0 
    },
    staff: {
      id: 0,
      name: ""
    },
    bookingDate: (() => {const tmpDate = new Date(); tmpDate.setHours(0,0,0,0); return tmpDate})(),
    time: ""
  });

  const steps = [
    "Location",
    "Service",
    "Time & Staff",
    "Complete",
    "Payment",
    "Confirmed"
  ];

  const icons = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F"
  ];

  const handleClickExtra = (direction:any) => {
    direction == "next" ? finalData.onClickNext()
    : finalData.onClickBack() 
    // : (() => {console.log("DFJDKLFJDSFKLJDLKFJDKLSJ")})()
  }

  const handleClick = async (direction: any) => {
    let newStep = currentStep;
    let onSuccess = false;
    let data = finalData;

    if (direction == "next"){
      await new Promise(resolve => {onSuccess = finalData.callback();
        resolve("foo");});
    }
    if (!data.backBool || (direction == "next" && !onSuccess))return;

    data.callback = () => {return false};
    setFinalData(data);
    direction == "next" ? newStep++ : newStep--;
    newStep > 0 && newStep <= steps.length && setCurrentStep(newStep);
  
    // //check if steps are within bounds
  }

  const displaySteps = (step: number) => {
    switch (step) {
      case 1:
        return <Location data={finalData} setter={setFinalData} />
      case 2:
        return <Service data={finalData} setter={setFinalData}/>
      case 3:
        return <TimeStaff data={finalData} setter={setFinalData}/>
      case 4:
        return <Confirm data={finalData} setter={setFinalData}/>
      case 5:
        return <Payment data={finalData} setter={setFinalData} handleClick={handleClick}/>
      case 6:
        return <Confirmed />
      default:
    }
  }

  return (
    currentStep != 6 ? <div className="mt-36 mt-[24px] mb-[134px] mx-[74px] sc>768:mx-[30px]">
      {/* Stepper */}
      <div className="container horizontal relative pt-40">
        <Stepper
          steps={steps}
          currentStep={currentStep}
        />
        {/* Display Components */}

        <div className="mt-4 py-10">
          <StepperContext.Provider value={{
            userData,
            setUserData,
            finalData,
            setFinalData
          }}>
            {displaySteps(currentStep)}
          </StepperContext.Provider>
        </div>
      </div>

      {/* StepperControl */}
      {currentStep != steps.length &&
        <StepperControl
          handleClick={handleClickExtra}
          currentStep={currentStep}
          steps={steps}
        />
      }
    </div> : <div className="w-full h-full text-white px-[95px] py-72"
    style={{
      backgroundImage: `url(${hostBE}/fe/bookingdoja.png)`,
      backgroundRepeat: "no-repeat",
      backgroundSize : "cover"
    }}
    >
      <div className="text-center bg-[#b3a09759] rounded-[10px] px-[32px] py-[72px]">
        <h2 className="leading-[48px] text-[36px] sm>480:text-[14px] text-white">Thank you for your reservation. Your information has been submitted successfully!</h2>
      </div>
      <div className="text-center mt-[25px]">
        <Button
          onClick={() => {
            navigate("/lien-he")
            window.location.reload();
          }}
          color="primary"
          className="sc>768:text-px14 text-px16 uppercase 2xl:px-[165px] py-[10px] lg:px-[28px] max-w-fit shadow-[0_4px_4px_0px_rgba(0,0,0,0.25)] rounded-[10px] m-auto"
        >
          Make another booking
        </Button>
      </div>
    </div>
  );

}