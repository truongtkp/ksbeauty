import { useTranslation } from "react-i18next"
import { FormikValues,useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import {loadScript} from "@paypal/paypal-js"
import { useEffect, useState} from "react";
import paypalService from "../../services/paypal";
import { hideModal } from "../../reducers/modal";
import { useAppDispatch } from "../../hooks/hook";

export default function PopUpForm({data,setter}:any) {
    const navigate = useNavigate();
    const [checked, setChecked] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const dispatch = useAppDispatch();

    const [paypalGenerated, setPaypalGenerated] = useState(false);
    // const [paypalCallback, setPaypalData] = useState({
    //     callback: (values:FormikValues) => {
    //         console.log("no good");
    //     }
    // }); 

    const [t] = useTranslation();
    const dateToLocalTimeISOString = (param:Date) => {
        return new Date(param.getTime() - (param.getTimezoneOffset() * 60000)).toISOString();
    }
    let reg = "";

    // const clientId = "AUIedOs8C2i7GhroqNCuxF1Rkay0KlcfdyJ876DSoEpPQ84w-wE7U9M-oEOa3xWELhPp5E_TbMQUDfmK";
    const formik = useFormik({
        initialValues: {
            phone: "",
            email: "",
            isValidated: false
        },
        validationSchema: Yup.object({
            phone: Yup.string().required(t("validate.error.required")).matches(/^[0-9]{10}$/, t("validate.error.pattern", { name: t("contact.form.phone") })),
            email: Yup.string().required(t("validate.error.required")).matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, t("validate.error.pattern", { name: t("career.form.email") })),
            // notcontente: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.help"), min: 5 })),
        }),
        onSubmit: (values) => {
            if (!checked){
                console.log("not checked");
                return;
            }
            values.isValidated = true;
            console.log("Validation Successful!");


            // let temp = cacheValue;
            // temp.phone = values.phone;
            // temp.email = values.email;
            // setCacheValue(temp);
            // paypalCallback.callback(values)
        }
    })

    const generateSdkScript = (token:any) => {
        loadScript({
        //   "sdkBaseURL":"https://www.paypal.com/sdk/js",
        //   components: "buttons,hosted-fields",
          "client-id": token.client_id,
          "data-client-token": token.client_token,
        //   "currency": "NZD"
          "currency": "USD"
        
        }).then((paypal) => {



        //     // Using HOSTED FIELD
        //   if (!paypal?.HostedFields?.isEligible()){
        //     console.log("NOT ELIGABLE");
        //     return;
        //   }
          
        //   console.log("HOSTED FIELDS IS ELIGABLE");
        //   paypal.HostedFields.render(
        //     {
        //       createOrder: () => {
        //         setIsLoading(true);
        //         return fetch("/api/order-voucher", {
        //             method: "POST",
        //             headers: {
        //                 'Accept': 'application/json',
        //                 'Content-Type': 'application/json'
        //             },
        //             body: JSON.stringify({
        //                 to: data.to,
        //                 from: data.from,
        //                 email: cacheValue.email,
        //                 phoneNo: cacheValue.phone,
        //                 voucherId: parseInt(data.voucher.id),
        //                 expirationDate: dateToLocalTimeISOString(data.expirationDate).split('T')[0]
        //             })
        //         }).then((res) => {
        //           return res.json();
        //         }).then((orderData) => {
        //             // data.orderId = orderData.orderId;
        //             console.log(orderData);
        //         //   temp = orderData.orderId;
        //             reg = orderData.orderId;
        //           return orderData.orderId;
        //         }).catch((err) => {
        //             alert(JSON.stringify(err));
        //             // setIsLoading(false);
        //             // return data.orderId; // BUG: A bit hacky here to trick react compiler
        //             console.log(cacheValue);
        //             return "ADFDFDSFDSFSDFSDFSD"; // BUG: A bit hacky here to trick react compiler
        //         });



        //       },
        //       fields: {
        //         number: {
        //           selector: "#card-number",
        //           placeholder: "1111 1111 1111 1111"
        //           // value: "test"
        //         },
        //         cvv: {
        //           selector: "#cvv",
        //           placeholder: "123"
        //         },
        //         expirationDate: {
        //           selector: "#expiration-date",
        //           placeholder: "00/0000"
        //         }
        //       }
        //     }
        //   ).then((cardField) => {

        //     document?.querySelector("#card-form")?.addEventListener("submit", (event) => {
        //       event.preventDefault();
        //       formik.submitForm(); // update value
        //     })

        //     let paypalTemp = paypalCallback;

        //     paypalTemp.callback = (values:FormikValues) => {
        //         cardField.submit().then((res) => {
        //             fetch ('/api/order-voucher/confirm/' + reg, {
        //                 method: "POST",
        //                 headers: {
        //                     'Accept': 'application/json',
        //                     'Content-Type': 'application/json'
        //                 },
        //                 body: JSON.stringify({
        //                     to: data.to,
        //                     from: data.from,
        //                     email: cacheValue.email,
        //                     phoneNo: cacheValue.phone,
        //                     voucherId: parseInt(data.voucher.id),
        //                     expirationDate: dateToLocalTimeISOString(data.expirationDate).split('T')[0]
        //                 })
        //             }

        //             ).then((res) => {
        //                 if (res.status == 200) {
        //                     alert("successful");
        //                     setIsLoading(false);
        //                     window.location.reload();
        //                 } ;
        //             }).catch((err) => {
        //                 alert(JSON.stringify(err));
        //                 setIsLoading(false);
        //             })
        //         }).catch((err) => {
        //             alert(JSON.stringify(err));
        //             setIsLoading(false);
        //         })
        //     }


        paypal?.Buttons?.({
            // Sets up the transaction when a payment button is clicked
            createOrder: async function () {
                  formik.values.isValidated = false;
                  await formik.submitForm();
                  if (!formik.values.isValidated){
                      throw Error("Validation Failed");
                  }

                return fetch("/api/order-voucher", {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        to: data.to,
                        from: data.from,
                        email: formik.values.email,
                        phoneNo: formik.values.phone,
                        voucherId: parseInt(data.voucher.id),
                        expirationDate: dateToLocalTimeISOString(data.expirationDate).split('T')[0]
                    })
                }).then((res) => {
                    return res.json();
                }).then((orderData) => {
                    // data.orderId = orderData.orderId;
                    reg = orderData.orderId;
                    return orderData.orderId;
                  }).catch((err) => {
                      alert(JSON.stringify(err));
                      setIsLoading(false);
                  })
                },
                onError: function (err) {
                  setIsLoading(false);
                },
  
            // Finalize the transaction after payer approval
            onApprove: function () {
                console.log("PROCESS APPROVE");
  
              return fetch ('/api/order-voucher/confirm/' + reg, {
                  method: "POST",
                  headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                  },
                    body: JSON.stringify({
                        to: data.to,
                        from: data.from,
                        email: formik.values.email,
                        phoneNo: formik.values.phone,
                        voucherId: parseInt(data.voucher.id),
                        expirationDate: dateToLocalTimeISOString(data.expirationDate).split('T')[0]
                    })
              }).then((res) => {
                  if (res.status == 200) {
                       alert("successful");
                       setIsLoading(false);
                       window.location.reload();
                      return;
                  };
                  alert(JSON.stringify(res));
              })
            },
          })
          .render("#paypal-button-container");

          }).then((res) => {
            data.backBool = true;
            setter(data);
        })
        .catch((err) => {
          console.log("Failed to generate paypal SDK",err);
        }).finally(() => {
            setIsLoading(false);
        })
    };

    useEffect(() => {
        if (!paypalGenerated) {
            paypalService.getTokenIdentity().then((res) => {
                generateSdkScript(res);
            })
            setPaypalGenerated(true);
        }
    },[]);


    const handleFieldChange = function (event:any) {
        formik.handleChange(event);
        (formik.values as any)[event.target.name] = event.target.value;
        // console.log(formik.values);

    }


    return (
        <div className="relative bg-[#F9F3EF] lssm:p-[18px] sc991:mx-1 xl:p-[30px] lg:p-[35px] xl:pb-20 lg:pb-34 w-max lssm:mt-[30px] lg:mt-0 border w-[98%] bg-white text-darkBlue  shadow rounded-[10px]">
                { isLoading && <div className="fixed top-0 left-0 h-screen w-full bg-[#0003]/20 z-50 flex justify-center">
                <button type="button" className=" transition ease-in-out duration-150 " disabled>

                <svg className="animate-spin -ml-1 mr-3 h-40 w-40 text-gray-400" fill="none" viewBox="0 0 24 24">
                    <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                    <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                </svg>

                    {/* <svg className="animate-spin h-5 w-5 mr-3 ..." viewBox="0 0 24 24"></svg> */}
                    {/* Processing... */}
                </button>
                </div> }



                <img src="/images/GitDecor.png" className="absolute 2xl:top-36 lg:top-28 2xl:right-36 m992:right-16 xl:top-28 m992:top-28 sc991:top-28 w-[15%] xl:right-16 lg:right-16 w-1920:top-32 sc991:right-14 w-1920:right-36 xl:top-18 right-0" alt="PopUp Image" />
                <img src="/images/GitDecor2.png" className="absolute 2xl:bottom-8  xl:w-[80%] lg:w-[70%] bottom-6 m992:w-[65%] sc991:w-[75%] lg:right-32 m992:right-24" alt="PopUp Image" />
                <div className="w-1920:w-[1000px] 2xl:w-[850px] xl:w-[850px] m-auto">
                <div className="">
                    <img src="/images/Gift.png" className="xl:w-[30%] lg:w-[30%] m992:w-[35%] sc991:w-[35%]" alt="logo" />
                </div>
                <div className="w-1920:w-[700px] 2xl:w-[650px] xl:w-[600px] m-auto">
                <form onSubmit={formik.handleSubmit} className="lssm:p-[28px] lg:p-18 xl:px-4 flex flex-col justify-center items-center">
                    <div className="mt-8 w-full z-10">
                        <div className="mt-[16px] mb-3 flex justify-start items-center">
                            <h2 className="text-[#BF8782] w-1920:text-px20 2xl:text-px20 xl:text-px16 font-semibold mr-6"> {t("button.amount")} </h2>
                            <p className="text-[#BF8782] w-1920:text-px20 2xl:text-px20 xl:text-px16 m992:text-px16 font-semibold"> {data.voucher.price} NZ$</p>
                        </div>
                        <div className="mt-[20px] mb-3 flex justify-start  items-center">
                            <label className="form-label w-[15%] w-1920:text-px20 2xl:text-px20 xl:text-px16 sc991:text-px14 font-semibold text-[#68483C]">Email</label>
                            <div className="flex-1">
                                <input
                                    type="text"
                                    name="email"
                                    placeholder=""
                                    value={formik.values.email}
                                    onChange={handleFieldChange}
                                    className="w-full h-[40px] bg-transparent border-b border-complementary-000 outline-none"
                                />
                                <span className="text-[14px] text-red-600 mt-1">
                                    {formik.errors.email}
                                </span>
                            </div>
                        </div>

                        <div className="mt-[16px] mb-5 flex justify-start  items-center">
                            <label className="form-label w-[15%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-semibold text-[#68483C]">Phone</label>
                            <div className="flex-1">
                                <input
                                    type="text"
                                    name="phone"
                                    placeholder=""
                                    value={formik.values.phone}
                                    onChange={handleFieldChange}
                                    className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                                />
                                <span className="text-[14px] text-red-600 mt-1">
                                    {formik.errors.phone}
                                </span>
                            </div>
                        </div>

                        <div className="mt-[24px] mb-[24px] flex justify-start items-center">
                            <h2 className="text-[#68483C] w-1920:text-px20 2xl:text-px20 m992:text-px16 xl:text-px16 font-semibold"> {t("button.title")} </h2>
                        </div>

                        {/* <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-semibold text-[#68483C]">Name card</label>
                            <input
                                type="email"
                                name="email"
                                placeholder=""
                                // value={formik.values.email}
                                // onChange={formik.handleChange}
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            />
                            <span className="text-[14px] text-red-600 mt-1">
                                {formik.errors.email}
                            </span>
                        </div> */}




                        {/* <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-semibold text-[#68483C]">Card number</label>
                            <div
                                id = "card-number"
                                placeholder=""
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            />
                        </div>

                        <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-semibold text-[#68483C]">Expiration date</label>
                            <div
                                id = "expiration-date"
                                placeholder=""
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            />
                        </div>

                        <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-semibold text-[#68483C]">Security code</label>
                            <div
                                id= "cvv"
                                placeholder=""
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            />
                        </div> */}

                        <div id="paypal-button-container"></div>

                        
                        <div className="mt-[24px] mb-3 flex items-center">
                            <input className="relative float-left mt-[0.15rem] border-[#D9A283] mr-[6px] h-[1.125rem] w-[1.125rem] appearance-none rounded-[0.25rem] border-[0.125rem] border-solid border-neutral-300 outline-none before:pointer-events-none before:absolute before:h-[0.875rem] before:w-[0.875rem] before:scale-0 before:rounded-full before:bg-transparent before:opacity-0 before:shadow-[0px_0px_0px_13px_transparent] before:content-[''] checked:border-primary checked:bg-primary checked:before:opacity-[0.16] checked:after:absolute checked:after:ml-[0.25rem] checked:after:-mt-px checked:after:block checked:after:h-[0.8125rem] checked:after:w-[0.375rem] checked:after:rotate-45 checked:after:border-[0.125rem] checked:after:border-t-0 checked:after:border-l-0 checked:after:border-solid checked:after:border-white checked:after:bg-transparent checked:after:content-[''] hover:cursor-pointer hover:before:opacity-[0.04] hover:before:shadow-[0px_0px_0px_13px_rgba(0,0,0,0.6)] focus:shadow-none focus:transition-[border-color_0.2s] focus:before:scale-100 focus:before:opacity-[0.12] focus:before:shadow-[0px_0px_0px_13px_rgba(0,0,0,0.6)] focus:before:transition-[box-shadow_0.2s,transform_0.2s] focus:after:absolute focus:after:z-[1] focus:after:block focus:after:h-[0.875rem] focus:after:w-[0.875rem] focus:after:rounded-[0.125rem] focus:after:content-[''] checked:focus:before:scale-100 checked:focus:before:shadow-[0px_0px_0px_13px_#3b71ca] checked:focus:before:transition-[box-shadow_0.2s,transform_0.2s] checked:focus:after:ml-[0.25rem] checked:focus:after:-mt-px checked:focus:after:h-[0.8125rem] checked:focus:after:w-[0.375rem] checked:focus:after:rotate-45 checked:focus:after:rounded-none checked:focus:after:border-[0.125rem] checked:focus:after:border-t-0 checked:focus:after:border-l-0 checked:focus:after:border-solid checked:focus:after:border-white checked:focus:after:bg-transparent dark:border-neutral-600 dark:checked:border-[#D9A283] dark:checked:bg-[#D9A283]" 
                            type="checkbox" value="" onClick={(event:any) => {
                                setChecked(event.target.checked);
                            }
                            }/>
                            <label className="ml-2 text-px16 sc991:text-px14 text-[#68483C] font-semibold">I agree to the terms of this Privacy& Cookies Policy*</label>
                        </div>
                    </div>
                </form>
                    <div className="text-end relative">
                        <button 
                        onClick={() => {
                            dispatch(hideModal())
                        }}
                        className="btn-popup shadow-md w-[134px] h-[24px] sc991:w-[110px] sc991:h-[28px] rounded lg:w-[138px] lg:h-[40px] lg:rounded-[10px] border-[1px] border-[#000000] text-[12px] lg:text-[18px] font-bold text-[#65493A] after:top-1 after:left-1 lg:after:top-[7px] lg:after:left-[7px]" type="submit">
                            {t("button.cancel")}
                        </button>
                    </div>
                </div>
                </div>
            </div>
    )
}