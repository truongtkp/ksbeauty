import { useTranslation } from "react-i18next"
import { useFormik } from "formik";
import { useAppDispatch } from "../../hooks/hook";
import Button from "../../components/Button";
import * as Yup from "yup"
import PopUpForm2  from "../Contact/PopupForm2";
import { showModal } from "../../reducers/modal";
import { useState, useEffect } from "react";
import Calendar from "react-calendar";
import AmountManager from "../../services/amount";

export default function PopUpForm() {
    const [overlay, setOverlay] = useState(false);
    const [priceList, setPriceList] = useState([]);

    const [finalData,setFinalData] = useState({
        to: "",
        from: "",
        phone: "",
        email: "",
        content: "",
        voucher: {
            id: 0,
            price: ""
        },
        orderId: "",
        expirationDate: (() => {
            const tmpDate = new Date();
            tmpDate.setFullYear(tmpDate.getFullYear() + 1);
            tmpDate.setHours(0,0,0,0);
            return tmpDate})(),
    });

    const [t] = useTranslation();
    const dispatch = useAppDispatch();

    const handleDateChange = (param:Date) => {
        // setOverlay(false);
        // if (new Date().getTime() >= param.getTime()){
        //     return;
        // }
        // let temp = finalData;
        // temp.expirationDate = param;
        // setFinalData(temp);
    }

    const formik = useFormik({
        initialValues: {
            to: "",
            from: ""
        },
        validationSchema: Yup.object({
            to: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.name"), min: 5 })),
            from: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.name"), min: 5 })),
            // fullname: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.name"), min: 5 })),
            // phone: Yup.string().required(t("validate.error.required")).matches(/^[0-9]{10}$/, t("validate.error.pattern", { name: t("contact.form.phone") })),
            // email: Yup.string().required(t("validate.error.required")).matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, t("validate.error.pattern", { name: t("career.form.email") })),
            // notcontente: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.help"), min: 5 })),
        }),
        onSubmit: (values) => {
            let temp = finalData;
            temp.to = values.to;
            temp.from = values.from;
            setFinalData(temp);

            dispatch(showModal(<PopUpForm2 data={finalData} setter={setFinalData}/>))
        }
    })

    useEffect(() => {
        AmountManager.get({page: 0,limit: 9}).then((res) => {
            let tmp = finalData;
            tmp.voucher.id = res.list[0].id;
            tmp.voucher.price = res.list[0].price;
            setFinalData(tmp)
            setPriceList((res.list as any));
        })
    },[]); 

    const parsePrice = () => {
        const rows = [];
        for (let i = 0; i < priceList.length; i++){
            const id = (priceList[i] as any).id;
            const price = (priceList[i] as any).price;
            rows.push(
                <option value={id} key={i}>{price}</option>
            );
            // tmp.service.id = priceList[i].
        }
        return rows;
    }

    const handleSelectedPrice = (event:any) => {
        const id = event.target.value;
        const price = event.target.options[event.target.selectedIndex].text;
        let tmp = finalData;
        tmp.voucher.id = id;
        tmp.voucher.price = price;
        setFinalData(tmp);
    }

    // const resetForm = () => {
    //     formik.resetForm();
    // }

    const showModalContact = () => {

        formik.submitForm();
    };

    return (
        <div className="relative bg-[#F9F3EF] lssm:p-[24px] sc991:mx-1 lg:p-[54px] lg:pb-60 w-max lssm:mt-[30px] lg:mt-0 border w-[98%] bg-white text-darkBlue  shadow rounded-[10px]">
            <img src="/images/GitDecor.png" className="absolute 2xl:top-36 lg:top-28 2xl:right-30 m992:w-[15%] xl:top-36 w-[15%] xl:right-16 lg:right-12 m992:right-16 w-1920:top-32 sc991:right-14 sc991:top-14 w-1920:right-20 xl:top-18 right-0" alt="PopUp Image" />
            <img src="/images/GitDecor2.png" className="absolute 2xl:bottom-8 xl:w-[80%] m992:w-[80%] sc991:w-[75%] bottom-8 lg:right-16" alt="PopUp Image" />
            <div className="w-1920:w-[850px] 2xl:w-[850px] xl:w-[850px]">
            <div className="">
                <img src="/images/Gift.png" className="xl:w-[30%] m992:w-[30%] sc991:w-[28%]" alt="logo" />
            </div>
            <div className="w-1920:w-[600px] 2xl:w-[600px] xl:w-[550px] m-auto">
            <form onSubmit={formik.handleSubmit} className="lssm:p-[28px] lg:p-[35px] flex flex-col justify-center items-center">
                <div className="mt-8 w-full z-10">
                    <div className="mb-3 flex justify-between items-center">
                        <label className="form-label w-[25%] w-1920:text-px20 2xl:text-px20 xl:text-px16 m992:text-px16 font-semibold text-[#68483C]">To</label>
                        <div className="flex-1">
                        <input
                            type="text"
                            name="to"
                            placeholder=""
                            value={formik.values.to}
                            onChange={formik.handleChange}
                            className="w-full h-[40px] bg-transparent border-b border-complementary-000 outline-none"
                        />
                            <span className="text-[14px] text-red-600 mt-1">
                                {formik.errors.to}
                            </span>
                            </div> 
                    </div>

                    <div className="mt-[16px] mb-3 flex justify-between items-center">
                        <label className="form-label w-[25%] w-1920:text-px20 2xl:text-px20 xl:text-px16 m992:text-px16 font-semibold text-[#68483C]">From</label>
                            <div className="flex-1">
                            <input
                                type="text"
                                name="from"
                                placeholder=""
                                value={formik.values.from}
                                onChange={formik.handleChange}
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            />
                            <div>
                                <span className="text-[14px] text-red-600 mt-1">
                                    {formik.errors.from}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div className="mt-[16px] mb-3 flex justify-between items-center">
                        <label className="form-label w-[25%] w-1920:text-px20 2xl:text-px20 xl:text-px16 m992:text-px16 font-semibold text-[#68483C]">Amount</label>
                        <div className="flex-1">
                            <select
                                name="amount"
                                placeholder=""
                                // value={formik.values.email}
                                onChange={(val) => handleSelectedPrice(val)}
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            >
                                {parsePrice()}
                            </select>
                            <span className="text-[14px] text-red-600 mt-1">
                                {/* {formik.errors.email} */}
                            </span>
                        </div>
                    </div>

                    <div className="mt-[16px] mb-3 flex justify-between items-center">
                        <label className="form-label w-[33%] w-1920:text-px20 2xl:text-px20 xl:text-px16 m992:text-px16  font-semibold text-[#68483C]">Expires</label>

                        <a
                            href="#" onClick={(e) => {
                                e.preventDefault();
                                // setOverlay(!overlay)
                            }}
                            className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none relative"
                        >
                            {finalData.expirationDate.toDateString()}
                            <img src="/images/calender.png" className="absolute right-0 top-0" alt="PopUp Image" />
                        </a>
                        <span className="text-[14px] text-red-600 mt-1">
                            {/* {formik.errors.content} */}
                        </span>
                    </div>
                    <div className="relative w-[100%]">
                    {/* {overlay && (
                        <div className="w-[20rem] absolute top-0 left-0 items-center bg-white border-2 border-t-border-box">
                            <Calendar
                            value={finalData.expirationDate.toDateString()} onChange={(e:any) => handleDateChange(e)}
                            />
                        </div>
                    )} */}
                    </div>
                </div>
            </form>
                <div className="mt-[16px] text-end relative">
                    <Button 
                    onClick={showModalContact}
                    color="white"
                    className="inline-block btn-popup shadow-md w-[134px] h-[24px] sc991:w-[110px] sc991:h-[36px] m992:h-[32px] rounded lg:w-[138px] lg:h-[40px] lg:rounded-[10px] text-[12px] lg:text-[18px] font-bold text-white bg-[#A68276] after:top-1 after:left-1 lg:after:top-[7px] lg:after:left-[7px]" type="submit">
                        {t("button.continue")}
                    </Button>
                </div>
            </div>
            </div>
        </div>
    )
}