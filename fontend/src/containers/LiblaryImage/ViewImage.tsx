import { MdClear } from "react-icons/md";
import { useAppDispatch } from "../../hooks/hook";
import { hideModal } from "../../reducers/modal";

type Props = {
    url: string;
}

export default function ViewImage({url}:Props) {

    const dispatch = useAppDispatch();

    return <div className="w-[100vw] flex items-center justify-center h-[100vh] relative">
        <div className="absolute top-4 right-4 text-white-color text-px24 cursor-pointer" onClick={()=>dispatch(hideModal())}>
            <MdClear />
        </div>
                <img src={url} alt="view images" className="w-[95%] h-[95%] object-contain"  />
        </div>
}