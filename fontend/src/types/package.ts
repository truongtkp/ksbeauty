export type Package = {   
    id:number,
    title: string,
    price: string,
    description: string
}