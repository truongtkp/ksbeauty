export type User = {
    id?: string
    fullname?:string
   
    email?:string
    phone?:string
    birth?:string
    gender?: boolean
    avatarUrl?:string
    avatarPath?:string
    activated?: boolean
    login?: string
    position?: string
    password?: string
    createdBy?:string
    createdDate?:string
    authorities?: {
        name: "ROLE_ADMIN" | "ROLE_USER" | "ROLE_SYSTEM"
    }
    enabled?: boolean
}

export type LoginPayload = {
    username: string
    password: string
    
    rememberMe?: boolean
}
