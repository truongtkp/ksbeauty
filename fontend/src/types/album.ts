export type Album = {
    id?: number
    descriptionEn: string,
    topicImage?: TopicImage
}

export type TopicImage = {
    id?:number
    imageUrl: string,
    imagePath: string
}