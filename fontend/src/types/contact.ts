

export type Contact = {
    id?: number
    fullname: string
    email: string
    phone: string
    // budget: string
    content: string
    status?: boolean
    feedback?: string
}


export type FormContact = {
    id?: number,
    fullname: string,
    link: string,
    avatarUrl: string,
    avatarPath: string,
    social: "GMAIL" | "PHONE" | "ZALO" | "SKYPE" | "FACEBOOK" | undefined
}