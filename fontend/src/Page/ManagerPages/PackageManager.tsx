import _debounce from "lodash/debounce";
import { Fragment, useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useSearchParams } from "react-router-dom";
import Button from "../../components/Button";
import CartPackage from "../../components/cart/CartPackage";
import DialogConfirm from "../../components/DialogConfirm";
import Loadding from "../../components/Loadding/Loadding";
import InputSearch from "../../components/ManagerComponent/InputSearch";
import TitlePage from "../../components/ManagerComponent/TitlePage";
import Pagination from "../../components/Pagination";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { hideModal, showModal } from "../../reducers/modal";
import { pushPopup } from "../../reducers/popupSlice";
import {
  getPackage,
  searchPackage,
  setCurrenPage,
} from "../../reducers/packagemanagerSlice";
import packageServiece from "../../services/package";
import { hostBE } from "../../types/host";

function PackageManager() {
  const [searchParams] = useSearchParams();
  const { t } = useTranslation();
  const navigator = useNavigate();
  const dispatch = useAppDispatch();
  const { packageList, error, loadding, total, currentPage } = useAppSelector(
    (state) => state.packagemanagerSlice
  );
  const { isEnglish } = useAppSelector((state) => state.translateSlice);
  const [valueSearch, setValueSearch] = useState<string>("");

  const handleChangeInputSearch = (inputData: string) => {
    navigator("");
    dispatch(setCurrenPage(1));
    setValueSearch(inputData);
  };
  const handleAddPackage = () => {
    navigator("add");
  };
  const handleEditPackage = async (id: number) => {
    navigator(`edit/${id}`);
  };
  const deletePackage = async (id: number) => {
    try {
      const result = await packageServiece.deletePackage(id);
      if (result) {
        dispatch(
          pushPopup({
            type: "SUCCESS",
            message: "Delete package successfully.",
          })
        );
        if (currentPage === 1) {
          dispatch(getPackage({ page: currentPage, limit: 12 }));
        } else {
          dispatch(setCurrenPage(1));
        }
        navigator("");
      }
    } catch (error) {
      dispatch(
        pushPopup({
          type: "WARNING",
          message: "Your customer selected this package. Can't delete.",
        })
      );
    }
    dispatch(hideModal());
  };

  const handleDelete = (id: number) => {
    dispatch(
      showModal(
        <DialogConfirm
          message="Do you want to delete it?"
          onClick={() => {
            deletePackage(id);
          }}
        />
      )
    );
  };

  // handle debounce iuputSearch
  const handleDebounceFn = (keySearch: string) => {
    if (keySearch != "") {
      dispatch(
        searchPackage({
          type: "",
          keySearch: keySearch,
          option: {
            page: currentPage - 1,
            limit: 9,
          },
        })
      );
    }
  };
  const debounceFn = useCallback(_debounce(handleDebounceFn, 1000), [
    valueSearch,
    currentPage,
  ]);

  useEffect(() => {
    if (searchParams.get("page")) {
      const numberPage = Number(searchParams.get("page"));
      setCurrenPage(numberPage);
    }
  }, [searchParams]);

  useEffect(() => {
    if (valueSearch != "") {
      if (currentPage == 1) {
        debounceFn(valueSearch);
      } else {
        dispatch(
          searchPackage({
            type: "",
            keySearch: valueSearch,
            option: {
              page: currentPage - 1,
              limit: 9,
            },
          })
        );
      }
    } else {
      dispatch(getPackage({ page: currentPage, limit: 9 }));
    }

    return () => {
      debounceFn.cancel();
    };
  }, [currentPage, valueSearch]);

  return (
    <div className="w-full">
      <TitlePage content="titleManager.packageTitle" />
      <div className="w-full flex sm:flex-row sm:flex-wrap flex-col justify-between items-center px-[10px]">
        <div className="2xl:w-3/4 m992:w-2/3 sm:w-[48%] w-full sm:mb-0 mb-4">
          <InputSearch
            ChangeInputFc={(param) => {
              handleChangeInputSearch(param);
            }}
          />
        </div>
        <div className="2xl:w-[24%] m992:w-[31%] sm:w-[48%] w-full">
          <Button
            color="primary"
            onClick={() => {
              handleAddPackage();
            }}
            className="w-full w-1920:py-[18px] xl:py-[17px] py-[12px] px-4 w-1920:px-10 2xl:px-0 flex "
            disabled={false}
            type="button"
          >
            <img className="mr-3" src={`${hostBE}/fe/add1.png`} alt="add" />
            <p className="sm-480:text-base text-sm font-medium text-white">
              {t("button.button_addPackage")}
            </p>
          </Button>
        </div>
      </div>
      {error ? (
        <div className="w-full sm:py-[60px] py-10 h-[200px] flex items-center justify-center">
          <p className="w-full text-text-red text-lg text-center">{error}</p>
        </div>
      ) : loadding ? (
        <div className="w-full sm:py-[60px] py-10 h-screen flex items-center justify-center">
          <Loadding />
        </div>
      ) : (
        <Fragment>
          <div className="w-full h-auto sm:py-[60px] py-10 flex flex-wrap">
            {packageList.length > 0 ? (
              packageList.map((packages, index) => {
                return (
                  <CartPackage
                    key={index}
                    cartContent={packages}
                    outstanding={true}
                    isGrid={true}
                    onClick={() => handleEditPackage(packages.id)}
                    onClickDelete={() => handleDelete(packages.id)}
                  />
                );
              })
            ) : (
              <div className="w-full min-h-[200px]">
                <p className="text-primary text-xl text-center">
                  Không có sản phẩm nào.
                </p>
              </div>
            )}
          </div>
          <div className="w-full  flex items-center justify-end sm>640:mb-0 mb-24">
            {total > 0 && (
              <Pagination
                total={Math.ceil(total / 9)}
                currenPage={currentPage}
                setCurrentPage={setCurrenPage}
              />
            )}
          </div>
        </Fragment>
      )}
    </div>
  );
}

export default PackageManager;
