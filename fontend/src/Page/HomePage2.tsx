import React from "react";
import ContactBottom from "../containers/Home/ContactBottom";
import SlideCompany from "../containers/Home/SlideCompany";
import Topic from "../containers/Home/Topic";
import MultiContact from "../components/MultiContact";
import Banner from "../containers/Home/Banner";
function HomePage2() {
  return (
    <div className="">
      <div>
        <Banner />
        <div className="relative z-50">
          <Topic />
          <img src="/images/homepages/bannercontent.png" className="absolute -top-[15%] -right-[3%]" alt="" />
          <img src="/images/homepages/bottombanner.png" className="absolute bottom-14 left-0" alt="" />
        </div>
        {/* <SlideCompany /> */}
        <ContactBottom />
      </div>
      <MultiContact />
    </div>
  );
}

export default HomePage2;
