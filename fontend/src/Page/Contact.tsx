import Map from "../components/Map";
import MultiContact from "../components/MultiContact";
import FormContact from "../containers/Contact/Form";


export default function Contact() {


    return (
        <div className="">
            {/* <div className="grid grid-cols-1 xl:grid-cols-2 xl:gap-x-[50px] w-1920:gap-x-[90px]"> */}
            <div className="grid grid-cols-1 xl:grid-cols-2">
                <FormContact />
                <div className="h-[100%]">

                    <Map width={"100%"} height={"100%"} />
                </div>
            </div>
            <MultiContact />
        </div>
    )
}