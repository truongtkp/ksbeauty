import clsx from "clsx";
import { ChangeEvent, useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useSearchParams } from "react-router-dom";
import DialogConfirm from "../../components/DialogConfirm";
import Pagination from "../../components/Pagination";
import ModalResponse from "../../containers/Dashboard/Booking/ModalResponse";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { AiOutlineSearch } from "react-icons/ai";
import { deleteBookingReduce, getBooking, searchBooking, setCurrentPage } from "../../reducers/bookingmanagerSlice";
import { showModal } from "../../reducers/modal";
import bookingService from "../../services/booking";
import { hostBE } from "../../types/host";

import Test from "../Test"

const LIMIT = 9;

export default function Advise() {

    const dispatch = useAppDispatch();

    const bookingData = useAppSelector(state => state.bookingSlice)
    const [value, setValue] = useState<string>();
    const [searchParam, setSearchParam] = useSearchParams();
    const [t] = useTranslation();
    const {isEnglish} = useAppSelector(state => state.translateSlice)


    useEffect(()=> {
        if(value === undefined || value.trim() === "") {
            
            dispatch(getBooking({page: bookingData.currentPage, limit: LIMIT}))

        }else {
            dispatch(searchBooking({keyword: value, option: {page: bookingData.currentPage, limit: LIMIT}}));
        }
    }, [bookingData.currentPage])

    useEffect(() => {
        if(value !== undefined) {
            dispatch(searchBooking({keyword: value, option: {page: 1, limit: LIMIT}}));
            setSearchParam({page: "1"});
        }
    }, [value])

    const showModalResponse = (id:string) => {
        bookingService.getBookingById(id).then((data)=> {
            dispatch(showModal(<ModalResponse booking={data} />))

        })
    }

    const showDialogConfirm = (id:string) => {
        dispatch(showModal(<DialogConfirm message="Do you want to delete it?" onClick={()=>deleteVoucher(id)} />))
    }

    const deleteVoucher = (id:string) => {
        dispatch(deleteBookingReduce(parseInt(id)));
    }

    const total = useMemo(()=> {
        return Math.ceil(bookingData.total/LIMIT);
    }, [bookingData.total])

    const handleChange = (event:ChangeEvent<HTMLInputElement>) => {
        setValue(event.currentTarget.value);
    }

    return (
        <div>
             <h2 className="text-center text-text-primary lssm:text-px20 md:text-[48px] font-bold mt-[74px] mb-[48px] uppercase">{t("dashboard.request.titlebooking")}</h2>

             <div className="w-full h-[60px] border border-solid border-border-color focus-within:border-primary flex items-center rounded-[5px] overflow-hidden">
                <div className="w-[60px] h-full flex items-center justify-center border-r border-r-border-gray">
                    <AiOutlineSearch className="text-bg_blue_bold text-3xl" />
                </div>
                <input value={value ?? ""} onChange={handleChange} className="flex-1 h-full px-2  outline-none border-none" placeholder={"Search..."} />
                
            </div>
            {
            bookingData.bookingList.length > 0 ? (
                <div>
                <div className="mt-[56px] border-2 border-solid border-border-color rounded-[12px] overflow-hidden mb-[60px] overflow-scroll">
                    <table className="dashboard-table w-full">
                        <thead>
                        <tr>
                            <td></td>
                            <td className="whitespace-nowrap px-10">{t("dashboard.request.customername")}</td>
                            <td className="whitespace-nowrap">{t("dashboard.request.service")}</td>
                            <td className="whitespace-nowrap">{t("dashboard.request.staff")}</td>
                            <td className="whitespace-nowrap">{t("dashboard.request.address")}</td>
                            <td className="whitespace-nowrap">{t("dashboard.request.date")}</td>
                            {/* <td>{t("dashboard.request.email")}</td>
                            <td>{t("dashboard.request.phonenumber")}</td> */}
                        </tr>

                        </thead>
                        <tbody>
                        {
                            bookingData.bookingList.map((booking)=> {
                                return (
                            <tr key={booking.id} className="cursor-pointer" >
                                    <td>
                                        <div className="flex justify-center items-center relative z-[2]">
                                        <span className="cursor-pointer" onClick={()=> showDialogConfirm(booking.id+"")}><img src={`${hostBE}/fe/delete_icon.png`} alt="" /></span>
                                        </div>
                                    </td>
                                    <td onClick={()=> showModalResponse(booking.id+"")}>{booking.fullName}</td>
                                    <td onClick={()=> showModalResponse(booking.id+"")}>{booking.servicePackage.title}</td>
                                    <td onClick={()=> showModalResponse(booking.id+"")}>{booking.staff.name}</td>
                                    <td onClick={()=> showModalResponse(booking.id+"")}>{booking.address.name}</td>
                                    <td onClick={()=> showModalResponse(booking.id+"")}>{booking.bookingDate.split('T')[0] + " " + booking.timetable}</td>
                                    {/* <td onClick={()=> showModalResponse(booking.id+"")}>{booking.email}</td>
                                    <td onClick={()=> showModalResponse(booking.id+"")}>{booking.phoneNo}</td> */}
                                    
                            </tr> 
                                )
                            })
                        }

                        </tbody>
                    </table>

                </div>
                    <Pagination currenPage={bookingData.currentPage} setCurrentPage={setCurrentPage} total={total} />

                </div>


            ):<div className="text-center mt-[60px]">No data</div>
            }

            <Test></Test>


        </div>
    )
}