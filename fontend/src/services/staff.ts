import HttpService from "../configs/api";
import getApi from "./getApi";
import { Option, Staff, ResponsiveData } from "../types";
import { staffResult, staffTypePost, staffType, searchStaffParam } from "../typeProps/Stafftype";

const StaffManager = {
    get: (option:Option):Promise<ResponsiveData<Staff>> => {
        const url = getApi("staff");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}})
    },
    getStaffHome: (option:Option):Promise<ResponsiveData<Staff>> => {
        const url = getApi("staff/home");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}});
    },
    postStaff: (data:staffTypePost):Promise<staffType> => {
        const url = getApi(`staff`);
        return HttpService.axiosClient.post(url, data )
    },
    getById: (id:number):Promise<Staff> => {
        const url = getApi("staff/"+id);
        return HttpService.axiosClient.get(url);
    },
    deleteStaff:(id:number):Promise<boolean>=> {
        const url = getApi(`staff/${id}`);
        return HttpService.axiosClient.delete(url)
    },
    editStaff: (data: any):Promise<Staff> => {
        const url = getApi(`staff`);
        return HttpService.axiosClient.put(url, data)
    },
    search: ({keyword, type, option}:{keyword:string, type:string, option:Option}):Promise<ResponsiveData<Staff>> => {
        const url = getApi("staff/search/"+keyword)

        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}});
    },
}

export default StaffManager