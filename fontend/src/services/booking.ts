import HttpService from "../configs/api";
import { Booking, Option, ResponsiveData } from "../types";
import getApi from "./getApi"


const bookingService = {
    postBooking: (data:Booking):Promise<Booking> => {
        const url = getApi("booking");
        return HttpService.axiosClient.post(url, data)
    },
    postBookingSimple: (data:any):Promise<any> => { // FOR PAYPAL PAYMENT
        const url = getApi("booking");
        return HttpService.axiosClient.post(url, data)
    },
    postBookingConfirm: (data:any):Promise<any> => { // FOR PAYPAL PAYMENT
        const url = getApi("booking/confirm/");
        return HttpService.axiosClient.post(url, data)
    },


    editBooking: (data: Booking):Promise<Booking> => {
        const url = getApi(`booking`);
        return HttpService.axiosClient.put(url, data)
    },
    getBookings: (option:Option)=> {
        const url = getApi("booking");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, size: option.limit}})
    },
    getBooking: (option:Option)=> {
        const url = getApi("booking");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, size: option.limit}})
    },
    getBookingById: (id:string):Promise<Booking> => {
        const url = getApi(`booking/${id}`);
        return HttpService.axiosClient.get(url);
    },
    bookingStatusSuccess: (data:any):Promise<Booking> => {
        const url = getApi("booking");
        return HttpService.axiosClient.put(url, data)
    },
    deleteBooking: (id:number):Promise<boolean> => {
        const url =getApi(`booking/${id}`)
        return HttpService.axiosClient.delete(url);
    },
    search: ({keyword, option}:{keyword:string, option:Option}):Promise<ResponsiveData<Booking>> => {
        const url = getApi("booking/search/"+keyword)
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}});
    }
}


export default bookingService