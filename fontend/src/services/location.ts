import HttpService from "../configs/api";
import getApi from "./getApi";
import { Option, Location, ResponsiveData } from "../types";
import { locationResult, locationTypePost, locationType, searchLocationParam } from "../typeProps/Locationtype";

const LocationManager = {
    get: (option:Option):Promise<ResponsiveData<Location>> => {
        const url = getApi(`address`);
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}})
    },
    getLocationHome: (option:Option):Promise<ResponsiveData<Location>> => {
        const url = getApi("address/home");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}});
    },
    postLocation: (data:locationTypePost):Promise<locationType> => {
            const url = getApi(`address`);
            return HttpService.axiosClient.post(url, data )
    },
    getById: (id:number):Promise<Location> => {
        const url = getApi("address/"+id);
        return HttpService.axiosClient.get(url);
    },
    deleteLocation:(id:number):Promise<boolean>=> {
        const url = getApi(`address/${id}`);
        return HttpService.axiosClient.delete(url)
    },
    editLocation: (data: any):Promise<Location> => {
        const url = getApi(`address`);
        return HttpService.axiosClient.put(url, data)
    },
    searchLocation:(data: searchLocationParam):Promise<locationResult> => { 
        const url = getApi(`address/search`);
        return HttpService.axiosClient.get(`${url}/${data.type}/${data.keySearch}`,{params: {page: data.option.page, limit: data.option.limit}});
    },
}

export default LocationManager