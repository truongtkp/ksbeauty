import { BsReverseLayoutTextWindowReverse } from "react-icons/bs";
import {
  AiOutlineMenu,
  AiOutlineInfoCircle,
  AiOutlineShop,
  AiOutlinePicture,
  AiOutlineIdcard,
  AiOutlineUsergroupAdd,
  AiOutlineHome,
} from "react-icons/ai";
import { CgProfile } from "react-icons/cg";

export const navList = [
  {
    path: "/",
    name: "header.menu.home",
    icon: <AiOutlineHome />,
  },
  {
    path: "/gioi-thieu",
    name: "header.menu.introduce",
    icon: <AiOutlineInfoCircle />,
  },
  {
    path: "/sanpham",
    name: "header.menu.product",
    icon: <AiOutlineShop />,

  },
  {
    path: "/lien-he",
    name: "header.menu.contact",
    icon: <AiOutlineIdcard />,
  },
  {
    path: "/album",
    name: "header.menu.photo",
    icon: <AiOutlinePicture />,

  },
  {
    path: "/quanly/thongtintaikhoan",
    name: "header.menu.account",
    icon: <CgProfile />,

  },
];
