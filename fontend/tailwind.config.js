/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        grey: "#2F2D38",
        primary: "#2F2D38",
        primary2: "#F1B290",
        'text-primary': "#85512B",
        'text-grey': "#2F2D38",
        'text-red': "#EC2626",
        'text-lightred': "#F20E0E",
        "text-decoration": "#393939",
        "text-inputSearch": "#C5C5C5",
        'text_pgay': " #848484",
        "text-gray": "#707070",
        'white-color': "#fff",
        'black-color': "#000",
        'border-color': "rgba(0,0,0,.079)",
        'border-red': "rgba(236, 38, 38, 0.5)",
        'border-box': "#EFEFEF",
        'border-gray': "#E2E2E2",
        'bg-overlay': "rgba(255, 255, 255, 0.5)",
        "error_color": "#fe5d5d",
        'bg-grey': "#2F2D38",
        'bg-lightgray': "#E4F1FF",
        'bg-gray01': "#F1F1F1",
        'bg-button-bourcure': "rgba(255, 255, 255, 0.9)",
        'bg-gray': "rgba(99, 99, 99, 0.1)",
        'bg_blue_bold': "#4C1410",
        'bg-table': "rgba(0, 88, 219, 0.04)",
        'bg-accout-hover': "#D9D9D9",
        'text-medium': "rgba(99, 99, 99, 0.8)",
        "bg-banner": "#3366ccb3",
        "text-title": "#11213F",
        "hover_header": "#f0f0f6",
        "border_primary": "#1d70b94d",
        "light-blue": "#E8EEF4",
        "bg-homepage": "#F7EEE9"
      },
      fontSize: {
        px12: "12px",
        px13: "13px",
        px14: "14px",
        px15: "15px",
        px16: "16px",
        px17: "17px",
        px18: "18px",
        px20: "20px",
        px24: "24px",
        px28: "28px",
        px32: "32px",
      }

    },
    screens: {
      "sc>768": { max: "767px" },
      "sc991": { max: '991px' },
      "sc<992": { min: '992px' },
      'lssm': '280px',
      'sm-390': '390px',
      'lsm-320': "320px",
      'lsm-360': "360px",
      'lsm-380': '380px',
      'sm-480': "480px",
      'sm>480': { max: '479.9px'},
      'sm': '640px',
      'sm>640': { max: '639.9px'},
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }
      'm992': '992px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'max-lg': { max: '1024px' },
      // => @media (min-width: 1024px) { ... }

      'max-lgs': { max: '1023.95px' },
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
      'w-1920': '1920px',
      // => @media (min-width: 1920px) { ... }
      "sc2200": { min: '2250px' }
    }
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
  ]
  // important: true
}
