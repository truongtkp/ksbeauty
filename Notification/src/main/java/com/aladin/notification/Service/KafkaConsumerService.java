package com.aladin.notification.Service;

import com.aladin.notification.Entity.KafkaConsumer;
import com.aladin.notification.Repository.KafkaRepository;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumerService {
 private final KafkaRepository kafkaRepository;

    public KafkaConsumerService(KafkaRepository kafkaRepository) {
        this.kafkaRepository = kafkaRepository;
    }
    public KafkaConsumer save(KafkaConsumer kafkaconsumer){
         return  kafkaRepository.save(kafkaconsumer);
    }
}
