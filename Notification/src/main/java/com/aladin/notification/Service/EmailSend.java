package com.aladin.notification.Service;

import com.aladin.notification.Entity.KafkaBooking;
import com.aladin.notification.Entity.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;

@Service
public class EmailSend {
    private final Logger log = LoggerFactory.getLogger(EmailSend.class);
    private final JavaMailSender emailSender;
    public EmailSend(JavaMailSender emailSender){
        this.emailSender = emailSender;
    }

    public void sendSimpleMessage(KafkaConsumer kafkaConsumer){
        try {
            MimeMessage mimeMessage = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

            if (kafkaConsumer.getContent() != null)
                helper.setText(kafkaConsumer.getContent(),true); // Use this or above line.
            if (kafkaConsumer.getSubject() != null)
                helper.setSubject(kafkaConsumer.getSubject());
            if (kafkaConsumer.getRecipients() != null) {
                log.info("Send mail to: "+ kafkaConsumer.getRecipients()+ " and cc:"+ kafkaConsumer.getCc()+ " and to:"+ kafkaConsumer.getToo()+ " and Price:"+ kafkaConsumer.getPrice()+ " and Expires:"+ kafkaConsumer.getExpires());
                helper.setTo(kafkaConsumer.getRecipients());
                emailSender.send(mimeMessage);
            }
        } catch (Exception exception) {
            throw new EmailServiceException(exception);
        }
    }

    public void sendSimpleMessageBooking(KafkaBooking kafkaBooking) {
        try {
            MimeMessage mimeMessage = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

            if (kafkaBooking.getContent() != null)
                helper.setText(kafkaBooking.getContent(),true);
            if (kafkaBooking.getSubject() != null)
                helper.setSubject(kafkaBooking.getSubject());
            if (kafkaBooking.getRecipients() != null) {
                log.info("Send mail to: "+ kafkaBooking.getRecipients() + "and cc:" + kafkaBooking.getCc() + "and booking date:" + kafkaBooking.getBookingDate() + "and subject" + kafkaBooking.getSubject() + "and address" + kafkaBooking.getAddress() + "and recipients name" + kafkaBooking.getRecipientsName() + "and service package" + kafkaBooking.getServicePackage() + "and staff" + kafkaBooking.getStaff() + " ,and time table" + kafkaBooking.getTimetable() + "and content" + kafkaBooking.getContent());
                helper.setTo(kafkaBooking.getRecipients());
                emailSender.send(mimeMessage);
            }

        } catch (Exception exception) {
            throw new EmailServiceException(exception);
        }
    }
}
