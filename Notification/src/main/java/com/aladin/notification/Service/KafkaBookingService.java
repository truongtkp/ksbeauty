package com.aladin.notification.Service;

import com.aladin.notification.Entity.KafkaBooking;
import com.aladin.notification.Repository.KafkaBookingRepository;
import org.springframework.stereotype.Service;

@Service
public class KafkaBookingService {
    private final KafkaBookingRepository kafkaBookingRepository;

    public KafkaBookingService(KafkaBookingRepository kafkaBookingRepository) {
        this.kafkaBookingRepository = kafkaBookingRepository;
    }

    public KafkaBooking save(KafkaBooking kafkaBooking) {
        return kafkaBookingRepository.save(kafkaBooking);
    }
}
