package com.aladin.notification.Config;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
@EnableKafka
@Configuration
public class KafkaConsumerConfig {
    @Value("${kafka.BOOTSTRAP_SERVERS_CONFIG}")
    String BOOTSTRAP_SERVERS_CONFIG;
    @Value("${kafka.GROUP_ID_CONFIG}")
    String GROUP_ID_CONFIG;
    @Value("${kafka.KEY_DESERIALIZER_CLASS_CONFIG}")
    String KEY_DESERIALIZER_CLASS_CONFIG;
    @Value("${kafka.VALUE_DESERIALIZER_CLASS_CONFIG}")
    String VALUE_DESERIALIZER_CLASS_CONFIG;
    @Value("${kafka.ENABLE_AUTO_COMMIT_CONFIG}")
    String ENABLE_AUTO_COMMIT_CONFIG;
    @Value("${kafka.AUTO_COMMIT_INTERVAL_MS_CONFIG}")
    String AUTO_COMMIT_INTERVAL_MS_CONFIG;
    @Value("${kafka.SESSION_TIMEOUT_MS_CONFIG}")
    String SESSION_TIMEOUT_MS_CONFIG;
    @Value("${kafka.AUTO_OFFSET_RESET_CONFIG}")
    String AUTO_OFFSET_RESET_CONFIG;


    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS_CONFIG);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID_CONFIG);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, KEY_DESERIALIZER_CLASS_CONFIG);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, VALUE_DESERIALIZER_CLASS_CONFIG);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, ENABLE_AUTO_COMMIT_CONFIG);
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, AUTO_COMMIT_INTERVAL_MS_CONFIG);
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, SESSION_TIMEOUT_MS_CONFIG);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, AUTO_OFFSET_RESET_CONFIG);
        return new DefaultKafkaConsumerFactory<>(props);
    }
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String>
                factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }
}