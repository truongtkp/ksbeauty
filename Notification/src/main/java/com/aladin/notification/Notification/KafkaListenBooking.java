package com.aladin.notification.Notification;


import com.aladin.notification.Entity.KafkaBooking;
import com.aladin.notification.Service.EmailSend;
import com.aladin.notification.Service.KafkaBookingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaListenBooking {
    private final Logger log = LoggerFactory.getLogger(KafkaListenBooking.class);
    private final KafkaBookingService kafkaBookingService;
    private final EmailSend emailSendContacts;

    public KafkaListenBooking(KafkaBookingService kafkaBookingService, EmailSend emailSendContacts) {
        this.kafkaBookingService = kafkaBookingService;
        this.emailSendContacts = emailSendContacts;
    }

    @KafkaListener(topics = "sendEmailDoJaBooking")
    public void start(String message) {
        try {
            log.info("Kafka booking starting...");
            log.info("Message: {}",message);
            KafkaBooking kafkaBooking = new ObjectMapper().readValue(message, KafkaBooking.class);

            log.info(kafkaBooking.toString());
            emailSendContacts.sendSimpleMessageBooking(kafkaBooking);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
