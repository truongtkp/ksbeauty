package com.aladin.notification.Notification;


import com.aladin.notification.Entity.KafkaConsumer;
import com.aladin.notification.Service.EmailSend;
import com.aladin.notification.Service.KafkaConsumerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
@Component
public class KafkaListen {
    private final Logger log = LoggerFactory.getLogger(KafkaListen.class);
    private final KafkaConsumerService kafkaConsumerService;
    private final EmailSend emailSendContacts;

    public KafkaListen(KafkaConsumerService kafkaConsumerService, EmailSend emailSendContacts) {
        this.kafkaConsumerService = kafkaConsumerService;
        this.emailSendContacts = emailSendContacts;
    }
    @KafkaListener(topics = "sendEmailDoJa")
    public void start(String message) {
        try {
            log.info("Kafka consumer starting...");
            log.info("Message: {}",message);
            KafkaConsumer kafkaconsumer = new ObjectMapper().readValue(message, KafkaConsumer.class);
            emailSendContacts.sendSimpleMessage(kafkaconsumer);
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
        }
    }
}
