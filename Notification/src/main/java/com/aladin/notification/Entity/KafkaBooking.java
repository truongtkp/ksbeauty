package com.aladin.notification.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class KafkaBooking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    private String subject;

    private String recipients;

    private String servicePackage;

    private String address;

    private String staff;

    private String timetable;

    private String bookingDate;

    private String recipientsName;

    private String cc;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getServicePackage() {
        return servicePackage;
    }

    public void setServicePackage(String servicePackage) {
        this.servicePackage = servicePackage;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getTimetable() {
        return timetable;
    }

    public void setTimetable(String timetable) {
        this.timetable = timetable;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getRecipientsName() {
        return recipientsName;
    }

    public void setRecipientsName(String recipientsName) {
        this.recipientsName = recipientsName;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "KafkaBooking{" +
                "id=" + id +
                ", subject='" + subject + '\'' +
                ", recipients='" + recipients + '\'' +
                ", servicePackage='" + servicePackage + '\'' +
                ", address='" + address + '\'' +
                ", staff='" + staff + '\'' +
                ", timetable='" + timetable + '\'' +
                ", bookingDate='" + bookingDate + '\'' +
                ", recipientsName='" + recipientsName + '\'' +
                ", cc='" + cc + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
