package com.aladin.notification.Repository;

import com.aladin.notification.Entity.KafkaBooking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KafkaBookingRepository extends JpaRepository<KafkaBooking, Long> {
}
