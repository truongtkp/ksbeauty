package com.aladin.notification.Repository;

import com.aladin.notification.Entity.KafkaConsumer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KafkaRepository extends JpaRepository<KafkaConsumer,Long> {
}
