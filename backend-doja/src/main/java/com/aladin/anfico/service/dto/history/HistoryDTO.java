package com.aladin.anfico.service.dto.history;

import com.aladin.anfico.config.Constants;
import com.aladin.anfico.domain.History;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HistoryDTO {


    public HistoryDTO (History history){
        this.id = history.getId();
        this.imagePath = history.getImagePath();
        this.imageUrl = history.getImageUrl();
        this.descriptionVi = history.getDescriptionVi();
        this.descriptionEn = history.getDescriptionEn();
        this.year = history.getYear();
    }

    private Long id;

    @NotNull
    @Size(min = 0, max = 500)
    private String imagePath;
    @NotNull
    @Pattern(regexp = Constants.YEAR_REGEX)
    private String year;
    @NotNull
    @Size(min = 0, max = 500)
    private String imageUrl;
    @NotNull
    private String descriptionVi;
    @NotNull
    private String descriptionEn;

}
