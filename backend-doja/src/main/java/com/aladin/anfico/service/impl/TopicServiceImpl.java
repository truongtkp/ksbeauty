package com.aladin.anfico.service.impl;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Topic;
import com.aladin.anfico.domain.TopicImage;
import com.aladin.anfico.domain.Type;
import com.aladin.anfico.repository.TopicImageRepository;
import com.aladin.anfico.repository.TopicRepository;
import com.aladin.anfico.service.TopicService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.topic.DeviceDTO;
import com.aladin.anfico.service.dto.topic.TopicDTO;
import com.aladin.anfico.service.mapper.device.DeviceMapper;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class TopicServiceImpl implements TopicService {

    private final Logger log = LoggerFactory.getLogger(TopicServiceImpl.class);
    private final TopicImageRepository topicImageRepository;
    private final TopicRepository topicRepository;
    private final DeviceMapper deviceMapper;
    private final FileServiceImpl fileServiceImpl;

    @Override
    public Optional<Set<TopicImage>> getTopicBanner(Type type){
        log.info("TodoService: get topic banner");
        Topic topic = topicRepository.findTopicByType(type);
        return Optional.of(topicImageRepository.findTopicImageByTopic(topic.getId()));
    }
    @Override
    public Optional<Set<TopicImage>> getTopicAlbum(Long id){
        log.info("TodoService: get topic banner");
        return Optional.of(topicImageRepository.findTopicImageByTopic(id));
    }
    @Override
    public HashMap<String, ?> addTopicImage(List<TopicImage> topicImages, Type type){
        Topic topic = topicRepository.findTopicByType(type);
        List<TopicImage> topicImgs = new ArrayList<>();
        for (int i = 0; i< topicImages.size(); i++){
            TopicImage topicImage = new TopicImage();
            topicImage.setTopic(topic);
            topicImage.setImagePath(topicImages.get(i).getImagePath());
            topicImage.setImageUrl(topicImages.get(i).getImageUrl());
            topicImgs.add(topicImageRepository.save(topicImage));
        }
        return ArrayDTO.convertListToSet(topicImages.size(), topicImgs);
    }
    @Override
    public void deleteTopicImage(Long id){
        Optional<TopicImage> topicImage = topicImageRepository.findById(id);
        if (topicImage.isPresent()){
            fileServiceImpl.deleteFile(topicImage.get().getImagePath());
            topicImageRepository.delete(topicImage.get());
        }else{
            throw new BadRequestAlertException(Alerts.ID_EXIST,"topic-management","id");
        }
    }

    @Override
    public Boolean deleteTopicImage(List<Long> idList){
        List<TopicImage> topicImages = new ArrayList<>();
        idList.forEach(aLong -> {
            Optional<TopicImage> topicImage = topicImageRepository.findById(aLong);
            if (!topicImage.isPresent()) return;
            fileServiceImpl.deleteFile(topicImage.get().getImagePath());
            topicImages.add(topicImage.get());
        });
        topicImageRepository.deleteAll(topicImages);
        return true;
    }

    @Override
    public Optional<List<DeviceDTO>> getAllTopic(Type type){
        List<Topic> topics = topicRepository.findTopicsByType(type);
        topics.stream().forEach(topic ->{
            topic.setTopicImage(topicImageRepository.findTopicImageByTopicLimit(topic.getId()));
        });
        return Optional.of(deviceMapper.toDto(topics));
    }

    @Override
    public HashMap<String, ?> getAllTopic(int page, int size){
        List<Topic> topics = topicRepository.findTopicsByType(Type.DEVICE, PageRequest.of(page,size));
        topics.stream().forEach(topic ->{
            topic.setTopicImage(topicImageRepository.findTopicImageByTopicLimit(topic.getId()));
        });
        return ArrayDTO.convertListToSet(deviceMapper.toDto(topics).size(), deviceMapper.toDto(topics));
    }

    @Override
    public HashMap<String, ?> searchDevice(String param, int page, int size){
        List<Topic> topics = topicRepository.searchTopicsByType(Type.DEVICE,param, PageRequest.of(page,size));
        topics.stream().forEach(topic ->{
            topic.setTopicImage(topicImageRepository.findTopicImageByTopicLimit(topic.getId()));
        });
        return ArrayDTO.convertListToSet(deviceMapper.toDto(topics).size(), deviceMapper.toDto(topics));
    }
    @Override
    public DeviceDTO addDevice(DeviceDTO dto){
        Topic t = deviceMapper.toEntity(dto);
        t.setType(Type.DEVICE);
        TopicImage ti = new TopicImage();
        ti.setImagePath(dto.getTopicImage().getImagePath());
        ti.setImageUrl(dto.getTopicImage().getImageUrl());
        ti.setTopic(t);
        Topic topicCreated = topicRepository.save(t);
        topicCreated.setTopicImage(topicImageRepository.save(ti));
        return Optional.of(topicCreated).map(DeviceDTO::new).get();
    }
    @Override
    public Optional<DeviceDTO> updateDevice(DeviceDTO dto){
        return Optional
            .of(topicRepository.findById(dto.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(topic -> {
//                topic.setDescriptionVi(dto.getDescriptionVi());
                topic.setDescriptionEn(dto.getDescriptionEn());
                Topic topicCreated = topicRepository.save(topic);
                TopicImage topicImage = topicImageRepository.findTopicImageByTopicLimit(topic.getId());
                if(!topicImage.getImagePath().equals(dto.getTopicImage().getImagePath())){
                    fileServiceImpl.deleteFile(topicImage.getImagePath());
                    topicImage.setImageUrl(dto.getTopicImage().getImageUrl());
                    topicImage.setImagePath(dto.getTopicImage().getImagePath());
                    topicImageRepository.save(topicImage);
                }
                topicCreated.setTopicImage(topicImage);
                return Optional.of(topicCreated).map(DeviceDTO::new).get();
            });
    }
    @Override
    public boolean deleteAlbum(Long id){
        Optional<Topic> topic = topicRepository.findById(id);
        if (!topic.isPresent()){
            return false;
        }
        Set<TopicImage> topicImage = topicImageRepository.findTopicImageByTopic(topic.get().getId());
        topicImage.stream().forEach(topicImage1 -> {
            fileServiceImpl.deleteFile(topicImage1.getImagePath());
        });
        topicRepository.delete(topic.get());
        return true;
    }
    @Override
    public boolean deleteDevice(Long id){
        Optional<Topic> topicDeleted = topicRepository.findById(id);
       if (!topicDeleted.isPresent()) return false;
        TopicImage topicImage = topicImageRepository.findTopicImageByTopicLimit(topicDeleted.get().getId());
        fileServiceImpl.deleteFile(topicImage.getImagePath());
        topicRepository.delete(topicDeleted.get());
        return true;

    }
    @Override
    public HashMap<String, ?> addListAlbum(List<TopicImage> topicImages, Type type, Long id){
        Topic topic = topicRepository.findTopicByType(type, id);
        List<TopicImage> topicImgs = new ArrayList<>();
        for (int i = 0; i< topicImages.size(); i++){
            TopicImage topicImage = new TopicImage();
            topicImage.setTopic(topic);
            topicImage.setImagePath(topicImages.get(i).getImagePath());
            topicImage.setImageUrl(topicImages.get(i).getImageUrl());
            topicImgs.add(topicImageRepository.save(topicImage));
        }
        return ArrayDTO.convertListToSet(topicImages.size(), topicImgs);
    }
    @Override
    public Topic addTopic(Type type, TopicDTO topic){
        Topic t = new Topic();
        t.setType(type);
//        t.setDescriptionVi(topic.getDescriptionVi());
        t.setDescriptionEn(topic.getDescriptionEn());
        t.setTopicImages(null);
        return topicRepository.save(t);
    }
    @Override
    public Topic updateTopic(TopicDTO topicDTO){
        Topic topic = topicRepository.findById(topicDTO.getId()).get();
        topic.setDescriptionEn(topicDTO.getDescriptionEn());
//        topic.setDescriptionVi(topicDTO.getDescriptionVi());
        return topicRepository.save(topic);
    }


}
