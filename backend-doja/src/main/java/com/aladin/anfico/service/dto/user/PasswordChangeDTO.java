package com.aladin.anfico.service.dto.user;

import com.aladin.anfico.config.Constants;
import lombok.Data;

import javax.validation.constraints.Pattern;

/**
 * A DTO representing a password change required data - current and new password.
 */
@Data
public class PasswordChangeDTO {

    private String currentPassword;

    @Pattern(regexp = Constants.PASS_REGEX)
    private String newPassword;

}
