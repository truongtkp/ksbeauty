package com.aladin.anfico.service;

import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.util.Date;

public interface NotificationBookingService {
    @Transactional
    void SendEmailBooking(String template, String email, String orderId, String subject, Date bookingDate, String address, String servicePackage, String staff, String timetable,
                          String recipientsName, String cc, String phoneNumber, String emailShow);
}
