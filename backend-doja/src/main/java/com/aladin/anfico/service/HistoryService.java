package com.aladin.anfico.service;

import com.aladin.anfico.domain.History;
import com.aladin.anfico.service.dto.history.HistoryDTO;

import java.util.HashMap;
import java.util.Optional;

public interface HistoryService {
    History addHistory(HistoryDTO historyDTO);

    Optional<History> updateHistory(HistoryDTO historyDTO);

    Optional<History> deleteHistory(Long id);

    HashMap<String, ?> getAllManagedHistory(int page, int size);

    Optional<History> getHistory(Long id);


}
