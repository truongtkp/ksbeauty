package com.aladin.anfico.service;

import com.aladin.anfico.domain.News;
import com.aladin.anfico.service.dto.news.NewsCreateDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface NewsService {
    News createNews(NewsCreateDTO news);

    Optional<News> updateNews(NewsCreateDTO newsCreateDTO);

    Optional<News> deleteNews(Long id);


    boolean deleteAllById(List<Long> listId);

    HashMap<String, ?> getAllManagedNews(int page, int size);

    HashMap<String, ?> searchNews(String param, int page, int size);
}
