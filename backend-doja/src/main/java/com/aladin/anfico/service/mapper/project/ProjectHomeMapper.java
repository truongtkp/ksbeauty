package com.aladin.anfico.service.mapper.project;

import com.aladin.anfico.domain.Project;
import com.aladin.anfico.service.dto.project.ProjectDTO;
import com.aladin.anfico.service.dto.project.ProjectHomeDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ProjectHomeMapper extends EntityMapper<ProjectHomeDTO, Project> {

}
