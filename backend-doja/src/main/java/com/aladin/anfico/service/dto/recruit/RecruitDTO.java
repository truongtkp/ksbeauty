package com.aladin.anfico.service.dto.recruit;

import com.aladin.anfico.domain.Recruit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecruitDTO {

    private Long id;

    @Size(max = 500)
    private String titleEn;

    @Size(max = 500)
    private String titleVi;

    @Size(max = 256)
    private String salaryEn;

    @Size(max = 256)
    private String salaryVi;

    @NotNull
    @Size(max = 500)
    private String workplaceVi;

    @NotNull
    @Size(max = 500)
    private String workplaceEn;

    private Date dateExpiration;

    @NotNull
    @Size(max = 500)
    private String avatarUrl;

    @NotNull
    @Size(max = 500)
    private String avatarPath;

}
