package com.aladin.anfico.service;

import com.aladin.anfico.domain.Project;
import com.aladin.anfico.service.dto.project.ProjectCreateDTO;
import com.aladin.anfico.service.dto.project.ProjectDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface ProjectService {
    Project createProject(ProjectCreateDTO Project);

    Optional<ProjectDTO> updateProject(ProjectCreateDTO ProjectCreateDTO);

    Optional<Project> deleteProject(Long id);

    boolean deleteAllById(List<Long> listId);

    HashMap<String, ?> getAllManagedProject(int page, int size);

    HashMap<String, ?> searchProjectEn(String param, int page, int size);

    HashMap<String, ?> getAllProject(int page, int size);

}
