package com.aladin.anfico.service;

import com.aladin.anfico.domain.Kafkabooking;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface KafkaBookingService {
    Kafkabooking save(Kafkabooking kafkabooking);

    @Transactional(readOnly = true)
    List<Kafkabooking> findAll();

    @Transactional(readOnly = true)
    Optional<Kafkabooking> findOne(Long id);

    void delete(Long id);
}
