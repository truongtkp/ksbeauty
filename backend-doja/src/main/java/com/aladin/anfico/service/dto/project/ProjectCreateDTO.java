package com.aladin.anfico.service.dto.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.Size;
import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProjectCreateDTO {

    private Long id;

    @Size(min = 0, max = 500)
    private String titleEn;

    private String descriptionEn;

    private String contentEn;
    @Size(min = 0, max = 500)

    private String avatarUrl;


    @Size(min = 0, max = 500)

    private String avatarPath;

    private Instant createdDate;
}

