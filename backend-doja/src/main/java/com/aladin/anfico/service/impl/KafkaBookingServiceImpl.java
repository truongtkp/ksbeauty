package com.aladin.anfico.service.impl;

import com.aladin.anfico.domain.Kafkabooking;
import com.aladin.anfico.domain.Kafkavoucher;
import com.aladin.anfico.repository.KafkaBookingRepository;
import com.aladin.anfico.service.KafkaBookingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class KafkaBookingServiceImpl implements KafkaBookingService {

    private final Logger log = LoggerFactory.getLogger(KafkaBookingServiceImpl.class);

    private final KafkaBookingRepository kafkaBookingRepository;

    public KafkaBookingServiceImpl(KafkaBookingRepository kafkaBookingRepository) {
        this.kafkaBookingRepository = kafkaBookingRepository;
    }

    @Override
    public Kafkabooking save(Kafkabooking kafkabooking) {
        log.debug("Request to save Kafkabooking : {}", kafkabooking);
        Kafkabooking result = kafkaBookingRepository.save(kafkabooking);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Kafkabooking> findAll() {
        log.debug("Request to get all Kafkabooking");
        return kafkaBookingRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Kafkabooking> findOne(Long id) {
        log.debug("Request to get Kafkabooking : {}", id);
        return kafkaBookingRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Kafkabooking : {}", id);
        kafkaBookingRepository.deleteById(id);
    }
}
