package com.aladin.anfico.service.impl;

import com.aladin.anfico.domain.Kafkavoucher;
import com.aladin.anfico.repository.KafkaVoucherRepository;
import com.aladin.anfico.service.KafkaVoucherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class KafkaVoucherServiceImpl implements KafkaVoucherService {

    private final Logger log = LoggerFactory.getLogger(KafkaVoucherServiceImpl.class);

    private final KafkaVoucherRepository kafkaVoucherRepository;

    public KafkaVoucherServiceImpl(KafkaVoucherRepository kafkaVoucherRepository) {
        this.kafkaVoucherRepository = kafkaVoucherRepository;
    }

    @Override
    public Kafkavoucher save(Kafkavoucher kafkavoucher) {
        log.debug("Request to save Kafkavoucher : {}", kafkavoucher);
        Kafkavoucher result = kafkaVoucherRepository.save(kafkavoucher);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Kafkavoucher> findAll() {
        log.debug("Request to get all Kafkavoucher");
        return kafkaVoucherRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Kafkavoucher> findOne(Long id) {
        log.debug("Request to get Kafkavoucher : {}", id);
        return kafkaVoucherRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Kafkavoucher : {}", id);
        kafkaVoucherRepository.deleteById(id);
    }
}
