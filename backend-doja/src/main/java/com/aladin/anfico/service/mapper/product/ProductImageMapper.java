package com.aladin.anfico.service.mapper.product;

import com.aladin.anfico.domain.Product;
import com.aladin.anfico.domain.ProductImage;
import com.aladin.anfico.service.dto.product.ProductCreateDTO;
import com.aladin.anfico.service.dto.product.ProductCrudDTO;
import com.aladin.anfico.service.dto.product.ProductImageDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;


/**
 * Mapper for the entity {@link ProductImage} and its DTO {@link ProductImageDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProductImageMapper extends EntityMapper<ProductImageDTO, ProductImage> {
}

