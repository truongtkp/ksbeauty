package com.aladin.anfico.service.impl;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.*;
import com.aladin.anfico.repository.*;
import com.aladin.anfico.service.FileService;
import com.aladin.anfico.service.ProductService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.product.*;
import com.aladin.anfico.service.mapper.product.*;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductImageRepository productImageRepository;
    private final SpecificationRepository specificationRepository;
    private final SpecificationDetailRepository specificationDetailRepository;

    private final FileService fileService;

    private final ProductCreateMapper productCreateMapper;

    private final CategoryRepository categoryRepository;

    private final SpecificationDetailMapper specificationDetailMapper;
    private final ProductImageMapper productImageMapper;
    private final SpecificationMapper specificationMapper;

    private final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Override
    public HashMap<String,?> getAllProduct(int page, int size){
        log.info("--Get all product table --");
        return ArrayDTO.convertListToSet(productRepository.count(),
            productRepository.getAllProduct(PageRequest.of(page,size, Sort.by(Sort.Direction.DESC, "priority","id"))));
    }

    @Override
    public HashMap<String,?> getAllProductSortedByIdDesc(int page, int size){
        log.info("--Get all product table --");
//        List<ProductDTO> products = productRepository.getAllProductSortedByIdDesc(PageRequest.of(page,size, Sort.by(Sort.Direction.DESC, "priority","id")));
        List<ProductDTO> products = productRepository.getAllProductSortedByIdDesc(PageRequest.of(page,size ));
        return ArrayDTO.convertListToSet(productRepository.count(),
            products);
    }

    @Override
    public HashMap<String,?> searchProductByTitle(String param, int page, int size){
        log.info("--Get all product table --");
        return ArrayDTO.convertListToSet(productRepository.countSearchProductEn(param),
                productRepository.searchProductEn(param, PageRequest.of(page,size)));
    }

    // 0 search - 0 category
    @Override
    public HashMap<String,?> filterProduct(int sort, int page, int size){
        log.info("--Get all product table --");
        Long count =  productRepository.count();
        switch (sort){
            case 0:
                log.info("hoang");
                return ArrayDTO.convertListToSet( count,
                        productRepository.getAllProduct(PageRequest.of(page,size, Sort.by(Sort.Direction.DESC,"id"))));
            case 1:
                return ArrayDTO.convertListToSet( count ,
                        productRepository.getAllProduct(PageRequest.of(page,size, Sort.by(Sort.Direction.ASC,"id"))));
            case 2:
                return ArrayDTO.convertListToSet( count,
                        productRepository.getAllProduct(PageRequest.of(page,size, Sort.by(Sort.Direction.ASC,"titleVi", "titleEn"))));
           case 3:
               return ArrayDTO.convertListToSet( count ,
                       productRepository.getAllProduct(PageRequest.of(page,size, Sort.by(Sort.Direction.DESC,"titleVi", "titleEn"))));
           default:{
               return ArrayDTO.convertListToSet(0,null);
           }
        }
    }

    // 0 search - 1 category
    @Override
    public HashMap<String,?> filterProduct(Long categoryId ,int sort, int page, int size){
        log.info("--Get all product table --");
        Long count = productRepository.countProductByCategory(categoryId);

        switch (sort){
            case 0:
                return ArrayDTO.convertListToSet( count,
                        productRepository.getProductByCategory(categoryId,PageRequest.of(page,size, Sort.by(Sort.Direction.DESC,"id"))));
            case 1:
                return ArrayDTO.convertListToSet( count ,
                        productRepository.getProductByCategory(categoryId,PageRequest.of(page,size, Sort.by(Sort.Direction.ASC,"id"))));
            case 2:
                return ArrayDTO.convertListToSet( count ,
                        productRepository.getProductByCategory(categoryId,PageRequest.of(page,size, Sort.by(Sort.Direction.ASC,"titleVi", "titleEn"))));
            case 3:
                return ArrayDTO.convertListToSet( count ,
                        productRepository.getProductByCategory(categoryId,PageRequest.of(page,size, Sort.by(Sort.Direction.DESC,"titleVi", "titleEn"))));
            default:{
                return ArrayDTO.convertListToSet(0,null);
            }
        }
    }

    // 1 search - 0 category
    @Override
    public HashMap<String,?> filterProductEn(String param ,int sort, int page, int size){
        log.info("--Get all product table --");
        Long count = productRepository.countSearchProductEn(param);
        switch (sort){
            case 0:
                return ArrayDTO.convertListToSet( count ,
                        productRepository.searchProductEn(param,PageRequest.of(page,size, Sort.by(Sort.Direction.DESC,"id"))));
            case 1:
                return ArrayDTO.convertListToSet( count ,
                        productRepository.searchProductEn(param,PageRequest.of(page,size, Sort.by(Sort.Direction.ASC,"id"))));
            case 2:
                return ArrayDTO.convertListToSet( count ,
                        productRepository.searchProductEn(param,PageRequest.of(page,size, Sort.by(Sort.Direction.ASC,"titleVi", "titleEn"))));
            case 3:
                return ArrayDTO.convertListToSet( count ,
                        productRepository.searchProductEn(param,PageRequest.of(page,size, Sort.by(Sort.Direction.DESC,"titleVi", "titleEn"))));
            default:{
                return ArrayDTO.convertListToSet(0,null);
            }
        }
    }

    // 1 search - 1 category
    public HashMap<String,?> filterProductEn(String param, Long categoryId ,int sort, int page, int size){
        log.info("--Get all product table --");
        Long count = productRepository.countSearchProductByCategoryEn(param, categoryId);;
        switch (sort){
            case 0:
                return ArrayDTO.convertListToSet( count !=  null ? count : 0,
                        productRepository.searchProductByCategoryEn(param, categoryId, PageRequest.of(page,size, Sort.by(Sort.Direction.DESC,"id"))));
            case 1:
                return ArrayDTO.convertListToSet(count !=  null ? count : 0,
                        productRepository.searchProductByCategoryEn(param, categoryId, PageRequest.of(page,size, Sort.by(Sort.Direction.ASC,"id"))));
            case 2:
                return ArrayDTO.convertListToSet(count !=  null ? count : 0,
                        productRepository.searchProductByCategoryEn(param, categoryId,  PageRequest.of(page,size, Sort.by(Sort.Direction.ASC,"titleVi", "titleEn"))));
            case 3:
                return ArrayDTO.convertListToSet(count !=  null ? count : 0,
                        productRepository.searchProductByCategoryEn(param, categoryId, PageRequest.of(page,size, Sort.by(Sort.Direction.DESC,"titleVi", "titleEn"))));
            default:{
                return ArrayDTO.convertListToSet(0,null);
            }
        }
    }


    @Override
    public Optional<ProductCreateDTO> getProductDetail(Long id){
        log.info("Get a product with id: {}", id);
        return Optional
                .of(productRepository.findById(id))
                .filter(Optional::isPresent)
                .map(product -> {
                    ProductCreateDTO productGet = productCreateMapper.toDto(product.get());
                    productGet.setProductImageList(productImageMapper.toDto(product.get().getProductImages().stream().sorted((o1, o2) -> o1.getId().compareTo(o2.getId())).collect(Collectors.toList())));
                    List<SpecificationDTO> specifications = new ArrayList<>();
                    product.get().getSpecifications().stream().forEach(specification -> {
                        SpecificationDTO specification1 = specificationMapper.toDto(specification);
                        specification1.setSpecificationDetailList(specificationDetailMapper.toDto(specification.getSpecificationDetails().stream().sorted((o1, o2) -> o1.getId().compareTo(o2.getId())).collect(Collectors.toList())));
                        specifications.add(specification1);
                    });
                    productGet.setSpecificationList(specifications.stream().sorted((o1, o2) -> o1.getId().compareTo(o2.getId())).collect(Collectors.toList()));
                    productGet.setCategoryId(categoryRepository.getCategoryByProductID(product.get().getId()));
                    return productGet;
                });
    }

    @Override
    @Transactional
    public ProductDTO addProduct(ProductCreateDTO productCreateDTO, Category category){
        log.info("--Add product table --");
        Product product = productCreateMapper.toEntity(productCreateDTO);
        product.setCategory(category);
        Product productCreated = productRepository.save(product);
        this.addProductImage(productImageMapper.toEntity(productCreateDTO.getProductImageList()), productCreated);
        productCreateDTO.getSpecificationList().stream().forEach(specificationDTO -> {
          Specification specification = specificationMapper.toEntity(specificationDTO);
          specification.set_product(product);
          Specification specificationCreated =  specificationRepository.save(specification);
          this.addSpecificationsDetail(specificationDetailMapper.toEntity(specificationDTO.getSpecificationDetailList()), specificationCreated);
        });
        return productRepository.getProductById(productCreated.getId());
    }
    @Transactional
    @Override
    public Optional<Product> deleteProduct(Long id){
        Optional<Product> product =  productRepository.findById(id);
        if (!product.isPresent()) return null;
        product.get().getProductImages().stream().forEach(
                productImage -> {
                    if (productImage.getImagePath() != null) fileService.deleteFile(productImage.getImagePath());
                }
        );
        productRepository.delete(product.get());
        return product;
    }


    @Transactional
    @Override
    public ProductDTO updateProduct(ProductCreateDTO productCreateDTO, Category category){

        Optional<Product> product =  productRepository.findById(productCreateDTO.getId());
        if (!product.isPresent()) return null;
        productCreateDTO.getProductImageList().forEach(productImage -> {
            if (productImage.getId() == null){
                this.addProductImage(productImageMapper.toEntity(productImage), product.get());
            }
            else if (productImage.getImagePath() == null){
                Optional<ProductImage> productImageGet  =  productImageRepository.findById(productImage.getId());
                if (!productImageGet.isPresent()) return;
                product.get().getProductImages().remove(productImageGet.get());
                fileService.deleteFile(productImageGet.get().getImagePath());
                this.deleteProductImage(productImageGet.get());
            }
        });

        productCreateDTO.getSpecificationList().forEach(
                specificationDTO -> {
                    if  (specificationDTO.getId() == null){
                        Specification specification = specificationMapper.toEntity(specificationDTO);
                        specification.set_product(product.get());
                        Specification specificationCreated =  specificationRepository.save(specification);
                        this.addSpecificationsDetail(specificationDetailMapper.toEntity(specificationDTO.getSpecificationDetailList()), specificationCreated);
                    }else if (specificationDTO.getNameEn() == null){
                        Optional<Specification> specification  =  specificationRepository.findById(specificationDTO.getId());
                        if (!specification.isPresent()) return;
                        product.get().getSpecifications().remove(specification.get());
                        specificationRepository.delete(specification.get());
                    }else{
                        Optional<Specification> specification  =  specificationRepository.findById(specificationDTO.getId());
                        if (!specification.isPresent()) return;
                        specificationMapper.partialUpdate(specification.get(), specificationDTO);
                        specificationDTO.getSpecificationDetailList().forEach(specificationDetailDTO -> {
                                if (specificationDetailDTO.getId() == null){
                                    SpecificationDetail specificationDetail = specificationDetailMapper.toEntity(specificationDetailDTO);
                                    specificationDetail.setSpecification(specification.get());
                                    this.specificationDetailRepository.save(specificationDetail);
                                }else{
                                    Optional<SpecificationDetail> specificationDetailGet   =  specificationDetailRepository.findById(specificationDetailDTO.getId());
                                    if (!specificationDetailGet.isPresent()) return;
                                    this.specificationDetailMapper.partialUpdate(specificationDetailGet.get(),specificationDetailDTO);
                                    this.specificationDetailRepository.save(specificationDetailGet.get());
                                }
                            }
                        );
                    }
                }
        );

        productCreateMapper.partialUpdate(product.get(), productCreateDTO);
        product.get().setCategory(category);
        Product productUpdated =  productRepository.save(product.get());
        return  productRepository.getProductById(productUpdated.getId());
    }



    public void addProductImage(List<ProductImage> productImage, Product product){
         productImage.stream().forEach(productImage1 -> {
             productImage1.setProduct(product);
         });
         productImageRepository.saveAll(productImage);
    }

    public void addProductImage(ProductImage productImage, Product product){
        productImage.setProduct(product);
        productImageRepository.save(productImage);
    }

    public void deleteProductImage(ProductImage productImage){
        productImageRepository.delete(productImage);
    }

    public void addSpecificationsDetail(List<SpecificationDetail> specificationDetail, Specification specification){
        specificationDetail.stream().forEach(specificationDetail1 -> {
            specificationDetail1.setSpecification(specification);
        });
        specificationDetailRepository.saveAll(specificationDetail);
    }

    @Override
    public Optional<Product> isPriority(Long id){
        return Optional.of(productRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(product -> {
                int count = productRepository.countProjectByPriority();
                if (count >= 6 && !product.isPriority()) {
                    throw new BadRequestAlertException(Alerts.PRIORITY, "product-management", "priority");
                }
                product.setPriority(!product.isPriority());
                return productRepository.save(product);
            });
    }


}
