package com.aladin.anfico.service.dto.topic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class TopicDTO {
    private Long id;

//    @Size(max = 256)
//    private String descriptionVi;

    @Size( max = 256)
    private String descriptionEn;

}
