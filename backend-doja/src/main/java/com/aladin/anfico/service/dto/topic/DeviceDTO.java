package com.aladin.anfico.service.dto.topic;

import com.aladin.anfico.domain.Topic;
import com.aladin.anfico.domain.TopicImage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDTO {

    public DeviceDTO(Topic topic){
        this.id = topic.getId();
        this.descriptionEn = topic.getDescriptionEn();
//        this.setDescriptionVi(topic.getDescriptionVi());
        this.topicImage = topic.getTopicImage();
    }

    private Long id;

//    @Size(min = 0, max = 256)
//    private String descriptionVi;
    @Size(min = 0, max = 256)
    private String descriptionEn;
    private TopicImage topicImage;



}
