package com.aladin.anfico.service.dto.category;

import lombok.Getter;

import javax.persistence.Column;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
public class CategoryDTO {
    private Long id;


    @NotNull
    @Size(max = 1000)
    private String nameEn;

//    @NotNull
//    @Size(max = 1000)
//    private String nameVi;


    @Size(max = 65535)
    @NotNull
    private String descriptionEn;

//    @Size(max = 65535)
//    @NotNull
//    private String descriptionVi;

    @Size(min = 0, max = 500)
    private String imagePath;

    @Size(min = 0, max = 500)
    private String imageUrl;
}
