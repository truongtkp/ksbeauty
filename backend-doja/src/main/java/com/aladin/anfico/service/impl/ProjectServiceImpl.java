package com.aladin.anfico.service.impl;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Project;
import com.aladin.anfico.repository.ProjectRepository;
import com.aladin.anfico.service.ProjectService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.project.ProjectCreateDTO;
import com.aladin.anfico.service.dto.project.ProjectDTO;
import com.aladin.anfico.service.dto.project.ProjectHomeDTO;
import com.aladin.anfico.service.mapper.project.ProjectGetMapper;
import com.aladin.anfico.service.mapper.project.ProjectHomeMapper;
import com.aladin.anfico.service.mapper.project.ProjectMapper;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final Logger log = LoggerFactory.getLogger(ProjectServiceImpl.class);
    private final ProjectRepository projectRepository;
    private final FileServiceImpl fileServiceImpl;
    private final ProjectMapper  projectMapper;
    private final ProjectHomeMapper projectHomeMapper;
    private final ProjectGetMapper projectGetMapper;

    @Override
    public Project createProject(ProjectCreateDTO Project){
        log.info("--Create a Project table --");
        return projectRepository.save(projectMapper.toEntity(Project));
    }


    public Optional<ProjectDTO> updateProject(ProjectCreateDTO ProjectCreateDTO){
        log.info("--Create a Project table --");
        return Optional
            .of(projectRepository.findById(ProjectCreateDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(Project -> {
                if(!ProjectCreateDTO.getAvatarPath().equals(Project.getAvatarPath())){
                    fileServiceImpl.deleteFile(Project.getAvatarPath());
                }
                projectMapper.partialUpdate(Project,ProjectCreateDTO);
                return projectGetMapper.toDto(projectRepository.save(Project));
            });
    }


    @Override
    public Optional<Project> deleteProject(Long id){

        return Optional
                .of(projectRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(Project -> {
                    fileServiceImpl.deleteFile(Project.getAvatarPath());
                    projectRepository.delete(Project);
                    return Project;
                });
    }


    @Transactional
    @Override
    public boolean deleteAllById(List<Long> listId){
        log.info("--Delete a Project by id--");

        List<Project> listProject = new ArrayList<>();
        listId.forEach(id -> {
                Optional<Project> Project =
             Optional.of(projectRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get);
             if (!Project.isPresent()) throw new BadRequestAlertException(Alerts.ID_EXIST, "product-manager", "id");
             listProject.add(Project.get());
        });
        projectRepository.deleteAll(listProject);
        return true;
    }
    @Override
    public HashMap<String,?> getAllManagedProject(int page, int size) {
        log.info("--Get all user table --");
        return ArrayDTO.convertListToSet(projectRepository.count(),
                projectGetMapper.toDto(projectRepository.findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "priority", "id"))).getContent()));
    }

    @Override
    public HashMap<String,?> getAllProject(int page, int size) {
        return ArrayDTO.convertListToSet(projectRepository.count(),
                projectGetMapper.toDto(projectRepository.findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.DESC,  "id"))).getContent()));
    }




//    search en-vi
    @Override
    public HashMap<String, ?> searchProjectEn(String param, int page, int size) {
        log.info("--Search --");
        return
                ArrayDTO.convertListToSet(
                        projectRepository.countSearchEn(param),
                       projectGetMapper.toDto( projectRepository.searchAllByProjectEn(param,PageRequest
                               .of(page, size, Sort.by(Sort.Direction.DESC, "id"))))
                );
    }

    public Optional<Project> isPriority(Long id){
        return Optional.of(projectRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(project -> {
                    project.setPriority(!project.isPriority());
                    return projectRepository.save(project);
                });
    }

    public Optional<List<ProjectHomeDTO>> getAllPriority(){
        return Optional.of(projectHomeMapper.toDto(projectRepository.getProjectByPriority()));
    }

}
