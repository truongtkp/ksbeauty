package com.aladin.anfico.service;

import com.aladin.anfico.domain.Kafkavoucher;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface KafkaVoucherService {
    Kafkavoucher save(Kafkavoucher kafkavoucher);

    @Transactional(readOnly = true)
    List<Kafkavoucher> findAll();

    @Transactional(readOnly = true)
    Optional<Kafkavoucher> findOne(Long id);

    void delete(Long id);
}
