package com.aladin.anfico.service.impl;

import com.aladin.anfico.config.KafkaProperties;
import com.aladin.anfico.domain.Kafkabooking;
import com.aladin.anfico.service.KafkaBookingService;
import com.aladin.anfico.service.NotificationBookingService;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class NotificationBookingServiceImpl implements NotificationBookingService {

    private String sendEmail = "sendEmailDoJaBooking";
    private final Logger log = LoggerFactory.getLogger(NotificationBookingServiceImpl.class);
    private final KafkaProperties kafkaProperties;
    private KafkaProducer<String, String> producer;
    private final SpringTemplateEngine springTemplateEngine;
    private final KafkaBookingService kafkaBookingService;

    public NotificationBookingServiceImpl(KafkaProperties kafkaProperties, SpringTemplateEngine springTemplateEngine, KafkaBookingService kafkaBookingService) {
        this.kafkaProperties = kafkaProperties;
        this.springTemplateEngine = springTemplateEngine;
        this.kafkaBookingService = kafkaBookingService;
    }

    @PostConstruct
    public void initialize() {
        log.info("Kafka producer initializing...");
        this.producer = new KafkaProducer<>(kafkaProperties.getProducerProps());
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
        log.info("Kafka producer initialized");
    }

    @Override
    public void SendEmailBooking(String template, String email, String orderId, String subject, Date bookingDate, String address, String servicePackage, String staff, String timetable, String recipientsName, String cc, String phoneNumber, String emailShow) {
        JSONObject obj = new JSONObject();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        obj.put("subject", subject);
        obj.put("recipients", email);
        obj.put("servicePackage", servicePackage);
        obj.put("address", address);
        obj.put("staff", staff);
        obj.put("timetable", timetable);
        obj.put("bookingDate",formatDate.format(bookingDate));
        obj.put("recipientsName", recipientsName);
        obj.put("cc", cc);
        Context context=new Context();
        context.setVariable("customerNameBooking",recipientsName);
        context.setVariable("bookingDateBooking",formatDate.format(bookingDate));
        context.setVariable("address",address);
        context.setVariable("servicePackage",servicePackage);
        context.setVariable("staff",staff);
        context.setVariable("timetable",timetable);
        context.setVariable("phoneNumberBooking",phoneNumber);
        context.setVariable("emailBooking",emailShow);
        context.setVariable("orderId",orderId);
        String html=springTemplateEngine.process(template,context);
        obj.put("content", html);
        Kafkabooking kafkabooking = new Kafkabooking();
        kafkabooking.setSubject((String) obj.get("subject"));
        kafkabooking.setRecipients((String) obj.get("recipients"));
        kafkabooking.setServicePackage((String) obj.get("servicePackage"));
        kafkabooking.setStaff((String) obj.get("staff"));
        kafkabooking.setAddress((String) obj.get("address"));
        kafkabooking.setTimetable((String) obj.get("timetable"));
        kafkabooking.setRecipientsName((String) obj.get("recipientsName"));
        kafkabooking.setOrderId((String) obj.get("orderId"));
        kafkabooking.setCc((String) obj.get("cc"));
        kafkabooking.setDatetime(new Date());
        kafkaBookingService.save(kafkabooking);
        String alert = obj.toJSONString();
        ProducerRecord<String, String> record = new ProducerRecord<>(sendEmail, alert);
        producer.send(record);
    }

    @PreDestroy
    public void shutdown() {
        log.info("Shutdown Kafka producer");
        producer.close();
    }
}
