package com.aladin.anfico.service.impl;

import com.aladin.anfico.domain.Voucher;
import com.aladin.anfico.repository.VoucherRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VoucherServiceImpl {
    private final Logger log = LoggerFactory.getLogger(VoucherServiceImpl.class);

    private final VoucherRepository voucherRepository;
    private final MailService mailService; // gui cho ca admin va khach -> khach

    public Voucher addVoucher(Voucher voucher){
        log.info("-- Add voucher --");



        return voucherRepository.save(voucher);
    }

}
