package com.aladin.anfico.service.impl;

import lombok.RequiredArgsConstructor;
import org.mapstruct.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.IContext;
import tech.jhipster.config.JHipsterProperties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 */
@Service
@RequiredArgsConstructor
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);


    private final JavaMailSender javaMailSender;

    @Async
    public void sendEmail(String formTo, String body ,String subject) throws MessagingException {
        log.info("send email to {}", formTo);
        MimeMessage message = javaMailSender.createMimeMessage();

        boolean multipart = true;

        MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");

        message.setContent(body, "text/html;charset=UTF-8");

        helper.setTo(formTo);

        helper.setSubject(subject);

        this.javaMailSender.send(message);
    }
}

