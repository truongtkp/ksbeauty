package com.aladin.anfico.service.dto.product;


import com.aladin.anfico.domain.ProductImage;
import com.aladin.anfico.domain.Specification;
import com.aladin.anfico.domain.SpecificationDetail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductCrudDTO {

    private Long id;

    private Long idCategory;

    @NotNull
    @Size(min = 0, max = 500)
    private String titleEn;

    @NotNull
    @Size(min = 0, max = 500)
    private String titleVi;

    @NotNull
    private String descriptionEn;

    @NotNull
    private String descriptionVi;

    @NotNull
    private String contentVi;

    @NotNull
    private String contentEn;

    private Set<Specification> specifications;

    private Set<ProductImage> productImages;

}
