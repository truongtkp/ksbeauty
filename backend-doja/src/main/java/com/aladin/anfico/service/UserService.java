package com.aladin.anfico.service;


import com.aladin.anfico.service.dto.user.*;

import java.util.HashMap;
import java.util.Optional;

public interface UserService {

    UserKeycloakDTO addUser(UserCreateDTO userCreateDTO);

    HashMap<String, ?> getAllManagedUsers(int page, int size);

    Optional<UserKeycloakDTO> updateUser(UserUpdateDTO userUpdateDTO);

    boolean deleteUser(String id);

    boolean activeUser(String userId);

    HashMap<String, ?> searchUser(String param, int page, int size);

    Optional<UserKeycloakDTO> updatePublicUser(UserPublicDTO userDTO);

    Optional<UserKeycloakDTO> getUserWithAuthoritiesByLogin(String login);

    Optional<UserKeycloakDTO> getUserWithAuthorities(String userId);

    boolean changePassword(PasswordChangeDTO passwordChangeDTO, String userId);

    boolean findOneByLoginIgnoreCase(String str);
}
