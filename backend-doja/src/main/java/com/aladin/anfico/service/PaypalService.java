package com.aladin.anfico.service;

import java.io.IOException;
import java.net.URISyntaxException;

public interface PaypalService {
    byte[] listPaymentV1(String bearer,Integer count, Integer startIndex) throws URISyntaxException, IOException;
}
