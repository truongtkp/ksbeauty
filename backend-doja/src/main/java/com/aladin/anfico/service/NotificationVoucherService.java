package com.aladin.anfico.service;

import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.util.Date;

public interface NotificationVoucherService {
    @Transactional
    void sendEmail(String email, String orderId, String subject, Date expires, Integer price, String to, String recipientsName, String cc);

    @Transactional
    void SendEmailVoucher(String template, String email, String orderId, String subject, Date expires, Integer price, String to, String recipientsName, String cc,
                          String phoneNumber, String emailShow);
}
