package com.aladin.anfico.service.dto.product;


import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductCreateDTO {

    private Long id;

    @NotNull
    private Long categoryId;
    @NotNull
    private String descriptionEn;
    @NotNull
    private String contentEn;
    @NotNull
    @Size(min = 0, max = 500)
    private String titleEn;
    @NotNull
    private List<SpecificationDTO> specificationList;
    @NotNull
    private List<ProductImageDTO> productImageList;


}
