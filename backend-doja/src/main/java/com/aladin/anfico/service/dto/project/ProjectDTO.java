package com.aladin.anfico.service.dto.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDTO {

    private Long id;

    private Instant createdDate;

    @Size(min = 0, max = 500)

    private String titleVi;

    @Size(min = 0, max = 500)

    private String titleEn;

    private String descriptionVi;

    private String descriptionEn;

    private String avatarUrl;

    private boolean priority = false;

}
