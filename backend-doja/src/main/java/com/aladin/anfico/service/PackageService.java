package com.aladin.anfico.service;

import com.aladin.anfico.domain.Packages;

import java.util.List;

public interface PackageService {
    List<Packages> getAllPackage();
}
