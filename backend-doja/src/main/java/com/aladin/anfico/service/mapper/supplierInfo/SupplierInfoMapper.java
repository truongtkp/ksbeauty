package com.aladin.anfico.service.mapper.supplierInfo;

import com.aladin.anfico.domain.SupplierInfo;
import com.aladin.anfico.service.dto.supplier.SupplierInfoDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;


/**
 * Mapper for the entity {@link SupplierInfo} and its DTO {@link SupplierInfoDTO}.
 */
@Mapper(componentModel = "spring")
public interface SupplierInfoMapper extends EntityMapper<SupplierInfoDTO, SupplierInfo> {

}
