package com.aladin.anfico.service;

import com.aladin.anfico.domain.SupplierInfo;
import com.aladin.anfico.service.dto.supplier.SupplierInfoDTO;

import java.util.HashMap;
import java.util.Optional;

public interface SupplierInfoService {

    SupplierInfoDTO addSupplier(SupplierInfoDTO supplierInfoDTO);

    Optional<SupplierInfo> updateSupplierInfo(SupplierInfoDTO supplierInfoDTO);

    Optional<SupplierInfo> deleteSupplierInfo(Long id);

    HashMap<String, ?> getAllManagedSupplierInfo(int page, int size);
}
