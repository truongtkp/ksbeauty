package com.aladin.anfico.service.dto.supplier;

import com.aladin.anfico.domain.Social;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SupplierInfoDTO {

    private Long id;

    @NotNull
    @Size(max = 500)
    private String fullname;

    @NotNull
    @Size(max = 500)
    private String link;

    @NotNull
    @Size(max = 500)
    private String avatarUrl;

    @Size(max = 500)
    @NotNull
    private String avatarPath;

    @NotNull
    private Social social;
}
