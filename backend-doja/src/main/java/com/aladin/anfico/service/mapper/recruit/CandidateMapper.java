package com.aladin.anfico.service.mapper.recruit;

import com.aladin.anfico.domain.Candidate;
import com.aladin.anfico.domain.Recruit;
import com.aladin.anfico.service.dto.recruit.CandidateDTO;
import com.aladin.anfico.service.dto.recruit.RecruitDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Candidate} and its DTO {@link CandidateDTO}.
 */
@Mapper(componentModel = "spring")
public interface CandidateMapper extends EntityMapper<CandidateDTO, Candidate> {

}

