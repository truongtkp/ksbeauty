package com.aladin.anfico.service.impl;

import com.aladin.anfico.domain.Packages;
import com.aladin.anfico.repository.PackageRepository;
import com.aladin.anfico.service.PackageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PackageServiceImpl implements PackageService {
    private final PackageRepository packageRepository;

    @Override
    public List<Packages> getAllPackage() {
        List<Packages> packages = packageRepository.findAll();
        return packages;
    }
}
