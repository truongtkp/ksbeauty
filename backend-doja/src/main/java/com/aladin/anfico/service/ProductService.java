package com.aladin.anfico.service;

import com.aladin.anfico.domain.Category;
import com.aladin.anfico.domain.Product;
import com.aladin.anfico.service.dto.product.ProductCreateDTO;
import com.aladin.anfico.service.dto.product.ProductDTO;

import java.util.HashMap;
import java.util.Optional;

public interface ProductService {

    HashMap<String, ?> getAllProduct(int page, int size);
    HashMap<String, ?> getAllProductSortedByIdDesc(int page, int size);

    HashMap<String, ?> searchProductByTitle(String param, int page, int size);


    HashMap<String, ?> filterProduct(int sort, int page, int size);

    HashMap<String, ?> filterProduct(Long categoryId, int sort, int page, int size);

    HashMap<String, ?> filterProductEn(String param, int sort, int page, int size);

    HashMap<String, ?> filterProductEn(String param, Long categoryId, int sort, int page, int size);

    Optional<ProductCreateDTO> getProductDetail(Long id);

    ProductDTO addProduct(ProductCreateDTO productCreateDTO, Category category);

    ProductDTO updateProduct(ProductCreateDTO productCreateDTO, Category category);

    Optional<Product> deleteProduct(Long id);

    Optional<Product> isPriority(Long id);
}
