package com.aladin.anfico.service.impl;

import com.aladin.anfico.service.PaypalService;

import com.aladin.anfico.service.dto.paypal.HttpResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.openid.connect.sdk.assurance.claims.CountryCode;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

@Service
@Getter
@Setter
public class PaypalServiceImpl implements PaypalService {
    private final String uri = "https://api.sandbox.paypal.com";
//    private final String uri = "https://api.paypal.com";
    private final String CLIENT_ID = "AUIedOs8C2i7GhroqNCuxF1Rkay0KlcfdyJ876DSoEpPQ84w-wE7U9M-oEOa3xWELhPp5E_TbMQUDfmK";
    private final String CLIENT_SECRET = "EBKpeWGq-ppcYsfe7BEgtF1R9dwXc9DtWWcncucxYfTDVt945ZbuivgTkLAz01dZmjG-kkITd-ukz1d-";
//    private final String CLIENT_ID = "Ad7Pg47KpUt5Kr7YFW7GfsIp9V4LJGpHlGHjFbgT8xGzU_T5HjB3DvIH_Ii3_gjLFcXb6XwVA5blFgjB";
//    private final String CLIENT_SECRET = "EN697TkDXdi6e4yyqpMse3BYhrl3zlfZgXNeWofthjOzLks4PitDRhEyQTYLIVnICt1lcxgZorSJ5Vqx";

//    private final String CLIENT_ID = "Ack1RpFGlgls1-r9uEgfpV82QJFfoZaIN1rrImU-NYDNls3BblK4qPyB_nwDw6h9vwiwiHAqQKYuUT6D";
//    private final String CLIENT_SECRET = "EFmx-Bpr19Kb1x9lWXCNrsynL7OuSqFguY8_ULRsHRu4FJXZ8mRyzdi5t4c0wJBxa7dBtILstTAz2yAb";

    private final String PAYPAL_REQUEST_ID = "PayPal-Request-Id";
    private final String CURRENCY = "USD";
//    private final String CURRENCY = "NZD";

    private String bearerTokenCache = "";

    // DEPENDENCY  INJECTION (sau nay dung bean)
    private final ObjectMapper objectMapper = new ObjectMapper();

    private HttpResponse processHttpResponse(HttpUriRequest httpUriRequest) throws IOException {
        HttpResponse res = new HttpResponse();
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(httpUriRequest);
        ){
            HttpEntity resEntity = response.getEntity();
            res.setMeta(httpClient);
            res.setResponseBody(EntityUtils.toByteArray(resEntity));
        }
        return res;
    }

    // v1 API
    public HttpResponse generateBearerAccessToken() throws IOException, URISyntaxException {
        HttpPost httpPost = new HttpPost(uri);
        httpPost.setURI(new URIBuilder(httpPost.getURI())
            .setPath("/v1/oauth2/token")
            .build()
        );

        // Request header
        byte[] client = (CLIENT_ID+":"+CLIENT_SECRET).getBytes();
        String encodedBase64Auth = Base64.getEncoder().encodeToString(client);
        httpPost.addHeader(HttpHeaders.AUTHORIZATION, "Basic " + encodedBase64Auth);

        // Request body
//        httpPost.setEntity(new StringEntity( objectMapper.writeValueAsString( new HashMap<>() {{
//            put("grant_type", "client_credentials");
//        }})));

        httpPost.setEntity(new StringEntity("grant_type=client_credentials"));
        return processHttpResponse(httpPost);
    }

    public HttpResponse generateBrainTreeAccessToken() throws IOException, URISyntaxException {
        return generateBrainTreeAccessToken(bearerTokenCache);
    }

    public HttpResponse generateBrainTreeAccessToken(String bearer) throws IOException, URISyntaxException {
        HttpPost httpPost = new HttpPost(uri);
        httpPost.setURI(new URIBuilder(httpPost.getURI())
            .setPath("/v1/identity/generate-token")
            .build()
        );
        // Request header
        httpPost.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + bearer);
        return processHttpResponse(httpPost);
    }

    public byte[] listPaymentV1(Integer count, Integer startIndex) throws URISyntaxException,IOException {
        return listPaymentV1(bearerTokenCache,count,startIndex);
    }

    @Override
    public byte[] listPaymentV1(String bearer,Integer count, Integer startIndex) throws URISyntaxException,IOException {
        HttpGet httpGet = new HttpGet(uri);
//        // params
        List<NameValuePair> params = new ArrayList<>(){{
            add(new BasicNameValuePair("count",Integer.toString(count)));
            add(new BasicNameValuePair("start_index",Integer.toString(startIndex)));
            add(new BasicNameValuePair("sort_by","create_time"));
            add(new BasicNameValuePair("sort_order","desc"));
        }};

        httpGet.setURI(new URIBuilder(httpGet.getURI())
            .setPath("/v1/payments/payment")
            .addParameters(params)
            .build()
        );
//
//        // header
        httpGet.addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE,"application/json"));
        httpGet.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION,"Bearer " + bearer));

//
//        // Start connection
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(httpGet);
        ){
            HttpEntity entity = response.getEntity();
            return EntityUtils.toByteArray(entity);
        }
    }


    // V2 API

    public HttpResponse createOrderBookingV2(String bearer, Integer price) throws URISyntaxException, IOException{
        HttpPost httpPost = new HttpPost(uri);

        httpPost.setURI(new URIBuilder(httpPost.getURI())
            .setPath("/v2/checkout/orders")
            .build()
        );

//        // header
        httpPost.addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE,"application/json"));
        httpPost.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION,"Bearer " + bearer));
        httpPost.addHeader(new BasicHeader(PAYPAL_REQUEST_ID, UUID.randomUUID().toString()));

        // body (Code sieu ban o day)
        HashMap<String, Object> reqBody = new HashMap<>() {{
            put("intent","CAPTURE"); // CAPTURE or AUTHORIZE
            put("purchase_units", new ArrayList<>(){{
                add(new HashMap<>() {{

                    put("reference_id",UUID.randomUUID().toString());
                    put("amount", new HashMap<>(){{
                        put("currency_code",CURRENCY);
                        put("value",""+price);
//                        put("value",""+price);
                    }});
                }});
            }});

            put("payment_source",new HashMap<>(){{
                put("paypal",new HashMap<>(){{
                    put("experience_context", new HashMap<>(){{
                        put("payment_method_preference", "IMMEDIATE_PAYMENT_REQUIRED");
                        put("payment_method_selected","PAYPAL");
                        put("brand_name","EXAMPLE INC");
                        put("locale","en-US");
                        put("landing_page","LOGIN");
//                        put("shipping_preference", "SET_PROVIDED_ADDRESS");
                        put("shipping_preference", "NO_SHIPPING");
                        put("user_action","PAY_NOW");
                        put("return_url","https://example.com/returnUrl");
                        put("cancel_url","https://example.com/cancelUrl");
                    }});
                }});
            }});
        }};
        httpPost.setEntity( new StringEntity(objectMapper.writeValueAsString(reqBody)));

        return processHttpResponse(httpPost);
    }

    public HttpResponse createOrderV2(String bearer) throws URISyntaxException, IOException{
        HttpPost httpPost = new HttpPost(uri);

        httpPost.setURI(new URIBuilder(httpPost.getURI())
            .setPath("/v2/checkout/orders")
            .build()
        );

//        // header
        httpPost.addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE,"application/json"));
        httpPost.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION,"Bearer " + bearer));
        httpPost.addHeader(new BasicHeader(PAYPAL_REQUEST_ID, UUID.randomUUID().toString()));

        // body (Code sieu ban o day)
        HashMap<String, Object> reqBody = new HashMap<>() {{
            put("intent","CAPTURE"); // CAPTURE or AUTHORIZE
            put("purchase_units", new ArrayList<>(){{
                add(new HashMap<>() {{

                    put("reference_id",UUID.randomUUID().toString());
                    put("amount", new HashMap<>(){{
                        put("currency_code",CURRENCY);
                        put("value","100.00");
                    }});
                }});
            }});

            put("payment_source",new HashMap<>(){{
                put("paypal",new HashMap<>(){{
                    put("experience_context", new HashMap<>(){{
                        put("payment_method_preference", "IMMEDIATE_PAYMENT_REQUIRED");
                        put("payment_method_selected","PAYPAL");
                        put("brand_name","EXAMPLE INC");
                        put("locale","en-US");
                        put("landing_page","LOGIN");
//                        put("shipping_preference", "SET_PROVIDED_ADDRESS");
                        put("shipping_preference", "NO_SHIPPING");
                        put("user_action","PAY_NOW");
                        put("return_url","https://example.com/returnUrl");
                        put("cancel_url","https://example.com/cancelUrl");
                    }});
                }});
            }});
        }};
        httpPost.setEntity( new StringEntity(objectMapper.writeValueAsString(reqBody)));

        return processHttpResponse(httpPost);
    }


    public HttpResponse updateOrderV2(String bearer, String orderId) throws URISyntaxException, IOException {
        HttpPatch httpPatch = new HttpPatch(uri);

        httpPatch.setURI(new URIBuilder(httpPatch.getURI())
            .setPath("/v2/checkout/orders/"+orderId)
            .build()
        );
        // header
        httpPatch.addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE,"application/json"));
        httpPatch.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION,"Bearer " + bearer));
        // body
        List<Object> reqBody = new ArrayList<>(){{
            add(new HashMap<>(){{
                put("op","replace");
                put("path","/purchase_units/@reference_id=='default'/shipping/address"); // ******
                put("value", new HashMap<>() {{
                    put("address_line_1","");
                    put("address_line_2","");
                    put("admin_area_2","");
                    put("admin_area_1","");
                    put("postal_code","95131");
                    put("country_code", CountryCode.DEFAULT_BYTE_LENGTH);
                }});
            }});
        }};
        httpPatch.setEntity( new StringEntity(objectMapper.writeValueAsString(reqBody)));

        return processHttpResponse(httpPatch);
    }

    public HttpResponse getOrderDetailV2(String bearer, String orderId) throws URISyntaxException,IOException {
        HttpGet httpGet = new HttpGet(uri);
//        // params
        httpGet.setURI(new URIBuilder(httpGet.getURI())
            .setPath("/v2/checkout/orders/" + orderId)
            .build()
        );
//
//        // header
        httpGet.addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE,"application/json"));
        httpGet.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION,"Bearer " + bearer));

        return processHttpResponse(httpGet);
    }


    public HttpResponse confirmPaymentSourceV2(String bearer, String orderId) throws URISyntaxException, IOException{
        HttpPost httpPost = new HttpPost(uri);
        httpPost.setURI(new URIBuilder(httpPost.getURI())
            .setPath("/v2/checkout/orders/" + orderId + "/confirm-payment-source")
            .build()
        );

        httpPost.addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE,"application/json"));
        httpPost.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION,"Bearer " + bearer));

        HashMap<String,Object> reqBody = new HashMap<>(){{
            put("payment_source",new HashMap<>(){{
                put("paypal", new HashMap<>() {{
                    put("name", new HashMap<>(){{
                        put("given_name","John");
                        put("surname","Doe");
                    }});
                    put("email_address", "sb-43utqe25374901@personal.example.com");
                    put("experience_context", new HashMap<>(){{
                        put("payment_method_preference","IMMEDIATE_PAYMENT_REQUIRED");
                        put("payment_method_selected","PAYPAL");
                        put("brand_name","EXAMPLE INC");
                        put("locale","en-US");
                        put("landing_page","LOGIN");
                        put("shipping_preference","NO_SHIPPING");
                        put("user_action","PAY_NOW");
                        put("return_url","");
                        put("cancel_url","");
                    }});
                }});
            }});
        }};

        httpPost.setEntity( new StringEntity(objectMapper.writeValueAsString(reqBody)));

        return processHttpResponse(httpPost);
    }


    public HttpResponse capturePaymentV2(String orderId) throws URISyntaxException, IOException{
        return capturePaymentV2(bearerTokenCache, orderId);
    };
    public HttpResponse capturePaymentV2(String bearer, String orderId) throws URISyntaxException,IOException {
        HttpPost httpPost = new HttpPost(uri);
//        // params
        httpPost.setURI(new URIBuilder(httpPost.getURI())
            .setPath("/v2/checkout/orders/" + orderId + "/capture")
            .build()
        );
        // header
        httpPost.addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE,"application/json"));
        httpPost.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION,"Bearer " + bearer));
        httpPost.addHeader(new BasicHeader(PAYPAL_REQUEST_ID,UUID.randomUUID().toString()));

//        // Start connection
        return processHttpResponse(httpPost);
    }
}
