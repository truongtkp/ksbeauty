package com.aladin.anfico.service.impl;

import com.aladin.anfico.config.KafkaProperties;
import com.aladin.anfico.domain.Kafkabooking;
import com.aladin.anfico.domain.Kafkavoucher;
import com.aladin.anfico.service.KafkaBookingService;
import com.aladin.anfico.service.KafkaVoucherService;
import com.aladin.anfico.service.NotificationVoucherService;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class NotificationVoucherServiceImpl implements NotificationVoucherService {

    private String sendEmail = "sendEmailDoJa";
    private final Logger log = LoggerFactory.getLogger(NotificationVoucherServiceImpl.class);
    private final KafkaProperties kafkaProperties;
    private KafkaProducer<String, String> producer;
    private final SpringTemplateEngine springTemplateEngine;
    private final KafkaVoucherService kafkaVoucherService;

    public NotificationVoucherServiceImpl(KafkaProperties kafkaProperties, SpringTemplateEngine springTemplateEngine, KafkaVoucherService kafkaVoucherService) {
        this.kafkaProperties = kafkaProperties;
        this.springTemplateEngine = springTemplateEngine;
        this.kafkaVoucherService = kafkaVoucherService;
    }


    @PostConstruct
    public void initialize() {
        log.info("Kafka producer initializing...");
        this.producer = new KafkaProducer<>(kafkaProperties.getProducerProps());
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
        log.info("Kafka producer initialized");
    }

    @Override
    @Transactional
    public void sendEmail(String email, String content, String subject, Date expires, Integer price, String to, String recipientsName, String cc) {
        JSONObject obj = new JSONObject();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        obj.put("subject", subject);
        obj.put("to",to);
        obj.put("price",price);
        obj.put("expires",formatDate.format(expires));
        obj.put("recipients", email);
        obj.put("recipientsName", recipientsName);
        obj.put("cc", cc);
        Context context=new Context();
        String html=springTemplateEngine.process(content,context);
        obj.put("content", html);
        Kafkavoucher kafkavoucher = new Kafkavoucher();
        kafkavoucher.setSubject((String) obj.get("subject"));
        kafkavoucher.setToo((String) obj.get("to"));
        kafkavoucher.setPrice((Integer) obj.get("price"));
        kafkavoucher.setExpires((Date) obj.get("expires"));
        kafkavoucher.setCc((String) obj.get("cc"));
        kafkavoucher.setRecipients((String) obj.get("recipients"));
        kafkavoucher.setRecipientsName((String) obj.get("recipientsName"));
        kafkavoucher.setOrderId((String) obj.get("content"));
        kafkavoucher.setDatetime(new Date());
        kafkaVoucherService.save(kafkavoucher);
        String alert = obj.toJSONString();
        ProducerRecord<String, String> record = new ProducerRecord<>(sendEmail, alert);
        producer.send(record);
    }

    @Override
    @Transactional
    public void SendEmailVoucher(String template, String email, String orderId, String subject, Date expires, Integer price, String too, String recipientsName, String cc, String phoneNumber, String emailShow) {
        JSONObject obj = new JSONObject();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        obj.put("subject", subject);
        obj.put("too",too);
        obj.put("price",price);
        obj.put("expires",formatDate.format(expires));
        obj.put("recipients", email);
        obj.put("recipientsName", recipientsName);
        obj.put("cc", cc);
        Context context=new Context();
        context.setVariable("too",too);
        context.setVariable("price",price);
        context.setVariable("expires",formatDate.format(expires));
        context.setVariable("customerName",recipientsName);
        context.setVariable("phoneNumber",phoneNumber);
        context.setVariable("email",emailShow);
        context.setVariable("orderId",orderId);
        String html=springTemplateEngine.process(template,context);
        obj.put("content", html);
        Kafkavoucher kafkavoucher = new Kafkavoucher();
        kafkavoucher.setSubject((String) obj.get("subject"));
        kafkavoucher.setToo((String) obj.get("too"));
        kafkavoucher.setPrice((Integer) obj.get("price"));
//        kafkavoucher.setExpires((Date) obj.get("expires"));
        kafkavoucher.setCc((String) obj.get("cc"));
        kafkavoucher.setRecipients((String) obj.get("recipients"));
        kafkavoucher.setRecipientsName((String) obj.get("recipientsName"));
        kafkavoucher.setOrderId((String) obj.get("orderId"));
        kafkavoucher.setDatetime(new Date());
        kafkaVoucherService.save(kafkavoucher);
        String alert = obj.toJSONString();
        ProducerRecord<String, String> record = new ProducerRecord<>(sendEmail, alert);
        producer.send(record);
    }

    @PreDestroy
    public void shutdown() {
        log.info("Shutdown Kafka producer");
        producer.close();
    }
}
