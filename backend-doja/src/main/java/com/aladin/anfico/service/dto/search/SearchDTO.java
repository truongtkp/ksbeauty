package com.aladin.anfico.service.dto.search;


import com.aladin.anfico.domain.News;
import com.aladin.anfico.domain.Product;
import com.aladin.anfico.domain.Project;
import com.aladin.anfico.domain.Recruit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SearchDTO {

    public SearchDTO(News news) {
        this.id = news.getId();
        this.titleEn = news.getTitleEn();
        this.titleVi = news.getTitleVi();
        this.avatarUrl = news.getAvatarUrl();
        this.descriptionEn = news.getDescriptionEn();
        this.descriptionVi = news.getDescriptionVi();
        this.categoryVi = "Tin tức";
        this.categoryEn = "News";
    }

    public SearchDTO(Product product) {
        this.id = product.getId();
        this.titleEn = product.getTitleEn();
        this.avatarUrl = product.getAvatarUrl();
        this.descriptionEn = product.getDescriptionEn();
        this.categoryEn = "Product";
        this.categoryVi = "Sản phẩm";
    }

    public SearchDTO(Recruit recruit) {
        this.id = recruit.getId();
        this.titleEn = recruit.getTitleEn();
        this.titleVi = recruit.getTitleVi();
        this.avatarUrl = recruit.getAvatarUrl();
        this.descriptionEn = recruit.getDescribeEn();
        this.descriptionVi = recruit.getDescribeVi();
        this.categoryEn = "Recruit";
        this.categoryVi = "Tuyển dụng";
    }

    public SearchDTO(Project project) {
        this.id = project.getId();
        this.titleEn = project.getTitleEn();
        this.avatarUrl = project.getAvatarUrl();
        this.descriptionEn = project.getDescriptionEn();
        this.categoryEn = "Project";
        this.categoryVi = "Dự án công ty";
    }

    private Long id;
    private String titleEn;
    private String titleVi;
    private String avatarUrl;
    private String descriptionEn;
    private String descriptionVi;
    private String categoryEn;
    private String categoryVi;
}
