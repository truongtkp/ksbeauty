package com.aladin.anfico.service.impl;

import com.aladin.anfico.repository.NewsRepository;
import com.aladin.anfico.repository.ProductRepository;
import com.aladin.anfico.repository.ProjectRepository;
import com.aladin.anfico.repository.RecruitRepository;
import com.aladin.anfico.service.HistoryService;
import com.aladin.anfico.service.NewsService;
import com.aladin.anfico.service.SearchService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.search.SearchDTO;
import com.aladin.anfico.web.rest.resource.ProductResource;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {


    private final Logger log = LoggerFactory.getLogger(SearchServiceImpl.class);


    private final NewsRepository newsRepository;
    private final ProjectRepository projectRepository;
    private final ProductRepository productRepository;
    private final RecruitRepository recruitRepository;

    private static List<SearchDTO> searchDTOS;

    private static String param = "";

    private static String type = "en";

    @Override
    public HashMap<String, ?> searchAllEn(String type, String search, int page, int size) {
        if (!param.equals(search) || !type.equals(type)) searchDTOS = new ArrayList<>();
        this.param = search;
        this.type = type;
        if (searchDTOS.size() == 0){
            searchDTOS.addAll(newsRepository.searchAllByNewsEn(search));
            searchDTOS.addAll(productRepository.searchProductEn(search));
            searchDTOS.addAll(projectRepository.searchAllByProjectEn(search));
            searchDTOS.addAll(recruitRepository.searchAllByRecruitEn(search));
            Collections.shuffle(searchDTOS);
        }
        List<SearchDTO> searchDTOS1;
        if (page*size+size >= searchDTOS.size()){
            searchDTOS1 = new ArrayList<>();
            for (int i = page*size; i < searchDTOS.size(); i++ ){
                searchDTOS1.add(searchDTOS.get(i));
            }
            return ArrayDTO.convertListToSet(searchDTOS.size(), searchDTOS1);
        }else if (page*size+size <= searchDTOS.size()){
            searchDTOS1 = new ArrayList<>();
            for (int i = page*size; i < (page*size + size); i++ ){
                searchDTOS1.add(searchDTOS.get(i));
            }
            return ArrayDTO.convertListToSet(searchDTOS.size(), searchDTOS1);
        }else{
            return ArrayDTO.convertListToSet(searchDTOS.size(),  new ArrayList<>());
        }
    }


    @Scheduled(cron = "5 * * * * *")
    public void setListSearch(){
        log.info("Update search five min before {}", param);
        this.param = "";
        log.info("Update search five min after {}", param);
    }



}
