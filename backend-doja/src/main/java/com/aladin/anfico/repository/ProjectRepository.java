package com.aladin.anfico.repository;

import com.aladin.anfico.domain.News;
import com.aladin.anfico.domain.Project;
import com.aladin.anfico.domain.Recruit;
import com.aladin.anfico.service.dto.search.SearchDTO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends JpaRepository<Project, Long> {


    @Query("select count(c.id) from Project c where c.priority = true ")
    int countProjectByPriority();

    @Query("select new Project(c.id, c.titleEn, c.descriptionEn, c.avatarUrl) from Project c where c.priority = true order by c.id desc ")
    List<Project> getProjectByPriority();

    @Query("select new Project(c.id, c.titleEn, c.descriptionEn, c.avatarUrl) from Project c where c.titleEn like %?1% ")
    List<Project> searchAllByProjectEn(String param, PageRequest pageRequest);

    @Query("select count(c.id)from Project c where c.titleEn like %?1% ")
    int countSearchEn(String param);

    @Query("select new Project(c.id, c.titleEn, c.descriptionEn, c.avatarUrl) from Project c where c.titleEn like %?1%  ")
    List<SearchDTO> searchAllByProjectEn(String param);

}
