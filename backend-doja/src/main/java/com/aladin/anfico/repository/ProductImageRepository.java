package com.aladin.anfico.repository;

import com.aladin.anfico.domain.ProductImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductImageRepository extends JpaRepository<ProductImage, Long> {


}
