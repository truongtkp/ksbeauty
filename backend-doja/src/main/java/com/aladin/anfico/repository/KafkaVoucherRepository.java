package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Kafkavoucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KafkaVoucherRepository extends JpaRepository<Kafkavoucher, Long> {
}
