package com.aladin.anfico.repository;

import com.aladin.anfico.domain.TopicImage;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface TopicImageRepository extends JpaRepository<TopicImage, Long> {

    @Query("select ti from TopicImage ti where ti.topic.id= ?1 order by ti.id asc")
    Set<TopicImage> findTopicImageByTopic(Long id);

    @Query("select ti from TopicImage ti inner join ti.topic t where ti.topic.id= ?1 group by t.id")
    TopicImage findTopicImageByTopicLimit(Long id);

}
