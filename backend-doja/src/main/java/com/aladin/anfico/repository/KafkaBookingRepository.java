package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Kafkabooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KafkaBookingRepository extends JpaRepository<Kafkabooking, Long> {
}
