package com.aladin.anfico.repository;

import com.aladin.anfico.domain.OrderVoucher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderVoucherRepository extends JpaRepository<OrderVoucher,Long> {
    @Query("select o.id from OrderVoucher o where o.voucher.id = ?1 ")
    List<Long> findOrderIdByVoucherId(Long voucherId);

//    @Query("select new OrderVoucher(o.id,o.voucher,o.orderStatus,o.balance,o.orderId,o.to,o.from,o.email,o.phoneNo,o.expirationDate) " +
//        "from OrderVoucher o where o.voucher.id = ?1 ")
//    @Query("select new OrderVoucher(o.id) "+
    @Query(value = "SELECT * FROM order_voucher WHERE voucher_id = ?1", nativeQuery = true)
    List<OrderVoucher> findOrderByVoucherId(Long voucherId);

    @Query(value = "SELECT * FROM order_voucher WHERE order_id = ?1", nativeQuery = true)
    List<OrderVoucher> findOrderByPaypalOrderId(String orderId);

    @Query(value = "select ov.* from order_voucher ov " +
        "INNER JOIN voucher v ON v.id = ov.voucher_id " +
        "WHERE ov.`to` Like CONCAT('%', :param, '%') " +
        "OR ov.`from` Like CONCAT('%', :param, '%') " +
        "OR ov.email Like CONCAT('%', :param, '%') " +
        "OR ov.phone_no Like CONCAT('%', :param, '%') " +
        "OR v.name Like CONCAT('%', :param,'%')",nativeQuery = true)
    List<OrderVoucher> searchOrderVoucher(@Param("param") String param);

    @Query(value = "select ov.* from order_voucher ov " +
        "INNER JOIN voucher v ON v.id = ov.voucher_id " +
        "WHERE ov.`to` Like CONCAT('%', :param, '%') " +
        "OR ov.`from` Like CONCAT('%', :param, '%') " +
        "OR ov.email Like CONCAT('%', :param, '%') " +
        "OR ov.phone_no Like CONCAT('%', :param, '%') " +
        "OR v.name Like CONCAT('%', :param,'%')",nativeQuery = true)
    List<OrderVoucher> searchOrderVoucher(@Param("param") String param, PageRequest pageRequest);

    @Query(value = "select * from order_voucher o ORDER BY o.id DESC",nativeQuery = true)
    List<OrderVoucher> getOrderVoucherSortedByIdDesc();

    @Query(value = "select * from order_voucher o ORDER BY o.id DESC",nativeQuery = true)
    List<OrderVoucher> getOrderVoucherSortedByIdDesc(PageRequest pageRequest);
}
