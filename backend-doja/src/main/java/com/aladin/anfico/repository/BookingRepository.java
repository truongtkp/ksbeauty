package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Booking;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface BookingRepository extends JpaRepository<Booking,Long> {
    @Query("select b.id from Booking b where b.staff.id = ?1 ")
    List<Long> findBookingIdByStaffId(Long staffId);

    @Query(value = "select * "+
        "from booking b where b.booking_date > ?1 AND b.booking_date < ?2 ",nativeQuery = true)
    List<Booking> searchBookingByDateRange(Date from,Date to);

//    @Query("select new Booking (b.id,b.address,b.servicePackage,b.staff,b.timetable,b.bookingDate,b.fullName,b.email,b.phoneNo,b.orderId) "+
//        "from Booking b where b.bookingDate = ?1")
    @Query(value = "select * "+
        "from booking b where b.booking_date between ?1 AND ?2",nativeQuery = true)
    List<Booking> searchBookingByDate(String from,String to);

    @Query(value = "SELECT * FROM booking WHERE order_id = ?1", nativeQuery = true)
    List<Booking> findBookingByPaypalOrderId(String orderId);

    @Query(value = " select b.* from booking b " +
        "INNER join address a ON a.id = b.address_id " +
        "Inner join staff s ON s.id = b.staff_id " +
        "Inner join packages p ON p.id = b.service_package_id " +
        "WHERE b.full_name Like CONCAT('%', :param, '%') " +
        "OR a.name Like CONCAT('%', :param, '%') " +
        "OR s.name like CONCAT('%', :param, '%') " +
        "OR p.title like CONCAT('%', :param, '%') ",nativeQuery = true)
    List<Booking> searchBooking(@Param("param") String param);


    @Query(value = " select b.* from booking b " +
        "INNER join address a ON a.id = b.address_id " +
        "Inner join staff s ON s.id = b.staff_id " +
        "Inner join packages p ON p.id = b.service_package_id " +
        "WHERE b.full_name Like CONCAT('%', :param, '%') " +
        "OR a.name Like CONCAT('%', :param, '%') " +
        "OR s.name like CONCAT('%', :param, '%') " +
        "OR p.title like CONCAT('%', :param, '%') ",nativeQuery = true)
    List<Booking> searchBooking(@Param("param") String param, PageRequest pageRequest);

    @Query(value = "select * from booking b ORDER BY b.id DESC",nativeQuery = true)
    List<Booking> getBookingSortedByIdDesc();

    @Query(value = "select * from booking b ORDER BY b.id DESC",nativeQuery = true)
    List<Booking> getBookingSortedByIdDesc(PageRequest pageRequest);
}
