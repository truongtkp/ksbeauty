package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Packages;
import com.aladin.anfico.domain.Staff;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PackageRepository extends JpaRepository<Packages,Long> {
    @Query("select new Packages(p.id,p.title,p.description,p.price) from " +
        "Packages p where p.title like %?1% group by p.id")
    List<Packages> searchPackages(String param, PageRequest pageRequest);

    @Query("select new Packages(p.id,p.title,p.description,p.price) from " +
        "Packages p where p.title like %?1% group by p.id")
    List<Packages> searchPackages(String param);

    @Query(value = "select * from packages p ORDER BY p.id DESC",nativeQuery = true)
    List<Packages> getPackagesSortedByIdDesc();

    @Query(value = "select * from packages p ORDER BY p.id DESC",nativeQuery = true)
    List<Packages> getPackagesSortedByIdDesc(PageRequest pageRequest);
}
