package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Blacklist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface BlacklistRepository extends JpaRepository<Blacklist, Date> {
    @Query(value = "select * "+
        "from blacklist b where b.at between ?1 AND ?2",nativeQuery = true)
    List<Blacklist> searchBlacklistByDate(String from, String to);
}
