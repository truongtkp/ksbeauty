package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Voucher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VoucherRepository extends JpaRepository<Voucher, Long> {
    @Query(value = "select * from voucher v ORDER BY v.id DESC",nativeQuery = true)
    List<Voucher> getAddressSortedByIdDesc();

    @Query(value = "select * from voucher v ORDER BY v.id DESC",nativeQuery = true)
    List<Voucher> getAddressSortedByIdDesc(PageRequest pageRequest);
}
