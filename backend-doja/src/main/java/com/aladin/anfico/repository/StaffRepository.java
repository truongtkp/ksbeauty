package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Staff;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StaffRepository extends JpaRepository<Staff,Long> {
    @Query("select new Staff(s.id,s.name,s.image,s.birthday,s.email,s.phoneNo,s.description,s.address) from " +
        "Staff s where s.name like %?1% group by s.id")
    List<Staff> searchStaff(String param, PageRequest pageRequest);

    @Query("select new Staff(s.id,s.name,s.image,s.birthday,s.email,s.phoneNo,s.description,s.address) from " +
        "Staff s where s.name like %?1% group by s.id")
    List<Staff> searchStaff(String param);

    @Query(value = "select * from staff s ORDER BY s.id DESC",nativeQuery = true)
    List<Staff> getStaffSortedByIdDesc();

    @Query(value = "select * from staff s ORDER BY s.id DESC",nativeQuery = true)
    List<Staff> getStaffSortedByIdDesc(PageRequest pageRequest);
}
