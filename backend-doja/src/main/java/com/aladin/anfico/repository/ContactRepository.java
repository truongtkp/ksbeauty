package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Contact;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact,Long> {
    @Query("select c from Contact c where c.fullname like %?1% or c.email like %?1% or c.companyName like %?1% ")
    List<Contact> searchContactsByAll(String param, PageRequest pageRequest);

    @Query("select count(c.id) from Contact c where c.fullname like %?1% or c.email like %?1% or c.companyName like %?1% ")
    int countSearch(String param);

    @Query("select c from Contact c order by c.status asc, c.id desc ")
    List<Contact> getContact( PageRequest pageRequest);

}
