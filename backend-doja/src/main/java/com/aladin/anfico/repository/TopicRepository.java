package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Topic;
import com.aladin.anfico.domain.Type;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TopicRepository extends JpaRepository<Topic, Long>{

    @Query("select t from Topic t where t.type = ?1 order by t.id desc ")
    Topic findTopicByType(Type type);

    @Query("select t from Topic t where t.type = ?1 and t.id = ?2 order by t.id desc ")
    Topic findTopicByType(Type type, Long id);

    @Query("select t from Topic t where t.type = ?1 order by t.id desc")
    List<Topic> findTopicsByType(Type type);

    @Query("select t from Topic t where t.type = ?1 order by t.id desc")
    List<Topic> findTopicsByType(Type type, PageRequest pageRequest);


//    @Query("select t from Topic t where t.type = ?1 and t.descriptionVi like %?2% order by t.id desc")
    @Query("select t from Topic t where t.type = ?1 and t.descriptionEn like %?2% order by t.id desc")
    List<Topic> searchTopicsByType(Type type, String param, PageRequest pageRequest);
}
