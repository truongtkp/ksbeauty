package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.*;
import com.aladin.anfico.repository.OrderStatusRepository;
import com.aladin.anfico.repository.OrderVoucherRepository;
import com.aladin.anfico.repository.VoucherRepository;
import com.aladin.anfico.service.NotificationVoucherService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.impl.PaypalServiceImpl;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.aspectj.weaver.ast.Or;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Order",description = "order")
public class OrderVoucherResource {
    private final Logger log = LoggerFactory.getLogger(OrderVoucherResource.class);
    private final OrderVoucherRepository orderVoucherRepository;
    private final OrderStatusRepository orderStatusRepository;
    private final VoucherRepository voucherRepository;
    private static final String ENTITY_NAME = "backEndContact";
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final PaypalServiceImpl paypalServiceImpl;
    private final ObjectMapper objectMapper;
    private final Validator validator;
    private final NotificationVoucherService notificationVoucherService;

    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private final TypeReference<Map<String,Object>> tr = new TypeReference<>() {};
    private final ObjectMapper om;

    @GetMapping("/order-voucher")
    public ResponseEntity<HashMap<String,?>> getOrders(
        @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size
    ) {
//        List<Packages> packages = packageService.getAllPackage();
//        List<OrderVoucher> orderVouchers = orderVoucherRepository.findAll();

        List<OrderVoucher> orderVouchers;
        if (page == null || size == null) {
            orderVouchers = orderVoucherRepository.getOrderVoucherSortedByIdDesc();
        } else {
            page = (page < 0)?0:page;
            size = (size <= 0)?10:size;
            orderVouchers = orderVoucherRepository.getOrderVoucherSortedByIdDesc(PageRequest.of(page,size));
        }
        HashMap<String,?> res = ArrayDTO.convertListToSet(orderVoucherRepository.count(),orderVouchers);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping("/order-voucher/{id}")
    public ResponseEntity<OrderVoucher> getOrderById(@PathVariable Long id) {
//        List<Packages> packages = packageService.getAllPackage();
        Optional<OrderVoucher> orderVoucher = orderVoucherRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(orderVoucher);
    }

    @PostMapping("/order-voucher")
    public ResponseEntity<OrderVoucher> addOrder(
        @RequestBody HashMap<String,Object> orderReq
    ) throws IOException, URISyntaxException {
        log.info("--POST: /api/voucher Order new voucher --");

        OrderVoucher order = objectMapper.convertValue(orderReq,OrderVoucher.class);
        order.setId(null);

        Set<ConstraintViolation<OrderVoucher>> violations = validator.validate(order);
        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Optional<Voucher> voucher = voucherRepository.findById(Long.valueOf((Integer) orderReq.get("voucherId")));
        if (voucher.isEmpty())throw new BadRequestAlertException("Invalid voucher","","");

        order.setOrderId(null);
        order.setVoucher(voucher.get());

        Map<String,Object> temp = om.readValue(paypalServiceImpl.generateBearerAccessToken().getResponseBody(),tr);
        String bearerToken = (String) temp.get("access_token");
        temp = om.readValue(paypalServiceImpl.createOrderBookingV2(bearerToken,voucher.get().getPrice())
                .getResponseBody()
            ,tr);
        log.info(temp.toString());
        order.setOrderId((String) temp.get("id"));
//        OrderVoucher res = orderVoucherRepository.save(order);
//        new Thread(() -> sendEmailOrderVoucher(res)).start();
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PostMapping("/order-voucher/confirm/{id}")
    public ResponseEntity<Map<String,Object>> confirmOrder(@PathVariable String id, @RequestBody HashMap<String,Object> orderReq) throws IOException, URISyntaxException {
        log.info("--POST: /api/voucher Order new voucher --");

        OrderVoucher order = objectMapper.convertValue(orderReq,OrderVoucher.class);
        order.setId(null);

        Set<ConstraintViolation<OrderVoucher>> violations = validator.validate(order);
        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Optional<Voucher> voucher = voucherRepository.findById(Long.valueOf((Integer) orderReq.get("voucherId")));
        if (voucher.isEmpty())throw new BadRequestAlertException("Invalid voucher","","");

        order.setOrderId(null);
        order.setVoucher(voucher.get());
        order.setOrderId(id);
        order.setOrderStatus(true);

        Map<String,Object> temp = om.readValue(paypalServiceImpl.generateBearerAccessToken().getResponseBody(),tr);
        String bearerToken = (String) temp.get("access_token");
        temp = om.readValue(paypalServiceImpl.capturePaymentV2(bearerToken,id).getResponseBody(),tr);

        log.info((String) temp.get("status"));
        String status = (String) temp.get("status");
        if (!Objects.equals(status, "COMPLETED")) {
            throw new BadRequestAlertException("CONFIRMATION VOUCHER FAILED","","");
        }

        OrderVoucher res = orderVoucherRepository.save(order);
        new Thread(() -> sendEmailVoucher(res)).start();

        return new ResponseEntity<>(temp,HttpStatus.OK);
    }

    @Async
    void sendEmailVoucher(OrderVoucher result) {
        try {
            notificationVoucherService.SendEmailVoucher("maildangkymienphikhachhang.html",result.getEmail(),result.getOrderId(),"[DO & JA] THANK YOU FOR SHOPPING WITH US", result.getExpirationDate(), result.getVoucher().getPrice(), result.getTo(), result.getFrom(), "dojalovemybody@gmail.com", result.getPhoneNo(), result.getEmail());
            notificationVoucherService.SendEmailVoucher("maildangkymienphistaff.html","dojalovemybody@gmail.com", result.getOrderId(), "[DO & JA] VOUCHER PAYMENT NOTIFICATION" + result.getTo(), result.getExpirationDate(), result.getVoucher().getPrice(), result.getTo(), result.getFrom(),"dojalovemybody@gmail.com", result.getPhoneNo(), result.getEmail());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PutMapping("/order-voucher")
    public ResponseEntity<OrderVoucher> updateOrderVoucher(
        @RequestBody HashMap<String, Object> orderReq
    ){
        OrderVoucher orderVoucher = objectMapper.convertValue(orderReq,OrderVoucher.class);
        Set<ConstraintViolation<OrderVoucher>> violations = validator.validate(orderVoucher);
        if (!violations.isEmpty() || orderVoucher.getId() == null)
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Optional<Voucher> voucher = voucherRepository.findById(Long.valueOf((Integer) orderReq.get("voucherId")));
        if (voucher.isEmpty())throw new BadRequestAlertException("Invalid voucher","","");
        orderVoucher.setVoucher(voucher.get());
        orderVoucher.setOrderStatus(false);
        Optional<OrderVoucher> res = Optional
            .of(orderVoucherRepository.findById(orderVoucher.getId()))
            .filter(Optional::isPresent)
            .map(param -> orderVoucherRepository.save(orderVoucher));

        return ResponseUtil.wrapOrNotFound(res);
    }



    @GetMapping("/order-voucher/search/{param}")
    public ResponseEntity<HashMap<String, ?>> searchOrderVoucher(@PathVariable String param
        , @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit
    ) {
        log.info("--GET: /api/order-voucher -- Search order voucher --");
//        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        List<OrderVoucher> orderVouchers;
        if (page == null || limit == null) {
            orderVouchers = orderVoucherRepository.searchOrderVoucher(param);
        } else {
            orderVouchers = orderVoucherRepository.searchOrderVoucher(param, PageRequest.of(page,limit));
        }
        HashMap<String,?> res = ArrayDTO.convertListToSet(orderVouchers.size(),orderVouchers);
        return ResponseEntity.ok().body(res);
    }


    @Data
    private static class OrderVoucherStatusUpdateDTO {
        @NotNull
        private Long id;
        @NotNull
        private Boolean orderStatus;
    }
    @PutMapping("/order-voucher/status")
    public ResponseEntity<OrderVoucher> updateOrderVoucherStatus(
        @RequestBody OrderVoucherStatusUpdateDTO orderReq
    ){
        Set<ConstraintViolation<OrderVoucherStatusUpdateDTO>> violations = validator.validate(orderReq);
        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Optional<OrderVoucher> res = Optional
            .of(orderVoucherRepository.findById(orderReq.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(param -> {
                param.setOrderStatus(orderReq.getOrderStatus());
                return orderVoucherRepository.save(param);
            });

        return ResponseUtil.wrapOrNotFound(res);
    }

    @DeleteMapping("/order-voucher/{id}")
    public ResponseEntity<Boolean> deleteOrderVoucher(@PathVariable Long id) {
        Optional
            .of(orderVoucherRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(param -> {
                orderVoucherRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {}, () -> {
                throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
            });

        return ResponseEntity.ok().body(true);
    }
}
