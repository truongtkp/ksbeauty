package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Packages;
import com.aladin.anfico.repository.PackageRepository;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Package",description = "package")
public class PackageResource {
    private final Logger log = LoggerFactory.getLogger(BookingResource.class);
    private final PackageRepository packageRepository;
    private final ObjectMapper objectMapper;
    private final Validator validator;

    @GetMapping("/package")
    public ResponseEntity<HashMap<String,?>> getPackages(
        @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size
    ){
//        List<Packages> packages = packageService.getAllPackage();
//        List<Packages> packages = packageRepository.findAll();
        List<Packages> packages;
        if (page == null || size == null) {
            packages = packageRepository.getPackagesSortedByIdDesc();
        } else {
            page = (page < 0)?0:page;
            size = (size <= 0)?10:size;
            packages = packageRepository.getPackagesSortedByIdDesc(PageRequest.of(page,size));
        }
        HashMap<String,?> res = ArrayDTO.convertListToSet(packageRepository.count(),packages);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PostMapping("/package")
    public ResponseEntity<Packages> addPackage(@RequestBody HashMap<String,Object> packagesReq){
        Packages packages = objectMapper.convertValue(packagesReq, Packages.class);

        Set<ConstraintViolation<Packages>> violations = validator.validate(packages);
        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");
        Packages res = packageRepository.save(packages);
        return new ResponseEntity<>(res,HttpStatus.OK);
    }

    @PutMapping("/package")
    public ResponseEntity<Packages> updatePackage(
        @RequestBody HashMap<String,Object> packageReq){
        Packages packages = objectMapper.convertValue(packageReq, Packages.class);

        Set<ConstraintViolation<Packages>> violations = validator.validate(packages);
        if (!violations.isEmpty() || packages.getId() == null)
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

//        List<Voucher> vouchers = voucherRepository.;
        Optional<Packages> res = Optional
            .of(packageRepository.findById(packages.getId()))
            .filter(Optional::isPresent)
            .map(param -> packageRepository.save(packages));

        return ResponseUtil.wrapOrNotFound(res);
    }

    @GetMapping ("/package/{id}")
    public ResponseEntity<Packages> getPackageById(@PathVariable Long id) {
        Optional<Packages> packages = packageRepository.findById(id);
        if (packages.isEmpty()){
            throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
        }
        return ResponseEntity.ok().body(packages.get());
    }

    @DeleteMapping("/package/{id}")
    public ResponseEntity<Boolean> deletePackage(@PathVariable Long id) {
//        Optional<Voucher> voucher = voucherRepository.delete
        Optional
            .of(packageRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(param -> {
                packageRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {},
                () -> {
                    throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
                }
            );
        return ResponseEntity.ok().body(true);
    }

    @GetMapping("/package/search/{param}")
    public ResponseEntity<HashMap<String, ?>> searchPackage(@PathVariable String param
        , @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit
    ) {
        log.info("--GET: /api/address -- Get product detail in product table--");
//        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        List<Packages> staffList;
        if (page == null || limit == null) {
            staffList = packageRepository.searchPackages(param);
        } else {
            staffList = packageRepository.searchPackages(param, PageRequest.of(page,limit));
        }
        HashMap<String,?> res = ArrayDTO.convertListToSet(staffList.size(),staffList);
        return ResponseEntity.ok().body(res);
    }
}
