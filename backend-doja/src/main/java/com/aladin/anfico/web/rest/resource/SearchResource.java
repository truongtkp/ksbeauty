package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.service.impl.SearchServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Null;
import java.util.HashMap;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class SearchResource {
    private final SearchServiceImpl searchService;
    @GetMapping("/search/{type}/{param}")
    public HashMap<String, ?> search(@PathVariable String type, @PathVariable String param, @RequestParam int page, @RequestParam int limit  ){
        return searchService.searchAllEn(type ,param, page, limit);
    }
}
