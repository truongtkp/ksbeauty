package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.service.FileService;
import com.aladin.anfico.service.dto.file.FileDTO;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.aladin.anfico.web.rest.errors.ImageFormatException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Upload",description = "OK")

public class UploadResource {

    private final Logger log = LoggerFactory.getLogger(com.aladin.anfico.web.rest.resource.UploadResource.class);

    private final FileService fileServiceImpl;
    @PostMapping("/upload-image")
    public ResponseEntity<HashMap<String,?>> uploadImage(@RequestParam("file") MultipartFile[] file) throws IOException {
        log.info("[Anfico App]: POST /api/upload-image -- Upload image on server");
        if(file[0].isEmpty()){
            throw new ImageFormatException("Not image upload!");
        }
        for (MultipartFile f : file){
            if (f.getSize() > 26214400) {
                throw new ImageFormatException("Image size larger than 25M");
            }else if (f.getOriginalFilename().endsWith(".jpg") || f.getOriginalFilename().endsWith(".JPG") ||
                    f.getOriginalFilename().endsWith(".JPEG") || f.getOriginalFilename().endsWith(".PNG") ||
                f.getOriginalFilename().endsWith(".jpeg") || f.getOriginalFilename().endsWith(".png") ||
                f.getOriginalFilename().endsWith(".ARW") || f.getOriginalFilename().endsWith(".arw") ||
                f.getOriginalFilename().endsWith(".WEBP") || f.getOriginalFilename().endsWith(".webp")){
            }else{
                throw new ImageFormatException("Image upload not jpg");
            }
        }
        HashMap<String,?> map = fileServiceImpl.uploadImage(file);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @DeleteMapping("/delete-file")
    public ResponseEntity<Boolean> deleteFile(String path){
        log.info("[Anfico App]: DELETE /api/delete-file -- Delete image on server");
        return new ResponseEntity<>(fileServiceImpl.deleteFile(path), HttpStatus.OK);
    }

    @PostMapping("/upload-file")
    public ResponseEntity<FileDTO> uploadFile(@RequestParam("file") MultipartFile[] file) throws IOException{
        log.info("[Anfico App]: POST /api/upload-file -- Upload file on server");
        if (file[0].getSize() > 26214400) {
            throw new BadRequestAlertException("file size larger than 25M", "upload-management", "upload");
        }else if(file.length > 1 || file.length == 0){
            throw new BadRequestAlertException("not file upload", "upload-management", "upload");
        }
        return new ResponseEntity<>(fileServiceImpl.uploadFile(file[0]), HttpStatus.OK);
    }

    @GetMapping("/download-file")
    public ResponseEntity<?> downloadFile(@RequestParam("file") String param) throws IOException{
        log.info("[Anfico App]: GET /api/download-image --  Download file on server");
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+ param.substring(param.length()-10));
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType("application/octet-stream"))
            .headers(header)
            .body(fileServiceImpl.downloadFile(param));
    }
}
