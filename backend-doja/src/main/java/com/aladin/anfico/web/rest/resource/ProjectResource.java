package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Project;
import com.aladin.anfico.repository.ProjectRepository;
import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.project.ProjectCreateDTO;
import com.aladin.anfico.service.dto.project.ProjectDTO;
import com.aladin.anfico.service.dto.project.ProjectHomeDTO;
import com.aladin.anfico.service.impl.ProjectServiceImpl;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.aladin.anfico.web.rest.errors.PageableRequestAlertException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Project",description = "OK")
public class ProjectResource {

    private final Logger log = LoggerFactory.getLogger(ProjectResource.class);

    private final ProjectServiceImpl projectServiceImpl;

    private final ProjectRepository projectRepository;


    @GetMapping("/projects")
    public ResponseEntity<HashMap<String,?>> getAllProject(@RequestParam int page, @RequestParam int limit ) {
        log.info("--GET: /api/upload-image -- Get project in project table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        HashMap<String,?> pages =  projectServiceImpl.getAllManagedProject(page,limit);
        return new ResponseEntity<>(pages, HttpStatus.OK);
    }

    @GetMapping("/project/home")
    public ResponseEntity<HashMap<String,?>> getAllProjectHome(@RequestParam int page, @RequestParam int limit ) {
        log.info("--GET: /api/upload-image -- Get project in project table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        HashMap<String,?> pages =  projectServiceImpl.getAllProject(page,limit);
        return new ResponseEntity<>(pages, HttpStatus.OK);
    }


    @PostMapping("/project")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Project> createProject(@RequestBody ProjectCreateDTO projectCreateDTO ) {
        log.info("--POST: /api/project -- add project in project table--");
        if (projectCreateDTO.getId() != null) {
            throw new BadRequestAlertException(Alerts.ID_NULL, "project-management", "id");
        }
        Project  n =  projectServiceImpl.createProject(projectCreateDTO);
        return new ResponseEntity<>(n, HttpStatus.OK);
    }

    @PutMapping("/project")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<ProjectDTO> updateProject(@RequestBody ProjectCreateDTO projectDTO ){
        log.info("--PUT: /api/project -- update project in project table--");
            return ResponseUtil.wrapOrNotFound(projectServiceImpl.updateProject(projectDTO));
    }

    @DeleteMapping("/project/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Boolean> deleteProject(@PathVariable Long id){
        log.info("--PUT: /api/project/status/{id} -- active project in project table--");
        if (!projectServiceImpl.deleteProject(id).isPresent())
            throw new BadRequestAlertException(Alerts.ID_EXIST, "project-service", "id");
        return new ResponseEntity<>(true,HttpStatus.OK);
    }
    @GetMapping("/project/detail/{id}")
    public ResponseEntity<Project> getProjectById(@PathVariable Long id){
        log.info("--PUT: /api/project/status/{id} -- active project in project table--");
        return ResponseUtil.wrapOrNotFound(projectRepository.findById(id));
    }
    @PutMapping("/project/priority/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Project> isPriorityProject(@PathVariable Long id){
        log.info("--PUT: /api/project/status/{id} -- active project in project table--");
        return ResponseUtil.wrapOrNotFound(projectServiceImpl.isPriority(id));
    }
    @GetMapping("/project/priority")
    public ResponseEntity<List<ProjectHomeDTO>> getProjectById(){
        log.info("--PUT: /api/project/status/{id} -- active project in project table--");
        return ResponseUtil.wrapOrNotFound(projectServiceImpl.getAllPriority());
    }


    @GetMapping("/project/search/{type}/{param}")
    public ResponseEntity<HashMap<String,?>> searchproject(@PathVariable String param, @PathVariable String type,@RequestParam int page, @RequestParam int limit ) {
        log.info("--GET: /api/upload-image -- Get project in project table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        return ResponseEntity.ok( projectServiceImpl.searchProjectEn(param, page, limit));
    }
}
