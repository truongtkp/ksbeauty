package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Address;
import com.aladin.anfico.repository.AddressRepository;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.aladin.anfico.web.rest.errors.PageableRequestAlertException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Address",description = "address")
public class AddressResources {
    private final Logger log = LoggerFactory.getLogger(AddressResources.class);
    private final AddressRepository addressRepository;
    private final ObjectMapper objectMapper;
    private final Validator validator;

    @GetMapping("address")
    public ResponseEntity<HashMap<String,?>> getAllAddress(
        @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit
    ){
        List<Address> addresses;
        if (page == null || limit == null) {
            addresses = addressRepository.getAddressSortedByIdDesc();
        } else {
            page = (page < 0)?0:page;
            limit = (limit <= 0)?10:limit;
            addresses = addressRepository.getAddressSortedByIdDesc(PageRequest.of(page,limit));
        }
//        List<Address> address = addressRepository.findAll();
        HashMap<String,?> res = ArrayDTO.convertListToSet(addressRepository.count(), addresses);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PostMapping("/address")
    public ResponseEntity<Address> addAddress(
        @RequestBody HashMap<String,Object> addressReq
    ){
        log.info("--POST: /api/voucher Add new voucher --");
        Address address = objectMapper.convertValue(addressReq, Address.class);
        Set<ConstraintViolation<Address>> violations = validator.validate(address);
        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Address res = addressRepository.save(address);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PutMapping("/address")
    public ResponseEntity<Address> updateAddress(
        @RequestBody HashMap<String,Object> addressReq){
        Address address = objectMapper.convertValue(addressReq, Address.class);

        Set<ConstraintViolation<Address>> violations = validator.validate(address);
        if (!violations.isEmpty() || address.getId() == null)
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

//        List<Voucher> vouchers = voucherRepository.;
        Optional<Address> res = Optional
            .of(addressRepository.findById(address.getId()))
            .filter(Optional::isPresent)
            .map(params -> addressRepository.save(address));

        return ResponseUtil.wrapOrNotFound(res);
    }

    @DeleteMapping("/address/{id}")
    public ResponseEntity<Boolean> deleteAddress(@PathVariable Long id) {
        Optional
            .of(addressRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(param -> {
                addressRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {},
                () -> {
                    throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
                }
            );
        return ResponseEntity.ok().body(true);
    }

    @GetMapping ("/address/{id}")
    public ResponseEntity<Address> getAddressById(@PathVariable Long id) {
        Optional<Address> address = addressRepository.findById(id);
        if (address.isEmpty()){
            throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
        }
        return ResponseEntity.ok().body(address.get());
    }

//    @GetMapping("/address/search/{param}")
//    public ResponseEntity<HashMap<String, ?>> searchAddress(@PathVariable String param) {
//        log.info("--GET: /api/address -- Get product detail in product table--");
////        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
//        List<Address> addresses = addressRepository.searchAddress(param);
//        HashMap<String,?> res = ArrayDTO.convertListToSet(addresses.size(),addresses);
//        return ResponseEntity.ok().body(res);
//    }

    @GetMapping("/address/search/{param}")
    public ResponseEntity<HashMap<String, ?>> searchAddress(@PathVariable String param
        , @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit
    ) {
        log.info("--GET: /api/address -- Get product detail in product table--");
//        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        List<Address> addresses;
        if (page == null || limit == null) {
            addresses = addressRepository.searchAddress(param);
        } else {
            addresses = addressRepository.searchAddress(param, PageRequest.of(page,limit));
        }
        HashMap<String,?> res = ArrayDTO.convertListToSet(addresses.size(),addresses);
        return ResponseEntity.ok().body(res);
    }
}
