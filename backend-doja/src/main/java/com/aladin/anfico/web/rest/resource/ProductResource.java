package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Category;
import com.aladin.anfico.domain.Product;
import com.aladin.anfico.repository.CategoryRepository;
import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.ProductService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.product.ProductCreateDTO;
import com.aladin.anfico.service.dto.product.ProductDTO;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.aladin.anfico.web.rest.errors.PageableRequestAlertException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Product",description = "product")
public class ProductResource {

    private final ProductService productServiceImpl;
    private final CategoryRepository categoryRepository;
    private final Logger log = LoggerFactory.getLogger(ProductResource.class);

    @PostMapping("/product")
//    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<ProductDTO> addProduct( @Valid @RequestBody ProductCreateDTO productCreateDTO ) {
        log.info("--POST: /api/product -- add product in news table--");
        Optional<Category> category = categoryRepository.findById(productCreateDTO.getCategoryId());
        if (!category.isPresent()){
            throw new BadRequestAlertException("Category not found", "category-management", "idexists");
        }
        if (productCreateDTO.getId() != null) {
            throw new BadRequestAlertException("A new product cannot already have an ID", "news-Management", "idexists");
        }
        ProductDTO productDTO =  productServiceImpl.addProduct(productCreateDTO, category.get());
        return new ResponseEntity<>(productDTO, HttpStatus.OK);
    }

    @PutMapping("/product")
//    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<ProductDTO> updateProduct( @RequestBody ProductCreateDTO productCreateDTO ) {
        log.info("--POST: /api/product -- add product in news table--");
        Category category = categoryRepository.findById(productCreateDTO.getCategoryId()).get();
        if (category == null){
            throw new BadRequestAlertException("Category not found", "category-management", "idexists");
        }
        ProductDTO productDTO =  productServiceImpl.updateProduct(productCreateDTO, category);
        return new ResponseEntity<>(productDTO, HttpStatus.OK);
    }
    @GetMapping("/product")
    public ResponseEntity<HashMap<String,?>> getAllProduct(@RequestParam int page, @RequestParam int limit ) {
        log.info("--GET: /api/product-- Get product in product table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
//        HashMap<String,?> pages =  productServiceImpl.getAllProduct(page,limit);
        HashMap<String,?> pages =  productServiceImpl.getAllProductSortedByIdDesc(page,limit);
        return new ResponseEntity<>(pages, HttpStatus.OK);
    }

    @GetMapping("/product/detail/{id}")
    public ResponseEntity<ProductCreateDTO> getProductDetail(@PathVariable Long id) {
        log.info("--GET: /api/product-- Get product detail in product table--");
        Optional<ProductCreateDTO> pages =  productServiceImpl.getProductDetail(id);
        return ResponseUtil.wrapOrNotFound(pages);
    }

    @GetMapping("/product/search/{param}")
    public ResponseEntity<HashMap<String, ?>> searchProduct(@PathVariable String param, @RequestParam int page, @RequestParam int limit) {
        log.info("--GET: /api/product-- Get product detail in product table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        return ResponseEntity.ok().body(productServiceImpl.searchProductByTitle(param, page, limit));
    }

    @GetMapping("/product/filter/{type}")
    public ResponseEntity<HashMap<String, ?>> filterProduct(@PathVariable String type, @RequestParam String search, @RequestParam Long categoryId, @RequestParam int page, @RequestParam int limit, @RequestParam int sort) {
        log.info("--GET: /api/product-- Get search and category null detail in product table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        if (search.isEmpty() && categoryId == 0 ) return ResponseEntity.ok().body(productServiceImpl.filterProduct(sort, page, limit));
        if (search.isEmpty()){
            log.info("--GET: /api/product-- Get search null detail in product table--");
            if (!categoryRepository.findById(categoryId).isPresent()){
                throw new BadRequestAlertException(Alerts.ID_EXIST, "product-management", "id");
            }
            return ResponseEntity.ok().body(productServiceImpl.filterProduct(categoryId, sort, page, limit));
        }
        if (categoryId == 0) {
            log.info("--GET: /api/product-- Get category null detail in product table--");
            return ResponseEntity.ok().body(productServiceImpl.filterProductEn(search, sort, page, limit));
        }
        if (!categoryRepository.findById(categoryId).isPresent()){
            throw new BadRequestAlertException(Alerts.ID_EXIST, "product-management", "id");
        }
        log.info("--GET: /api/product-- Get categoryv and search  detail in product table--");
        return ResponseEntity.ok().body(productServiceImpl.filterProductEn(search,categoryId,sort, page, limit));
    }


    @DeleteMapping("/product/{id}")
    public ResponseEntity<Boolean> deleteProduct(@PathVariable Long id) {
        log.info("--GET: /api/product-- Get product detail in product table--");
        Optional<Product> product = productServiceImpl.deleteProduct(id);
        if (!product.isPresent())
            throw new BadRequestAlertException(Alerts.ID_EXIST, "product-management", "id");
        return new ResponseEntity<>(true, HttpStatus.OK);
    }


    @PutMapping("/product/priority/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Product> isPriorityProject(@PathVariable Long id) {
        log.info("--PUT: /api/project/status/{id} -- active project in project table--");
        return ResponseUtil.wrapOrNotFound(productServiceImpl.isPriority(id));
    }
}
