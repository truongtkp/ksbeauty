package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.service.impl.ContactServiceImpl;
import com.aladin.anfico.web.rest.vm.LoginVM;

import javax.validation.Valid;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.web.bind.annotation.*;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "login",description = "OK")

public class LoginResource {

    private final ContactServiceImpl contactServiceImpl;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final Logger log = LoggerFactory.getLogger(LoginResource.class);


    @PostMapping("/authenticate")
    public ResponseEntity<?> authorize(@Valid @RequestBody LoginVM loginVM) {
//        log.info("--POST: /api/authenticate -- Sign In--");
//        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
//            loginVM.getUsername(),
//            loginVM.getPassword()
//        );
//        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//        String jwt = tokenProvider.createToken(authentication, loginVM.isRememberMe());
//        String expiresIn = dateService.convertDateToStringJWT(tokenProvider.getExpired(loginVM.isRememberMe()));
//        HttpHeaders httpHeaders = new HttpHeaders();
//        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
//        return new ResponseEntity<>(new JWTToken(jwt, expiresIn), httpHeaders, HttpStatus.OK);
        return null;
    }

}
