package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.*;
import com.aladin.anfico.repository.*;
import com.aladin.anfico.service.NotificationBookingService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.paypal.HttpResponse;
import com.aladin.anfico.service.impl.MailService;
import com.aladin.anfico.service.impl.PaypalServiceImpl;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Booking",description = "booking")
public class BookingResource {

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final Logger log = LoggerFactory.getLogger(BookingResource.class);
    private final AddressRepository addressRepository;
    private final PackageRepository packageRepository;
    private final StaffRepository staffRepository;
    private final BookingRepository bookingRepository;
    private final TimeTableRepository timeTableRepository;
    private final OrderVoucherRepository orderVoucherRepository;
    private final BlacklistRepository blacklistRepository;

    private final NotificationBookingService notificationBookingService;
    private final PaypalServiceImpl paypalServiceImpl;
    private final ObjectMapper objectMapper;

    private final Validator validator;

    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private final TypeReference<Map<String,Object>> tr = new TypeReference<>() {};
    private final ObjectMapper om;

    private final MailService mailService;

    @PostMapping("/booking")
    public ResponseEntity<Booking> addBooking(
//        @Valid @RequestBody BookingCreateDTO bookingCreateDTO
        @RequestBody HashMap<String,Object> bookingReq
    ) throws IOException, URISyntaxException {
        log.info("--POST: /api/booking Add booking --");

        Booking booking = objectMapper.convertValue(bookingReq,Booking.class);
        Set<ConstraintViolation<Booking>> violations = validator.validate(booking);

        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Optional<Address> address = addressRepository.findById(Long.valueOf((Integer) bookingReq.get("addressId")));
        if (address.isEmpty())throw new BadRequestAlertException("Address not found","","");

        Optional<Packages> servicePackage = packageRepository.findById(Long.valueOf((Integer) bookingReq.get("packageId")));
        if (servicePackage.isEmpty())throw new BadRequestAlertException("Service (Package) not found","","");

        Optional<Staff> staff = staffRepository.findById(Long.valueOf((Integer) bookingReq.get("staffId")));
        if (staff.isEmpty())throw new BadRequestAlertException("Staff not found","","");

        String dateStr = (String) bookingReq.get("bookingDate");
        Date bookingDate;
        try {
            bookingDate = df.parse((String) bookingReq.get("bookingDate"));
//            bookingDate.setDate(bookingDate.getDate() + 1); // fix time
            bookingDate.setDate(bookingDate.getDate()); // fix time
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }
        booking.setOrderId(null);
        booking.setAddress(address.get());
        booking.setServicePackage(servicePackage.get());
        booking.setStaff(staff.get());
        booking.setBookingDate(bookingDate);

        booking.setOrderVoucher(null);
        Optional<OrderVoucher> voucherCode = orderVoucherRepository.findById(1L);
        if (voucherCode.isPresent()){
            // apply voucher
//            booking.setOrderVoucher();
        }

        int hour = (Integer) bookingReq.get("hour");
        int minute = (Integer) bookingReq.get("minute");
        booking.setTimetable(
            new Time(hour, minute, 0)
        );
        String timeStr = ((hour < 10)?"0":"") + hour + ":" + ((minute < 10)?"0":"") + minute;

        List<Blacklist> blacklists = blacklistRepository.searchBlacklistByDate(dateStr+" 00:00:00", dateStr+ " 23:59:59");
        if (!blacklists.isEmpty() ){
            Blacklist bl = blacklists.get(0);
            HashMap<String,Object> res = bl.getTab();
            HashMap<String,Boolean> res1 = (HashMap<String, Boolean>) res.get(timeStr);
            if (res1 != null && res1.get(""+ bookingReq.get("staffId")) != null && res1.get(""+ bookingReq.get("staffId"))){
                throw new BadRequestAlertException("Blocked","","");
            }
        }

        Map<String,Object> temp = om.readValue(paypalServiceImpl.generateBearerAccessToken().getResponseBody(),tr);
        String bearerToken = (String) temp.get("access_token");
        temp = om.readValue(paypalServiceImpl.createOrderBookingV2(bearerToken,servicePackage.get().getPrice())
            .getResponseBody()
            ,tr);
        log.info(temp.toString());
        booking.setOrderId((String) temp.get("id"));
//        Booking res = bookingRepository.save(booking);

        return new ResponseEntity<>(booking,HttpStatus.OK);
    }




//    @PostMapping("/booking")
//    public ResponseEntity<Booking> addBooking(
////        @Valid @RequestBody BookingCreateDTO bookingCreateDTO
//        @RequestBody HashMap<String,Object> bookingReq
//    ) throws IOException, URISyntaxException {
//        log.info("--POST: /api/booking Add booking --");
//
//        Booking booking = objectMapper.convertValue(bookingReq,Booking.class);
//        Set<ConstraintViolation<Booking>> violations = validator.validate(booking);
//
//        if (!violations.isEmpty() || booking.getServicePackage().isEmpty())
//            throw new BadRequestAlertException("Validation failed",violations.toString(),"");
//
//
//
//        Optional<Address> address = addressRepository.findById(Long.valueOf((Integer) bookingReq.get("addressId")));
//        if (address.isEmpty())throw new BadRequestAlertException("Address not found","","");
//
//        Optional<Packages> servicePackage = packageRepository.findById(Long.valueOf((Integer) bookingReq.get("packageId")));
//        if (servicePackage.isEmpty())throw new BadRequestAlertException("Service (Package) not found","","");
//
//        Optional<Staff> staff = staffRepository.findById(Long.valueOf((Integer) bookingReq.get("staffId")));
//        if (staff.isEmpty())throw new BadRequestAlertException("Staff not found","","");
//
//        Date bookingDate;
//        try {
//            bookingDate = df.parse((String) bookingReq.get("bookingDate"));
//            bookingDate.setDate(bookingDate.getDate() + 1); // fix time
//        } catch (ParseException e) {
//            throw new BadRequestAlertException("Invalid date format","","");
//        }
//        booking.setOrderId(null);
//        booking.setAddress(address.get());
////        booking.setServicePackage(servicePackage.get());
//        booking.setStaff(staff.get());
//        booking.setBookingDate(bookingDate);
//        booking.setTimetable(
//            new Time(
//                (Integer) bookingReq.get("hour"),
//                (Integer) bookingReq.get("minute"),
//                0
//            )
////            timeTable.get()
//        );
//
//        Map<String,Object> temp = om.readValue(paypalServiceImpl.generateBearerAccessToken().getResponseBody(),tr);
//        String bearerToken = (String) temp.get("access_token");
//        temp = om.readValue(paypalServiceImpl.createOrderBookingV2(bearerToken,servicePackage.get().getPrice())
//                .getResponseBody()
//            ,tr);
//        log.info(temp.toString());
//        booking.setOrderId((String) temp.get("id"));
////        Booking res = bookingRepository.save(booking);
//
//        return new ResponseEntity<>(booking,HttpStatus.OK);
//    }


    @PostMapping("/booking/confirm/{id}")
    public ResponseEntity<Map<String,Object>> confirmBooking(@PathVariable String id, @RequestBody Map<String,Object> bookingReq) throws IOException, URISyntaxException {
        Booking booking = objectMapper.convertValue(bookingReq,Booking.class);
        Set<ConstraintViolation<Booking>> violations = validator.validate(booking);

        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Optional<Address> address = addressRepository.findById(Long.valueOf((Integer) bookingReq.get("addressId")));
        if (address.isEmpty())throw new BadRequestAlertException("Address not found","","");

        Optional<Packages> servicePackage = packageRepository.findById(Long.valueOf((Integer) bookingReq.get("packageId")));
        if (servicePackage.isEmpty())throw new BadRequestAlertException("Service (Package) not found","","");

        Optional<Staff> staff = staffRepository.findById(Long.valueOf((Integer) bookingReq.get("staffId")));
        if (staff.isEmpty())throw new BadRequestAlertException("Staff not found","","");

        booking.setOrderVoucher(null);
        Optional<OrderVoucher> voucherCode = orderVoucherRepository.findById(1L);
        if (voucherCode.isPresent()){
            // apply voucher
//            booking.setOrderVoucher();
        }

        Date bookingDate;
        try {
            bookingDate = df.parse((String) bookingReq.get("bookingDate"));
//            bookingDate.setDate(bookingDate.getDate() + 1); // fix time
            bookingDate.setDate(bookingDate.getDate()); // fix time
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }

        booking.setOrderId(null);
        booking.setAddress(address.get());
        booking.setServicePackage(servicePackage.get());
        booking.setStaff(staff.get());
        booking.setBookingDate(bookingDate);
        booking.setTimetable(
            new Time(
                (Integer) bookingReq.get("hour"),
                (Integer) bookingReq.get("minute"),
                0
            )
//            timeTable.get()
        );
        booking.setOrderId(id);

        Map<String,Object> temp = om.readValue(paypalServiceImpl.generateBearerAccessToken().getResponseBody(),tr);
        String bearerToken = (String) temp.get("access_token");

        HttpResponse returnConfirm = paypalServiceImpl.capturePaymentV2(bearerToken,id);
        temp = om.readValue(returnConfirm.getResponseBody(),tr);
        log.info(temp.toString());

        log.info((String) temp.get("status"));
        String status = (String) temp.get("status");
        if (!Objects.equals(status, "COMPLETED")) {
            throw new BadRequestAlertException("CONFIRMATION BOOKING FAILED","","");
        }

        Booking res = bookingRepository.save(booking);
        new Thread(() -> sendEmailBooking(res)).start();

        return new ResponseEntity<>(temp,HttpStatus.OK);
    }

    @Async
    void sendEmailBooking(Booking result) {
        try {
            notificationBookingService.SendEmailBooking("mailbookingdangkymienphikhachhang.html",result.getEmail(),
                (result.getOrderVoucher() == null)? "":result.getOrderVoucher().getOrderId() ,"[DO & JA] THANK YOU FOR SHOPPING WITH US", result.getBookingDate(), result.getAddress().getName() + " " + result.getAddress().getAddress(), result.getServicePackage().getTitle(), result.getStaff().getName(), result.getTimetable().toString(), result.getFullName(),"dojalovemybody@gmail.com", result.getPhoneNo(), result.getEmail());
            notificationBookingService.SendEmailBooking("mailbookingdangkymienphistaff.html", "dojalovemybody@gmail.com",
                (result.getOrderVoucher() == null)? "":result.getOrderVoucher().getOrderId() ,"[DO & JA] VOUCHER PAYMENT NOTIFICATION " + result.getFullName(), result.getBookingDate(), result.getAddress().getName() + " " + result.getAddress().getAddress(), result.getServicePackage().getTitle(), result.getStaff().getName(), result.getTimetable().toString(), result.getFullName(),"dojalovemybody@gmail.com", result.getPhoneNo(), result.getEmail());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/booking")
    public ResponseEntity<HashMap<String,?>> getAllBooking(
        @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size
    ){

//        List<Booking> bookings = bookingRepository.findAll();
        List<Booking> bookings;
        if (page == null || size == null) {
            bookings = bookingRepository.getBookingSortedByIdDesc();
        } else {
            page = (page < 0)?0:page;
            size = (size <= 0)?10:size;
            bookings = bookingRepository.getBookingSortedByIdDesc(PageRequest.of(page,size));
        }

//        List<Booking> bookings = bookingRepository.getBookingSortedByIdDesc();
        HashMap<String,?> res = ArrayDTO.convertListToSet(bookingRepository.count(),bookings);
        return new ResponseEntity<>(res,HttpStatus.OK);
    }

    @PutMapping("/booking")
    public ResponseEntity<Booking> updateBooking(
        @RequestBody HashMap<String, Object> bookingReq
    ){
        Booking booking = objectMapper.convertValue(bookingReq,Booking.class);
        Set<ConstraintViolation<Booking>> violations = validator.validate(booking);
        if (!violations.isEmpty() || booking.getId() == null)
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Optional<Address> address = addressRepository.findById(Long.valueOf((Integer)bookingReq.get("addressId")));
        if (address.isEmpty())throw new BadRequestAlertException("Address not found","","");

        Optional<Packages> servicePackage = packageRepository.findById(Long.valueOf((Integer) bookingReq.get("packageId")));
        if (servicePackage.isEmpty())throw new BadRequestAlertException("Service (Package) not found","","");

        Optional<Staff> staff = staffRepository.findById(Long.valueOf((Integer) bookingReq.get("staffId")));
        if (staff.isEmpty())throw new BadRequestAlertException("Staff not found","","");
        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        booking.setAddress(address.get());
        booking.setServicePackage(servicePackage.get());
        booking.setStaff(staff.get());

        Optional<Booking> res = Optional
            .of(bookingRepository.findById(booking.getId()))
            .filter(Optional::isPresent)
            .map(param -> bookingRepository.save(booking));

        return ResponseUtil.wrapOrNotFound(res);
    }

    @GetMapping ("/booking/{id}")
    public ResponseEntity<Booking> getBookingById(@PathVariable Long id) {
        Optional<Booking> booking = bookingRepository.findById(id);
        if (booking.isEmpty()){
            throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
        }
        return ResponseEntity.ok().body(booking.get());
    }

    @GetMapping("/booking/search/{param}")
    public ResponseEntity<HashMap<String, ?>> searchBooking(@PathVariable String param
        , @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit
    ) {
        log.info("--GET: /api/address -- Get product detail in product table--");
//        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        List<Booking> bookings;
        if (page == null || limit == null) {
            bookings = bookingRepository.searchBooking(param);
        } else {
            bookings = bookingRepository.searchBooking(param, PageRequest.of(page,limit));
        }
        HashMap<String,?> res = ArrayDTO.convertListToSet(bookings.size(),bookings);
        return ResponseEntity.ok().body(res);
    }

    @DeleteMapping("/booking/{id}")
    public ResponseEntity<Boolean> deleteBooking(@PathVariable Long id) {
        Optional
            .of(bookingRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(param -> {
                bookingRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {}, () -> {
                throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
            });

        return ResponseEntity.ok().body(true);
    }


    @GetMapping("/booking/date")
    public ResponseEntity<HashMap<String, ?>> searchBookingByDateRange(
        @RequestParam String from, @RequestParam String to
    ) {
        log.info("--GET: /api/address -- Get product detail in product table--");

        Date fromDate;
        Date toDate;
        try {
            fromDate = df.parse(from);
            toDate = df.parse(to);
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }
        List<Booking> bookings = bookingRepository.searchBookingByDateRange(fromDate,toDate);
        HashMap<String,?> res = ArrayDTO.convertListToSet(bookings.size(),bookings);
        return ResponseEntity.ok().body(res);
    }

    @GetMapping("/booking/date/{at}")
    public ResponseEntity<HashMap<String, ?>> searchBookingByDate(@PathVariable String at
    ) {
        log.info("--GET: /api/date -- Get product detail in product table--");

        Date atDate;
        try {
            atDate = df.parse(at);
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }
        log.info("Date validation passed! => " + atDate.toString());

        List<Booking> bookings = bookingRepository.searchBookingByDate(at+" 00:00:00", at+ " 23:59:59");
        HashMap<String,?> res = ArrayDTO.convertListToSet(bookings.size(),bookings);
        return ResponseEntity.ok().body(res);
    }
}
