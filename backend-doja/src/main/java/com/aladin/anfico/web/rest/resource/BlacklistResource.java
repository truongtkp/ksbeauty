package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.domain.Blacklist;
import com.aladin.anfico.domain.Booking;
import com.aladin.anfico.repository.BlacklistRepository;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Blacklist",description = "blacklist")
public class BlacklistResource {
    private final Logger log = LoggerFactory.getLogger(BlacklistResource.class);
    private final BlacklistRepository blacklistRepository;
    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    @GetMapping("/blacklist/{dateStr}")
    public ResponseEntity<Blacklist> getBlacklist(@PathVariable String dateStr) throws IOException, URISyntaxException {

        Date date;
        try {
            date = df.parse(dateStr);
//            date.setDate(date.getDate() + 1); // fix time
            date.setDate(date.getDate()); // fix time
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }

        List<Blacklist> blacklists = blacklistRepository.searchBlacklistByDate(dateStr+" 00:00:00", dateStr+ " 23:59:59");
        log.info(blacklists.toString());
        if (blacklists.isEmpty()) {
            return null;
        }
        return new ResponseEntity<>(blacklists.get(0),HttpStatus.OK);
    }

    @PostMapping("/blacklist/force")
    public ResponseEntity<Blacklist> addBlacklist(
        @RequestBody HashMap<String,Object> blacklistReq
    ) throws IOException, URISyntaxException {
        log.info("--POST: /api/booking Add booking --");
        log.info(blacklistReq.toString());
        Blacklist bl = new Blacklist();
        bl.setAt(new Date());
        bl.setTab(blacklistReq);
        blacklistRepository.save(bl);
        return null;
    }


    @PostMapping("/blacklist/{dateStr}")
    public ResponseEntity<Blacklist> putBlacklist(
        @PathVariable String dateStr,
        @RequestBody HashMap<String,Object> blacklistReq
//        List<HashMap<String,Object>> blacklistReq
    ) throws IOException, URISyntaxException {
        log.info("--POST: /api/booking Add booking --");
        log.info(blacklistReq.toString());

        Date date;
        try {
            date = df.parse(dateStr);
//            date.setDate(date.getDate() + 1); // fix time
            date.setDate(date.getDate()); // fix time
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }

        Blacklist bl = new Blacklist();
        bl.setTab(blacklistReq);
        bl.setAt(date);
        blacklistRepository.save(bl);
        return null;
    }
}
