package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.History;
import com.aladin.anfico.repository.HistoryRepository;
import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.HistoryService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.impl.HistoryServiceImpl;
import com.aladin.anfico.service.dto.history.HistoryDTO;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.aladin.anfico.web.rest.errors.PageableRequestAlertException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "History",description = "OK")
public class HistoryResource {

    private final Logger log = LoggerFactory.getLogger(HistoryResource.class);

    private final HistoryRepository historyRepository;
    private final HistoryService historyServiceImpl;

        @GetMapping("/histories")
    public ResponseEntity<HashMap<String,?>> getAllHistory(@RequestParam int page, @RequestParam int limit ) {
        log.info("[Anfico App]: GET /api/history -- Get all information history table");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        HashMap<String,?> history =  historyServiceImpl.getAllManagedHistory(page,limit);
        return new ResponseEntity<>(history, HttpStatus.OK);
    }

    @GetMapping("/history/check/{year}")
    public ResponseEntity<Boolean> getAllHistory(@PathVariable String year) {
        log.info("[Anfico App]: GET /api/history -- Check year history table");
        if (historyRepository.findOneByYear(year).isPresent()){
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);

    }

    @GetMapping("/history/{id}")
    public ResponseEntity<History> getAllHistory(@PathVariable Long id) {
        log.info("[Anfico App]: GET /api/history -- Get a information history table");
        return ResponseUtil.wrapOrNotFound(historyServiceImpl.getHistory(id));
    }

    @PostMapping("/history")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<History> createHistory(@Valid @RequestBody HistoryDTO history ) {
        log.info("[Anfico App]: POST /api/history -- Add a information history table");
        if (history.getId() != null) {
            throw new BadRequestAlertException(Alerts.ID_NULL, "history-management", "id");
        }else if (historyRepository.findOneByYear(history.getYear()).isPresent()){
            throw new BadRequestAlertException(Alerts.YEAR_EXIST, "history-management", "year");
        }
        return new ResponseEntity<>(historyServiceImpl.addHistory(history), HttpStatus.OK);
    }

    @PutMapping("/history")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<History> updateHistory(@Valid @RequestBody HistoryDTO historyDTO ){
        log.info("[Anfico App]: PUT /api/history -- Update a information history table");
        Optional<History> history = historyRepository.findOneByYear(historyDTO.getYear());
        if (history.isPresent() && !history.get().getId().equals(historyDTO.getId())){
            throw new BadRequestAlertException(Alerts.YEAR_EXIST, "history-management", "year");
        }
        return ResponseUtil.wrapOrNotFound(historyServiceImpl.updateHistory(historyDTO));
    }

    @DeleteMapping("/history/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Boolean> deleteHistory(@PathVariable Long id ){
        log.info("[Anfico App]: DELETE /api/history/{id} -- Delete a information history table");
        Optional<History> history = historyServiceImpl.deleteHistory(id);
        if (!history.isPresent())
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        return ResponseEntity.ok().body(true);
    }
}
