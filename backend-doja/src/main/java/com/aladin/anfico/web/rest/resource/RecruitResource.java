package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Candidate;
import com.aladin.anfico.domain.Recruit;
import com.aladin.anfico.repository.RecruitRepository;
import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.RecruitService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.recruit.*;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.aladin.anfico.web.rest.errors.PageableRequestAlertException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Recruit",description = "OK")
public class RecruitResource {

    private final Logger log = LoggerFactory.getLogger(com.aladin.anfico.web.rest.resource.RecruitResource.class);

    private final RecruitService recruitServiceImpl;
    private final RecruitRepository recruitRepository;

    @GetMapping("/recruits")
    public ResponseEntity<HashMap<String,?>> getAllUsers(@RequestParam int page, @RequestParam int limit ) {
        log.info("--GET: /api/recruits -- Get all information  recruit table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        HashMap<String,?> recruit =  recruitServiceImpl.getAllManagedRecruit(page,limit);
        return new ResponseEntity<>(recruit, HttpStatus.OK);
    }

    @GetMapping("/recruit/fillter")
    public ResponseEntity<List<RecruitFillterDTO>> getAllUsers() {
        log.info("--GET: /api/recruits -- Get all information  recruit table--");
        return new ResponseEntity<>(recruitServiceImpl.getAllFillterRecruit(), HttpStatus.OK);
    }

    @GetMapping("/recruit/{id}")
    public ResponseEntity<Recruit> getRecruitDetail(@PathVariable Long id ) {
        log.info("--GET: /api/recruit/{id} -- Get detail information  recruit table--");
        return ResponseUtil.wrapOrNotFound( recruitServiceImpl.getRecruitById(id));
    }

    @DeleteMapping("/recruit/{id}")
    public ResponseEntity<Boolean> deleteRecruit(@PathVariable Long id ) {
        log.info("--DELETE: /api/recruit/{id} -- delete detail information  recruit table--");
        Optional<Recruit> recruit = recruitServiceImpl.deleteRecruit(id);
        if (!recruit.isPresent()){
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @PostMapping("/recruit")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<RecruitDTO> createRecruit(@RequestBody RecruitCreateDTO recruitAdminDTO ) {
        log.info("--POST: /api/recruit -- Create information  recruit table--");
        if (recruitAdminDTO.getId() != null) {
            throw new BadRequestAlertException("A new recruit cannot already have an ID", "recruit-Management", "idexists");
        }
        RecruitDTO  recruit =  recruitServiceImpl.addRecruit(recruitAdminDTO).get();
        return new ResponseEntity<>(recruit, HttpStatus.OK);
    }

    @PutMapping("/recruit")
//    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<RecruitDTO> updateRecruit(@RequestBody RecruitCreateDTO recruitAdminDTO ){
        log.info("--PUT: /api/recruit -- Update information  recruit table--");
        recruitServiceImpl.updateRecruit(recruitAdminDTO);
        return ResponseUtil.wrapOrNotFound(recruitServiceImpl.updateRecruit(recruitAdminDTO));
    }

    @PostMapping("/candidate/{id}")
    public ResponseEntity<Candidate> createCandidate(@RequestBody CandidateDTO candidateDTO, @PathVariable Long id ) {
        log.debug("REST request to create candidate");
        Recruit recruit = recruitRepository.findById(id).get();
        if (recruit == null){
            throw new BadRequestAlertException("not recruit", "recruit-Management", "idexists");
        }
        if (candidateDTO.getId() != null) {
            throw new BadRequestAlertException("A new Candidate cannot already have an ID", "recruit-Management", "idexists");
        }
        Candidate  c =  recruitServiceImpl.createCandidateByRecruit(recruit, candidateDTO).get();
        return new ResponseEntity<>(c, HttpStatus.OK);
    }
    @GetMapping("/candidates/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<HashMap<String, ?>> getCandidateById(@PathVariable Long id, @RequestParam int page, @RequestParam int limit ) {
        log.info("--GET: /api/candidate/{id} -- Get all information  recruit table--");
        return new ResponseEntity<>(recruitServiceImpl.getCandidateByRecruit(id, page, limit), HttpStatus.OK);
    }

    @GetMapping("/candidate/detail/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Candidate> getCandidateById(@PathVariable Long id ) {
        log.info("--GET: /api/candidate/{id} -- Get all information  recruit table--");
        return ResponseUtil.wrapOrNotFound(recruitServiceImpl.getCandidateById(id));
    }

    @GetMapping("/candidates")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<HashMap<String, ?>> getCandidate(@RequestParam int page, @RequestParam int limit ) {
        log.info("--GET: /api/candidate/{id} -- Get all information  recruit table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        return new ResponseEntity<>(recruitServiceImpl.getCandidate(page, limit), HttpStatus.OK);
    }
    @PostMapping("/candidate/status")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Boolean> isStatus(@RequestBody CandidateStatusDTO candidateDTO ) {
        log.debug("REST request to status candidate");
        Optional<Candidate> candidate = recruitServiceImpl.isStatus(candidateDTO);
        if (!candidate.isPresent()){
            throw new BadRequestAlertException(Alerts.ID_EXIST, "candidate-management", "id");
        }
        return ResponseEntity.ok().body(candidate.get().isStatus());
    }

    @GetMapping("/recruit/search/{type}/{param}")
    public ResponseEntity<HashMap<String, ?>> searchRecuit(@PathVariable String param, @PathVariable String type, @RequestParam int page, @RequestParam int limit) {
        log.info("--GET: /api/recruit/{id} -- Get detail information  recruit table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        if (type.equals("en")) return ResponseEntity.ok( recruitServiceImpl.searchRecruitEn(param, page, limit));
        return ResponseEntity.ok( recruitServiceImpl.searchRecruitVi(param, page, limit));
    }

}
