package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.user.UserKeycloakDTO;
import com.aladin.anfico.service.impl.UserServiceImpl;
import com.aladin.anfico.service.dto.user.PasswordChangeDTO;
import com.aladin.anfico.service.dto.user.UserPublicDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "User",description = "OK")
public class UserPublicResource {

    private final Logger log = LoggerFactory.getLogger(UserPublicResource.class);
    private final UserServiceImpl userServiceImpl;


    @GetMapping("/user")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<UserKeycloakDTO> getUser(JwtAuthenticationToken auth) {
        final var userId = auth.getToken().getClaimAsString("sub");
        log.info("[Anfico App]: GET /api/user -- Get information give user current");
        return ResponseUtil.wrapOrNotFound(userServiceImpl.getUserWithAuthorities(userId));
    }

    @PutMapping("/change-password")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<?> changePassword(@Valid @RequestBody PasswordChangeDTO passwordChangeDTO, JwtAuthenticationToken auth) {
        final var userId = auth.getToken().getClaimAsString("sub");
        log.info("[Anfico App]: POST /api/user -- Update password give user current id {}", userId);
        if (userServiceImpl.changePassword(passwordChangeDTO, userId )){
            return ResponseEntity.ok().body("Success password !");
        }
        return new ResponseEntity<>("Password incorrect ",null, HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/user")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<UserKeycloakDTO> updatePublicUser(@Valid @RequestBody UserPublicDTO userDTO) {
        log.info("[Anfico App]: POST /api/user -- Update infomation give user current");
        Optional<UserKeycloakDTO> u = userServiceImpl.updatePublicUser(userDTO);
        return ResponseUtil.wrapOrNotFound(u);
    }



}
