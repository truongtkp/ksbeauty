package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.OrderVoucher;
import com.aladin.anfico.domain.Voucher;
import com.aladin.anfico.repository.OrderVoucherRepository;
import com.aladin.anfico.repository.VoucherRepository;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.impl.MailService;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Voucher",description = "OK")
public class VoucherResources {

    private final Logger log = LoggerFactory.getLogger(VoucherResources.class);

    private final VoucherRepository voucherRepository;
    private final OrderVoucherRepository orderVoucherRepository;
    private final ObjectMapper objectMapper;
    private final Validator validator;
    private final MailService mailService;

    @GetMapping("/voucher")
    public ResponseEntity<HashMap<String,?>> getAllVoucher(
        @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit
    ) {
//        List<Voucher> vouchers = voucherRepository.findAll();
        List<Voucher> vouchers;
        if (page == null || limit == null) {
            vouchers = voucherRepository.getAddressSortedByIdDesc();
        } else {
            page = (page < 0)?0:page;
            limit = (limit <= 0)?10:limit;
            vouchers = voucherRepository.getAddressSortedByIdDesc(PageRequest.of(page,limit));
        }
        HashMap<String,?> res = ArrayDTO.convertListToSet(voucherRepository.count(),vouchers);
        return new ResponseEntity<>(res,HttpStatus.OK);
    }

    @PostMapping("/voucher")
    public ResponseEntity<Voucher> addVoucher(
        @RequestBody HashMap<String,Object> voucherReq
    ){
        log.info("--POST: /api/voucher Add new voucher --");
        Voucher voucher = objectMapper.convertValue(voucherReq, Voucher.class);
        Set<ConstraintViolation<Voucher>> violations = validator.validate(voucher);
        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Voucher res = voucherRepository.save(voucher);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PutMapping("/voucher")
    public ResponseEntity<Voucher> updateVoucher(
        @RequestBody HashMap<String,Object> voucherReq){
        Voucher voucher = objectMapper.convertValue(voucherReq, Voucher.class);

        Set<ConstraintViolation<Voucher>> violations = validator.validate(voucher);
        if (!violations.isEmpty() || voucher.getId() == null)
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

//        List<Voucher> vouchers = voucherRepository.;
        Optional<Voucher> res = Optional
            .of(voucherRepository.findById(voucher.getId()))
            .filter(Optional::isPresent)
            .map(param -> voucherRepository.save(voucher));

        return ResponseUtil.wrapOrNotFound(res);
    }

    @GetMapping ("/voucher/{id}")
    public ResponseEntity<Voucher> getVoucherById(@PathVariable Long id) {
        Optional<Voucher> voucher = voucherRepository.findById(id);
        if (voucher.isEmpty()){
            throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
        }
        return ResponseEntity.ok().body(voucher.get());
    }

    @DeleteMapping("/voucher/{id}")
    public ResponseEntity<Boolean> deleteVoucher(@PathVariable Long id){
        Optional
            .of(voucherRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get) .map(param -> {
                List<OrderVoucher> orders = orderVoucherRepository.findOrderByVoucherId(id);
                for (OrderVoucher orderVoucher : orders){
                    orderVoucher.setVoucher(null);
                }
                orderVoucherRepository.saveAll(orders);
//                orderVoucherRepository;

                voucherRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {},
                () -> {
                    throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
                }
            );
        return ResponseEntity.ok().body(true);
    }

    @DeleteMapping("/voucher/force/{id}")
    public ResponseEntity<Boolean> deleteStaffForce(@PathVariable Long id) {
        Optional
            .of(voucherRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get) .map(param -> {
                List<Long> orders = orderVoucherRepository.findOrderIdByVoucherId(id);
                orderVoucherRepository.deleteAllByIdInBatch(orders);

                voucherRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {},
                () -> {
                    throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
                }
            );
        return ResponseEntity.ok().body(true);
    }

    @DeleteMapping("/voucher/safe/{id}")
    public ResponseEntity<Boolean> deleteVoucherSafe(@PathVariable Long id) {
//        Optional<Voucher> voucher = voucherRepository.delete
        Optional
            .of(voucherRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(param -> {
                voucherRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {},
                () -> {
                    throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
                }
            );
        return ResponseEntity.ok().body(true);
    }
}
