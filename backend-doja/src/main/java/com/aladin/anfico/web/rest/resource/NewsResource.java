package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.News;
import com.aladin.anfico.repository.NewsRepository;
import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.NewsService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.impl.NewsServiceImpl;
import com.aladin.anfico.service.dto.news.NewsCreateDTO;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.aladin.anfico.web.rest.errors.PageableRequestAlertException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "News",description = "OK")
public class NewsResource  {

    private final Logger log = LoggerFactory.getLogger(NewsResource.class);
    private final NewsServiceImpl newsServiceImpl;

    private final NewsRepository newsRepository;


    @GetMapping("/news")
    public ResponseEntity<HashMap<String,?>> getAllUsers(
        @RequestParam int page, @RequestParam int limit
    ) {
        log.info("--GET: /api/upload-image -- Get news in news table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        HashMap<String,?> pages =  newsServiceImpl.getAllManagedNews(page,limit);
        return new ResponseEntity<>(pages, HttpStatus.OK);
    }


    @PostMapping("/news")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<News> createNews(@RequestBody NewsCreateDTO news ) {
        log.info("--POST: /api/news -- add news in news table--");
        if (news.getId() != null) {
            throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "news-Management", "id");
        }
        News  n =  newsServiceImpl.createNews(news);
        return new ResponseEntity<>(n, HttpStatus.OK);
    }

    @PutMapping("/news")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<News> updateNews(@RequestBody NewsCreateDTO newsDTO ){
        log.info("--PUT: /api/news -- update news in news table--");
        return ResponseUtil.wrapOrNotFound(newsServiceImpl.updateNews(newsDTO));
    }

    @DeleteMapping("/news/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Boolean> deleteNews(@PathVariable Long id){
        log.info("--PUT: /api/news/status/{id} -- active news in news table--");
        if (!newsServiceImpl.deleteNews(id).isPresent())
            throw new BadRequestAlertException(Alerts.ID_EXIST, "new-service", "id");
        return new ResponseEntity<>(true,HttpStatus.OK);
    }
    @GetMapping("/news/detail/{id}")
    public ResponseEntity<News> getNewsById(@PathVariable Long id){
        log.info("--PUT: /api/news/status/{id} -- active news in news table--");
        return ResponseUtil.wrapOrNotFound(newsRepository.findById(id));
    }

    @DeleteMapping("/news")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Boolean> deleteNews(@RequestBody List<Long> id){
        log.info("--PUT: /api/news -- delete news in news table--");
        return new ResponseEntity<>(newsServiceImpl.deleteAllById(id),HttpStatus.OK);
    }

    @GetMapping("/news/search/{param}")
    public ResponseEntity<HashMap<String,?>> searchNews(@PathVariable String param, @RequestParam int page, @RequestParam int limit ) {
        log.info("--GET: /api/upload-image -- Get news in news table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        HashMap<String,?> pages =  newsServiceImpl.searchNews(param, page, limit);
        return new ResponseEntity<>(pages, HttpStatus.OK);
    }
}
