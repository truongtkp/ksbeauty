package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.service.impl.PaypalServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/api/paypal")
@RequiredArgsConstructor
@Tag(name = "Paypal",description = "OK")
public class PaypalResource {

    private final Logger log = LoggerFactory.getLogger(PaypalResource.class);

    private final ObjectMapper om;

    private final PaypalServiceImpl paypalService;
    private final TypeReference<Map<String,Object>> tr = new TypeReference<>() {};


    @GetMapping("/bearer-token")
    public Map<String,Object> getToken() throws Exception {
        Map<String,Object> res = om.readValue(paypalService.generateBearerAccessToken().getResponseBody(),tr);
        log.info(res.toString());
        return res;
    }

    @PostMapping("/token-identity")
    public Map<String,Object> getBraintreeToken() throws Exception {

        Map<String,Object> res1 = getToken();
        String bearerToken = (String) res1.get("access_token");
        paypalService.setBearerTokenCache(bearerToken);

        Map<String,Object> res = om.readValue(paypalService.generateBrainTreeAccessToken(bearerToken).getResponseBody(),tr);
        res.put("client_id", paypalService.getCLIENT_ID());
        res.put("expires_in",res1.get("expires_in"));
        res.put("nonce",res1.get("nonce"));
        log.info(res.toString());
        return res;
    }

    @GetMapping("/create-order")
    public Map<String,Object> createOrder() {
        try {
//            String bearerToken = "A21AAKZatkwkRJhHVex3HqQENeELNt6SLinEUqvfBvJN7SPXLHF7D9oBP5U_U3Nap341m6tMRpI_D1R1pr7BZi_2_LUrjHTkw";

            Map<String,Object> res1 = getToken();
            String bearerToken = (String) res1.get("access_token");


//            String bearerToken = paypalService.getBearerTokenCache();


//            if (false){ // for testing
//                Map<String,Object> res1 = getToken();
//                bearerToken = (String) res1.get("access_token");
//            }

            return om.readValue(paypalService.createOrderV2(bearerToken).getResponseBody(),tr);
        } catch (Exception ex) {
            log.error("",ex);
        }
        return null;
    }

    @GetMapping("/capture-order/{id}")
    public Map<String, Object> captureOrder(@PathVariable String id) {
        log.info("--GET: /api/paypal/capture-order/{id} --");
        try {
//            String bearerToken = "A21AAKZatkwkRJhHVex3HqQENeELNt6SLinEUqvfBvJN7SPXLHF7D9oBP5U_U3Nap341m6tMRpI_D1R1pr7BZi_2_LUrjHTkw";
            Map<String,Object> res1 = getToken();
            String bearerToken = (String) res1.get("access_token");
//            String bearerToken = paypalService.getBearerTokenCache();
            return om.readValue(paypalService.capturePaymentV2(bearerToken, id).getResponseBody(), tr);
        } catch (Exception ex) {
            log.error("",ex);
        }
        return null;
    }

    @GetMapping("/payments")
    public Map<String, Object> getAllPayments(){
        try {
            Map<String,Object> res1 = getToken();
            String bearerToken = (String) res1.get("access_token");
            return om.readValue(paypalService.listPaymentV1(bearerToken,10,0),tr);
        } catch (Exception ex) {
            log.error("",ex);
        }
        return null;
    }


    @PostMapping("/checkout/{id}")
    public Map<String, Object> checkout(@PathVariable String id){
        log.info("--GET: /api/paypal/checkout/{id}");

        return null;
    }
}
