/**
 * View Models used by Spring MVC REST controllers.
 */
package com.aladin.anfico.web.rest.vm;
