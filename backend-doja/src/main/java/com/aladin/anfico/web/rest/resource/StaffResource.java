package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Staff;
import com.aladin.anfico.repository.BookingRepository;
import com.aladin.anfico.repository.StaffRepository;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Staff",description = "staff")
public class StaffResource {
    private final Logger log = LoggerFactory.getLogger(StaffResource.class);
    private final StaffRepository staffRepository;
    private final BookingRepository bookingRepository;
    private final ObjectMapper objectMapper;
    private final Validator validator;
    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    @GetMapping("/staff")
    public ResponseEntity<HashMap<String,?>> getAllStaff(
        @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit
    ) {
        log.info("--GET: /api/staff-- Get staff in staff table--");
        List<Staff> staffList;
        if (page == null || limit == null) {
            staffList = staffRepository.getStaffSortedByIdDesc();
        } else {
            page = (page < 0)?0:page;
            limit = (limit <= 0)?10:limit;
            staffList = staffRepository.getStaffSortedByIdDesc(PageRequest.of(page,limit));
        }
        HashMap<String,?> res = ArrayDTO.convertListToSet(staffRepository.count(),staffList);
        return new ResponseEntity<>(res,HttpStatus.OK);
    }

    @PostMapping("/staff")
    public ResponseEntity<Staff> addStaff(
        @RequestBody HashMap<String,Object> staffReq) {
        log.info("--POST: /api/product -- add staff table--");
        Staff staff = objectMapper.convertValue(staffReq,Staff.class);

        Set<ConstraintViolation<Staff>> violations = validator.validate(staff);
        if (!violations.isEmpty() )
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Date birthday;
        try {
            birthday = df.parse((String) staffReq.get("birthday"));
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }

        staff.setBirthday(birthday);
        Staff res = staffRepository.save(staff);
        return new ResponseEntity<>(res,HttpStatus.OK);
    }

    @PutMapping("/staff")
    public ResponseEntity<Staff> updateStaff(
        @RequestBody HashMap<String,Object> staffReq){
        Staff staff = objectMapper.convertValue(staffReq, Staff.class);

        Set<ConstraintViolation<Staff>> violations = validator.validate(staff);
        if (!violations.isEmpty() || staff.getId() == null)
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Date birthday;
        try {
            birthday = df.parse((String) staffReq.get("birthday"));
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }

        staff.setBirthday(birthday);

//        List<Voucher> vouchers = voucherRepository.;
        Optional<Staff> res = Optional
            .of(staffRepository.findById(staff.getId()))
            .filter(Optional::isPresent)
            .map(param -> staffRepository.save(staff));

        return ResponseUtil.wrapOrNotFound(res);
    }

    @GetMapping ("/staff/{id}")
    public ResponseEntity<Staff> getStaffById(@PathVariable Long id) {
        Optional<Staff> booking = staffRepository.findById(id);
        if (booking.isEmpty()){
            throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
        }
        return ResponseEntity.ok().body(booking.get());
    }

    @DeleteMapping("/staff/{id}")
    public ResponseEntity<Boolean> deleteStaff(@PathVariable Long id) {
//        Optional<Voucher> voucher = voucherRepository.delete
        Optional
            .of(staffRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(param -> {
                staffRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {},
                () -> {
                    throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
                }
            );
        return ResponseEntity.ok().body(true);
    }

    @DeleteMapping("/staff/force/{id}")
    public ResponseEntity<Boolean> deleteStaffForce(@PathVariable Long id){
        Optional
            .of(staffRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get) .map(param -> {
                List<Long> bookings = bookingRepository.findBookingIdByStaffId(id);
                bookingRepository.deleteAllByIdInBatch(bookings);

                staffRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {},
                () -> {
                    throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
                }
            );
        return ResponseEntity.ok().body(true);
    }

    @GetMapping("/staff/search/{param}")
    public ResponseEntity<HashMap<String, ?>> searchStaff(@PathVariable String param
        , @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit
    ) {
        log.info("--GET: /api/address -- Get product detail in product table--");
//        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        List<Staff> staffList;
        if (page == null || limit == null) {
            staffList = staffRepository.searchStaff(param);
        } else {
            staffList = staffRepository.searchStaff(param, PageRequest.of(page,limit));
        }
        HashMap<String,?> res = ArrayDTO.convertListToSet(staffList.size(),staffList);
        return ResponseEntity.ok().body(res);
    }
}
