package com.aladin.anfico.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Packages {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "title", length = 256)
    @Size(min = 0, max = 256)
    private String title;

    @Column(name = "description",columnDefinition = "LONGTEXT")
    private String description;

    @NotNull
    @PositiveOrZero
    @Column(name = "price")
    private Integer price;
}
