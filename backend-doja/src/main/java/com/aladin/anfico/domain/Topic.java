package com.aladin.anfico.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "topic")
@AllArgsConstructor
@NoArgsConstructor

public class Topic implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


//    @Column(name = "description_vi", length = 256)
//    @Size(max = 256)
//    private String descriptionVi;

    @Column(name = "description_en", length = 256)
    @Size(max = 256)
    private String descriptionEn;

    @JsonIgnore
    private Type type;

    @JsonIgnore
    @OneToMany(mappedBy = "topic", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    private Set<TopicImage> topicImages = new HashSet<>();

    @Transient
    private TopicImage topicImage;

}
