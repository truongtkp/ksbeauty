package com.aladin.anfico.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;


@Entity
@Table(name = "category")
@Getter
@Setter
public class Category  extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min= 0, max = 1000)
    @Column(name = "name_en", length = 1000)
    private String nameEn;

//    @Column(name = "name_vi", length = 1000)
//    @Size(min= 0, max = 1000)
//    private String nameVi;

    @Column(name = "description_en", columnDefinition = "TEXT")
    @Size(max = 65535)
    @NotNull
    private String descriptionEn;

//    @Column(name = "description_vi", columnDefinition = "TEXT")
//    @Size(max = 65535)
//    @NotNull
//    private String descriptionVi;

    @Column(name = "image_path", length = 500)
    @Size(min = 0, max = 500)
    private String imagePath;

    @Column(name = "image_url", length = 500)
    @Size(min = 0, max = 500)
    private String imageUrl;

    @JsonIgnore
    @OneToMany(mappedBy = "category", cascade = {CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Collection<Product> products;

}
