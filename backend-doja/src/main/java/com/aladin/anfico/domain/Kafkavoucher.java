package com.aladin.anfico.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name = "kafkavoucher")
@AllArgsConstructor
@NoArgsConstructor
public class Kafkavoucher extends AbstractAuditingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "subject")
    private String subject;

    @Column(name = "too")
    private String too;

    @Column(name = "expires")
    private Date expires;

    @Column(name = "recipients")
    private String recipients;

    @Column(name = "recipients_name")
    private String recipientsName;

    @Column(name = "cc")
    private String cc;

    @Column(name = "datetime")
    private Date datetime;

    @Column(name="orderId")
    private String orderId;

    @Column(name="price")
    private Integer price;

    @Column(name = "path")
    private String path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getToo() {
        return too;
    }

    public void setToo(String too) {
        this.too = too;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getRecipientsName() {
        return recipientsName;
    }

    public void setRecipientsName(String recipientsName) {
        this.recipientsName = recipientsName;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Kafkavoucher)) {
            return false;
        }
        return id != null && id.equals(((Kafkavoucher) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "Kafkavoucher{" +
            "id=" + id +
            ", subject='" + subject + '\'' +
            ", too='" + too + '\'' +
            ", expires=" + expires +
            ", recipients='" + recipients + '\'' +
            ", recipientsName='" + recipientsName + '\'' +
            ", cc='" + cc + '\'' +
            ", datetime=" + datetime +
            ", orderId='" + orderId + '\'' +
            ", price=" + price +
            ", path='" + path + '\'' +
            '}';
    }
}
