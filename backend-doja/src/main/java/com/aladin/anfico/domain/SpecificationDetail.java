package com.aladin.anfico.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "specification_detail")
@Getter
@Setter
public class SpecificationDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "properties_en", length = 200)
    @Size(max = 200)
    private String propertiesEn;

    @Column(name = "properties_vi", length = 200)
    @Size(max = 200)
    private String propertiesVi;

    @Column(name = "value_vi", length = 500)
    @Size(max = 500)
    private String valueVi;

    @Column(name = "value_en", length = 500)
    @Size(max = 500)
    private String valueEn;

    @JsonIgnore
    @ManyToOne
    private Specification specification;


}
