package com.aladin.anfico.domain;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "project")
@AllArgsConstructor
@NoArgsConstructor
public class Project extends AbstractAuditingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title_en", length = 500)
    @Size(min = 0, max = 500)
    private String titleEn;

    @Column(name = "descriptionEn", columnDefinition = "LONGTEXT")
    private String descriptionEn;

    @Column(name = "content_en", columnDefinition = "LONGTEXT")
    private String contentEn;

    @Column(name = "avatar_url", length = 500)
    @Size(min = 0, max = 500)
    private String avatarUrl;

    @Column(name = "avatar_path", length = 500)
    @Size(min = 0, max = 500)
    private String avatarPath;

    private boolean priority = false;

    public Project(Long id, String titleEn, String descriptionEn, String avatarUrl) {
        this.id = id;
        this.titleEn = titleEn;
        this.descriptionEn = descriptionEn;
        this.avatarUrl = avatarUrl;
    }
}
