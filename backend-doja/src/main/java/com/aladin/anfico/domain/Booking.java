package com.aladin.anfico.domain;

import com.aladin.anfico.config.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Time;
import java.util.Date;
import java.util.List;

// list of booking
@Entity
@Data
@Table(name = "booking"
//    ,uniqueConstraints = { @UniqueConstraint(columnNames = {"staff_id","timetable","booking_date"})}
)
@AllArgsConstructor
@NoArgsConstructor
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.MERGE)
    private Address address;

//    @NotNull
//    @OneToMany(cascade = CascadeType.MERGE)
//    private List<Packages> servicePackage;

    @OneToOne(cascade = CascadeType.MERGE)
    private Packages servicePackage;

    @OneToOne(cascade = CascadeType.MERGE)
    private Staff staff;

    @OneToOne(cascade = CascadeType.MERGE)
    private OrderVoucher orderVoucher = null;


//    @OneToOne(cascade = CascadeType.MERGE)
//    private TimeTable timetable;

    @Column
    private Time timetable;

    @Column(name= "booking_date")
    private Date bookingDate;

    @NotNull
    @Column(name= "full_name")
    private String fullName;

    @Email
    @Column(name="email")
    private String email;

    @Pattern(regexp = Constants.PHONE_REGEX)
    @NotNull
    @Column(name="phone_no")
    private String phoneNo;

    @Column(name="order_id") // paypal order id
    private String orderId;
}
