package com.aladin.anfico.domain;

import com.aladin.anfico.config.Constants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "candidate")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Candidate extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "fullname", length = 256)
    @Size(min = 5, max = 256)
    private String fullname;


    private boolean gender;

    @Email
    @Size(min = 5, max = 254)
    @Column(length = 254, unique = false)
    private String email;

    @Column(name = "birthday")
    private Date birthday;

    @Size(min = 5, max = 256)
    @Column(name = "address", length = 256)
    private String address;


    @Column(name = "phone", length = 10)
    @Pattern(regexp = Constants.PHONE_REGEX)
    private String phone;

    @Column(name = "education", length = 50)
    @Size(min = 5, max = 50)
    private String education;

    @Column(name = "experience", length = 50)
    @Size(min = 5, max = 50)
    private String experience;

    @Column(name = "goal", length = 50)
    @Size(min = 5, max = 50)
    private String goal;

    @Column(name = "presenter", length = 50)
    private String presenter;

    @Column(name = "cv_url", length = 256)
    @Size(min = 5, max = 256)
    private String cvUrl;

    @Column(name = "cv_path", length = 256)
    @Size(min = 5, max = 256)
    private String cvPath;

    private boolean status;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Recruit recruit;

}
