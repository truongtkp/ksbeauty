package com.aladin.anfico.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;


@Entity
@Table(name = "product")
@Getter
@Setter
@NoArgsConstructor
public class Product extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title_en", length = 500)
    @NotNull
    @Size(min = 0, max = 500)
    private String titleEn;

//    @Column(name = "title_vi", length = 500)
//    @Size(min = 0, max = 500)
//    @NotNull
//    private String titleVi;

    @Column(name = "description_en", columnDefinition = "LONGTEXT")
    @NotNull
    private String descriptionEn;

//    @Column(name = "description_vi", columnDefinition = "LONGTEXT")
//    @NotNull
//    private String descriptionVi;

//    @Column(name = "content_vi", columnDefinition = "LONGTEXT")
//    @NotNull
//    private String contentVi;

    @Column(name = "content_en", columnDefinition = "LONGTEXT")
    @NotNull
    private String contentEn;

    private boolean priority = false;

    @OneToMany(mappedBy = "_product", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Specification> specifications;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Category category;

    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<ProductImage> productImages = new HashSet<>();

    public Product(Long id, String titleEn,
                    String description_en, String avatarUrl, boolean priority) {
        this.id = id;
        this.titleEn = titleEn;
//        this.titleVi = titleVi;
        this.descriptionEn = description_en;
//        this.descriptionVi = description_vi;
        this.avatarUrl = avatarUrl;
        this.category = null;
        this.specifications = null;
        this.productImages = null;
        this.priority = priority;
    }
    @Transient
    private String avatarUrl;

}
