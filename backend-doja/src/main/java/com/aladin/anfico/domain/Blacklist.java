package com.aladin.anfico.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;


@Entity
@Data
@Table(name = "blacklist")
@AllArgsConstructor
@NoArgsConstructor
public class Blacklist {
    @Id
    @NotNull
    @Column
    private Date at;

//    @Column
//    private Date from;

//    @Column
//    private Date to;

    @NotNull
    @Lob
    @Column
    private HashMap<String,Object> tab;

}
