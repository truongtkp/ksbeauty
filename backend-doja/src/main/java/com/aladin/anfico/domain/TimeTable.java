package com.aladin.anfico.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;

@Entity
@Getter
@Table(name = "timetable")
@NoArgsConstructor
public class TimeTable {
    @NotNull
    @Id
    private Time id;

    @Column
    private Integer hour;

    @Column
    private Integer minute;

    public TimeTable(Time id) {
        this.id = id;
        this.hour = id.getHours();
        this.minute = id.getMinutes();
    }

    public void setId(Time id) {
        this.id = id;
        this.hour = id.getHours();
        this.minute = id.getMinutes();
    }
}
