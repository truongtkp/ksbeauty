package com.aladin.anfico.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Entity
@Data
@Table(name = "Kafkabooking")
@AllArgsConstructor
@NoArgsConstructor
public class Kafkabooking extends AbstractAuditingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "subject")
    private String subject;

    @Column(name = "address")
    private String address;

    @Column(name = "servicePackage")
    private String servicePackage;

    @Column(name = "staff")
    private String staff;

    @Column(name = "timetable")
    private String timetable;

    @Column(name= "bookingDate")
    private Date bookingDate;

    @Column(name = "recipients")
    private String recipients;

    @Column(name = "recipients_name")
    private String recipientsName;

    @Column(name="orderId")
    private String orderId;

    @Column(name = "cc")
    private String cc;

    @Column(name = "datetime")
    private Date datetime;

    @Column(name = "path")
    private String path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getServicePackage() {
        return servicePackage;
    }

    public void setServicePackage(String servicePackage) {
        this.servicePackage = servicePackage;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getTimetable() {
        return timetable;
    }

    public void setTimetable(String timetable) {
        this.timetable = timetable;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getRecipientsName() {
        return recipientsName;
    }

    public void setRecipientsName(String recipientsName) {
        this.recipientsName = recipientsName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Kafkabooking)) {
            return false;
        }
        return id != null && id.equals(((Kafkabooking) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "Kafkabooking{" +
            "id=" + id +
            ", subject='" + subject + '\'' +
            ", address='" + address + '\'' +
            ", servicePackage='" + servicePackage + '\'' +
            ", staff='" + staff + '\'' +
            ", timetable=" + timetable +
            ", bookingDate=" + bookingDate +
            ", recipients='" + recipients + '\'' +
            ", recipientsName='" + recipientsName + '\'' +
            ", orderId='" + orderId + '\'' +
            ", cc='" + cc + '\'' +
            ", datetime=" + datetime +
            ", path='" + path + '\'' +
            '}';
    }
}
