package com.aladin.anfico.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


    @Configuration
    @ComponentScan
    public class UploadFileConfig implements WebMvcConfigurer {

        @Override
        public void addResourceHandlers(final ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/"+"fe"+"/**").addResourceLocations("file:"+"fe"+"/");
            registry.addResourceHandler("/"+"file"+"/**").addResourceLocations("file:"+"file"+"/");
            registry.addResourceHandler("/"+"image"+"/**").addResourceLocations("file:"+"image"+"/");
            registry.addResourceHandler("/"+"images"+"/**").addResourceLocations("file:"+"images"+"/");
        }


    }
