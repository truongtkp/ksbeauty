package com.aladin.anfico.config;

import org.keycloak.admin.client.Keycloak;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Component
public class KeycloakConfiguration {
    @Value("${keycloak.auth-server-url}")
    private String authServerUrl;
    @Value("${keycloak.realm}")
    private String realm;

    @Value("${keycloak_system.username}")
    private String username;

    @Value("${keycloak_system.password}")
    private String password;

    @Value("${spring.security.oauth2.client.registration.oidc.client-id}")
    private String clientId;

    @Value("${spring.security.oauth2.client.registration.oidc.client-secret}")
    private String clientSecret;


    public final Keycloak getKeycloakInstance() {
        return Keycloak.getInstance(
            authServerUrl,
            realm,
            username,
            password,
            clientId,
            clientSecret);
    }

    public final Keycloak getKeycloakInstanceUser(String usernameUser, String passwordUser) {
        return Keycloak.getInstance(
            authServerUrl,
            realm,
            usernameUser,
            passwordUser,
            clientId,
            clientSecret);
    }



}
