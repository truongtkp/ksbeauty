package com.aladin.anfico.config;

import com.aladin.anfico.security.*;
import com.aladin.anfico.security.SecurityUtils;
import com.aladin.anfico.security.oauth2.AudienceValidator;
import com.aladin.anfico.security.oauth2.JwtGrantedAuthorityConverter;
import java.util.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;
import tech.jhipster.config.JHipsterProperties;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import(SecurityProblemSupport.class)
public class SecurityConfiguration {

    private final JHipsterProperties jHipsterProperties;

    @Value("${spring.security.oauth2.client.provider.oidc.issuer-uri}")
    private String issuerUri;

    private final SecurityProblemSupport problemSupport;

    public SecurityConfiguration(JHipsterProperties jHipsterProperties, SecurityProblemSupport problemSupport) {
        this.problemSupport = problemSupport;
        this.jHipsterProperties = jHipsterProperties;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        // @formatter:off
        http
            .csrf()
            .disable()
            .exceptionHandling()
                .authenticationEntryPoint(problemSupport)
                .accessDeniedHandler(problemSupport)
        .and()
            .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
            .antMatchers("/api/authenticate").permitAll()
            .antMatchers("/api/register").permitAll()



            .antMatchers(HttpMethod.GET,"/api/recruits").permitAll()
            .antMatchers(HttpMethod.GET,"/api/search/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/recruit/**").permitAll()

            .antMatchers(HttpMethod.GET,"/api/news").permitAll()
            .antMatchers(HttpMethod.GET,"/api/news/detail/**").permitAll()

            .antMatchers(HttpMethod.GET,"/api/projects").permitAll()
            .antMatchers(HttpMethod.GET,"/api/project/detail/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/project/priority").permitAll()
            .antMatchers(HttpMethod.GET,"/api/project/home").permitAll()


            .antMatchers(HttpMethod.GET,"/api/banner").permitAll()
            .antMatchers(HttpMethod.GET,"/api/download-file").permitAll()
            .antMatchers(HttpMethod.GET,"/api/partner").permitAll()

            .antMatchers(HttpMethod.GET,"/api/histories").permitAll()
            .antMatchers(HttpMethod.GET,"/api/categories").permitAll()
            .antMatchers(HttpMethod.GET,"/api/search/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/devices").permitAll()
            .antMatchers(HttpMethod.GET,"/api/album").permitAll()
            .antMatchers(HttpMethod.GET,"/api/album/**").permitAll()

            .antMatchers(HttpMethod.GET,"/api/product/filter/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/product").permitAll()

            .antMatchers("/api/product").permitAll()

            .antMatchers(HttpMethod.GET,"/api/product/detail/**").permitAll()


            .antMatchers(HttpMethod.DELETE,"/api/delete-file").permitAll()

            .antMatchers(HttpMethod.GET,"/api/supplier/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/suppliers").permitAll()

            .antMatchers(HttpMethod.POST,"/api/candidate/**").permitAll()

            .antMatchers(HttpMethod.POST,"/api/contact").permitAll()

            .antMatchers("/api/activate").permitAll()
            .antMatchers("/api/banner").permitAll()
            .antMatchers(HttpMethod.POST,"/api/upload-file").permitAll()
            .antMatchers("/api/account/reset-password/init").permitAll()
            .antMatchers("/api/account/reset-password/finish").permitAll()

            // DOJA SPA

            .antMatchers("/api/voucher").permitAll()
            .antMatchers("/api/package").permitAll()
            .antMatchers("/api/order-voucher").permitAll()
            .antMatchers("/api/staff").permitAll()
            .antMatchers("/api/address").permitAll()
            .antMatchers("/api/booking").permitAll()
            .antMatchers("/api/timetable").permitAll()

            .antMatchers("/api/map/search/**").permitAll()
            .antMatchers("/api/address/search/**").permitAll()
            .antMatchers("/api/staff/search/**").permitAll()

            .antMatchers("/api/booking/search/date").permitAll()
            .antMatchers("/api/booking/confirm/**").permitAll()

            .antMatchers("/api/package/**").permitAll()
            .antMatchers("/api/address/**").permitAll()
            .antMatchers("/api/booking/**").permitAll()
            .antMatchers("/api/voucher/**").permitAll()
            .antMatchers("/api/order-voucher/**").permitAll()
            .antMatchers("/api/blacklist/**").permitAll()

            .antMatchers("/api/staff/**").permitAll()

            // paypal
            .antMatchers("/api/paypal/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/paypal/create-order").permitAll()
            .antMatchers(HttpMethod.GET,"/api/paypal/capture-order/**").permitAll()
//            .antMatchers(HttpMethod.GET)
//            .antMatchers(HttpMethod.GET,"/api/").permitAll()



            .antMatchers("/api/admin/users").hasAuthority(AuthoritiesConstants.USER)
            .antMatchers("/api/admin/user/search/**").hasAuthority(AuthoritiesConstants.USER)
            .antMatchers("/api/admin/**")
            .hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers("/api/**").authenticated()

//            .antMatchers("/api/admin/users").permitAll()
//            .antMatchers("/api/admin/user/search/**").permitAll()
//            .antMatchers("/api/admin/**").permitAll()

            .antMatchers("/management/health").permitAll()
            .antMatchers("/management/health/**").permitAll()
            .antMatchers("/management/info").permitAll()
            .antMatchers("/management/prometheus").permitAll()
            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)


        .and()
            .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(authenticationConverter())
                .and()
            .and()
                .oauth2Client();
        return http.build();
        // @formatter:on
    }

    Converter<Jwt, AbstractAuthenticationToken> authenticationConverter() {
        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(new JwtGrantedAuthorityConverter());
        return jwtAuthenticationConverter;
    }

    @Bean
    JwtDecoder jwtDecoder() {
        NimbusJwtDecoder jwtDecoder = JwtDecoders.fromOidcIssuerLocation(issuerUri);

        OAuth2TokenValidator<Jwt> audienceValidator = new AudienceValidator(jHipsterProperties.getSecurity().getOauth2().getAudience());
        OAuth2TokenValidator<Jwt> withIssuer = JwtValidators.createDefaultWithIssuer(issuerUri);
        OAuth2TokenValidator<Jwt> withAudience = new DelegatingOAuth2TokenValidator<>(withIssuer, audienceValidator);

        jwtDecoder.setJwtValidator(withAudience);

        return jwtDecoder;
    }
}
